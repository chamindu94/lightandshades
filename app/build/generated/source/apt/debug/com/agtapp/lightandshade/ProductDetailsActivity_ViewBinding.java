// Generated code from Butter Knife. Do not modify!
package com.agtapp.lightandshade;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.agtapp.lightandshade.util.text_view.TVRobotoBold;
import com.agtapp.lightandshade.util.text_view.TVRobotoRegular;
import java.lang.IllegalStateException;
import java.lang.Override;

public class ProductDetailsActivity_ViewBinding implements Unbinder {
  private ProductDetailsActivity target;

  @UiThread
  public ProductDetailsActivity_ViewBinding(ProductDetailsActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public ProductDetailsActivity_ViewBinding(ProductDetailsActivity target, View source) {
    this.target = target;

    target.toolbar = Utils.findRequiredViewAsType(source, R.id.toolbar, "field 'toolbar'", Toolbar.class);
    target.imageLoad = Utils.findRequiredViewAsType(source, R.id.image_load, "field 'imageLoad'", ProgressBar.class);
    target.imImage = Utils.findRequiredViewAsType(source, R.id.im_image, "field 'imImage'", ImageView.class);
    target.txProductId = Utils.findRequiredViewAsType(source, R.id.tx_product_id, "field 'txProductId'", TVRobotoRegular.class);
    target.txProductTitle = Utils.findRequiredViewAsType(source, R.id.tx_product_title, "field 'txProductTitle'", TVRobotoBold.class);
    target.txProductCategory = Utils.findRequiredViewAsType(source, R.id.tx_product_category, "field 'txProductCategory'", TVRobotoRegular.class);
    target.txProductBrand = Utils.findRequiredViewAsType(source, R.id.tx_product_brand, "field 'txProductBrand'", TVRobotoRegular.class);
    target.txProductUnit = Utils.findRequiredViewAsType(source, R.id.tx_product_unit, "field 'txProductUnit'", TVRobotoRegular.class);
    target.txProductPrice = Utils.findRequiredViewAsType(source, R.id.tx_product_price, "field 'txProductPrice'", TVRobotoRegular.class);
    target.txProductDes = Utils.findRequiredViewAsType(source, R.id.tx_product_des, "field 'txProductDes'", TVRobotoRegular.class);
    target.cdAllProducts = Utils.findRequiredViewAsType(source, R.id.cd_all_products, "field 'cdAllProducts'", CardView.class);
    target.cdAddQuote = Utils.findRequiredViewAsType(source, R.id.cd_add_quote, "field 'cdAddQuote'", CardView.class);
    target.lyNormalView = Utils.findRequiredViewAsType(source, R.id.ly_normal_view, "field 'lyNormalView'", LinearLayout.class);
    target.swipeRefreshLayout = Utils.findRequiredViewAsType(source, R.id.swipe_refresh_layout, "field 'swipeRefreshLayout'", SwipeRefreshLayout.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    ProductDetailsActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.toolbar = null;
    target.imageLoad = null;
    target.imImage = null;
    target.txProductId = null;
    target.txProductTitle = null;
    target.txProductCategory = null;
    target.txProductBrand = null;
    target.txProductUnit = null;
    target.txProductPrice = null;
    target.txProductDes = null;
    target.cdAllProducts = null;
    target.cdAddQuote = null;
    target.lyNormalView = null;
    target.swipeRefreshLayout = null;
  }
}
