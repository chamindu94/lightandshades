// Generated code from Butter Knife. Do not modify!
package com.agtapp.lightandshade;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.support.v7.widget.CardView;
import android.view.View;
import android.widget.CheckBox;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.agtapp.lightandshade.util.edit_text.ETRobotoRegular;
import com.agtapp.lightandshade.util.text_view.TVRobotoRegular;
import java.lang.IllegalStateException;
import java.lang.Override;

public class LoginActivity_ViewBinding implements Unbinder {
  private LoginActivity target;

  @UiThread
  public LoginActivity_ViewBinding(LoginActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public LoginActivity_ViewBinding(LoginActivity target, View source) {
    this.target = target;

    target.etEmail = Utils.findRequiredViewAsType(source, R.id.et_email, "field 'etEmail'", ETRobotoRegular.class);
    target.etPassword = Utils.findRequiredViewAsType(source, R.id.et_password, "field 'etPassword'", ETRobotoRegular.class);
    target.passwordView = Utils.findRequiredViewAsType(source, R.id.passwordView, "field 'passwordView'", CheckBox.class);
    target.txForgotPassword = Utils.findRequiredViewAsType(source, R.id.tx_forgot_password, "field 'txForgotPassword'", TVRobotoRegular.class);
    target.buLogin = Utils.findRequiredViewAsType(source, R.id.bu_login, "field 'buLogin'", CardView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    LoginActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.etEmail = null;
    target.etPassword = null;
    target.passwordView = null;
    target.txForgotPassword = null;
    target.buLogin = null;
  }
}
