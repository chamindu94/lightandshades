// Generated code from Butter Knife. Do not modify!
package com.agtapp.lightandshade;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.LinearLayout;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.agtapp.lightandshade.util.text_view.TVRobotoBold;
import com.agtapp.lightandshade.util.text_view.TVRobotoRegular;
import java.lang.IllegalStateException;
import java.lang.Override;

public class InquireDetailActivity_ViewBinding implements Unbinder {
  private InquireDetailActivity target;

  @UiThread
  public InquireDetailActivity_ViewBinding(InquireDetailActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public InquireDetailActivity_ViewBinding(InquireDetailActivity target, View source) {
    this.target = target;

    target.toolbar = Utils.findRequiredViewAsType(source, R.id.toolbar, "field 'toolbar'", Toolbar.class);
    target.txInquireId = Utils.findRequiredViewAsType(source, R.id.tx_inquire_id, "field 'txInquireId'", TVRobotoBold.class);
    target.txCreatedNameAndRole = Utils.findRequiredViewAsType(source, R.id.tx_created_name_and_role, "field 'txCreatedNameAndRole'", TVRobotoRegular.class);
    target.txCreatedDate = Utils.findRequiredViewAsType(source, R.id.tx_created_date, "field 'txCreatedDate'", TVRobotoRegular.class);
    target.txCreatedTime = Utils.findRequiredViewAsType(source, R.id.tx_created_time, "field 'txCreatedTime'", TVRobotoRegular.class);
    target.txCustomerName = Utils.findRequiredViewAsType(source, R.id.tx_customer_name, "field 'txCustomerName'", TVRobotoBold.class);
    target.txCustomerPhone = Utils.findRequiredViewAsType(source, R.id.tx_customer_phone, "field 'txCustomerPhone'", TVRobotoRegular.class);
    target.txCustomerEmail = Utils.findRequiredViewAsType(source, R.id.tx_customer_email, "field 'txCustomerEmail'", TVRobotoRegular.class);
    target.txCustomerAddress = Utils.findRequiredViewAsType(source, R.id.tx_customer_address, "field 'txCustomerAddress'", TVRobotoRegular.class);
    target.quotationList = Utils.findRequiredViewAsType(source, R.id.quotation_list, "field 'quotationList'", RecyclerView.class);
    target.cdQuotationView = Utils.findRequiredViewAsType(source, R.id.cd_quotation_view, "field 'cdQuotationView'", CardView.class);
    target.cdCheckIn = Utils.findRequiredViewAsType(source, R.id.cd_check_in, "field 'cdCheckIn'", CardView.class);
    target.cdCreateQuote = Utils.findRequiredViewAsType(source, R.id.cd_create_quote, "field 'cdCreateQuote'", CardView.class);
    target.lyStep1 = Utils.findRequiredViewAsType(source, R.id.ly_step_1, "field 'lyStep1'", LinearLayout.class);
    target.cdEndInquiry = Utils.findRequiredViewAsType(source, R.id.cd_end_inquiry, "field 'cdEndInquiry'", CardView.class);
    target.cdCancelInquiry = Utils.findRequiredViewAsType(source, R.id.cd_cancel_inquiry, "field 'cdCancelInquiry'", CardView.class);
    target.lyStep2 = Utils.findRequiredViewAsType(source, R.id.ly_step_2, "field 'lyStep2'", LinearLayout.class);
    target.swipeRefreshLayout = Utils.findRequiredViewAsType(source, R.id.swipe_refresh_layout, "field 'swipeRefreshLayout'", SwipeRefreshLayout.class);
    target.cdQuoteNew = Utils.findRequiredViewAsType(source, R.id.cd_quote_new, "field 'cdQuoteNew'", CardView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    InquireDetailActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.toolbar = null;
    target.txInquireId = null;
    target.txCreatedNameAndRole = null;
    target.txCreatedDate = null;
    target.txCreatedTime = null;
    target.txCustomerName = null;
    target.txCustomerPhone = null;
    target.txCustomerEmail = null;
    target.txCustomerAddress = null;
    target.quotationList = null;
    target.cdQuotationView = null;
    target.cdCheckIn = null;
    target.cdCreateQuote = null;
    target.lyStep1 = null;
    target.cdEndInquiry = null;
    target.cdCancelInquiry = null;
    target.lyStep2 = null;
    target.swipeRefreshLayout = null;
    target.cdQuoteNew = null;
  }
}
