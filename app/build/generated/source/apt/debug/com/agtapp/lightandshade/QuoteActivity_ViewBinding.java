// Generated code from Butter Knife. Do not modify!
package com.agtapp.lightandshade;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.agtapp.lightandshade.util.edit_text.ETRobotoRegular;
import com.agtapp.lightandshade.util.text_view.TVRobotoBold;
import java.lang.IllegalStateException;
import java.lang.Override;

public class QuoteActivity_ViewBinding implements Unbinder {
  private QuoteActivity target;

  @UiThread
  public QuoteActivity_ViewBinding(QuoteActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public QuoteActivity_ViewBinding(QuoteActivity target, View source) {
    this.target = target;

    target.toolbar = Utils.findRequiredViewAsType(source, R.id.toolbar, "field 'toolbar'", Toolbar.class);
    target.roomList = Utils.findRequiredViewAsType(source, R.id.room_list, "field 'roomList'", RecyclerView.class);
    target.buBehaviorChange = Utils.findRequiredViewAsType(source, R.id.bu_behavior_change, "field 'buBehaviorChange'", LinearLayout.class);
    target.lyBottomView = Utils.findRequiredViewAsType(source, R.id.ly_bottom_view, "field 'lyBottomView'", NestedScrollView.class);
    target.imBehaviorChange = Utils.findRequiredViewAsType(source, R.id.im_behavior_change, "field 'imBehaviorChange'", ImageView.class);
    target.cdSubmitQuote = Utils.findRequiredViewAsType(source, R.id.cd_submit_quote, "field 'cdSubmitQuote'", CardView.class);
    target.txAllTotal = Utils.findRequiredViewAsType(source, R.id.tx_all_total, "field 'txAllTotal'", TVRobotoBold.class);
    target.etInstallationFee = Utils.findRequiredViewAsType(source, R.id.et_installation_fee, "field 'etInstallationFee'", ETRobotoRegular.class);
    target.etTransportFee = Utils.findRequiredViewAsType(source, R.id.et_transport_fee, "field 'etTransportFee'", ETRobotoRegular.class);
    target.etRemovableFee = Utils.findRequiredViewAsType(source, R.id.et_removable_fee, "field 'etRemovableFee'", ETRobotoRegular.class);
    target.etCleaningFee = Utils.findRequiredViewAsType(source, R.id.et_cleaning_fee, "field 'etCleaningFee'", ETRobotoRegular.class);
    target.etOtherFee = Utils.findRequiredViewAsType(source, R.id.et_other_fee, "field 'etOtherFee'", ETRobotoRegular.class);
    target.etDiscountPer = Utils.findRequiredViewAsType(source, R.id.et_discount_per, "field 'etDiscountPer'", ETRobotoRegular.class);
    target.txSubmitQuote = Utils.findRequiredViewAsType(source, R.id.tx_submit_quote, "field 'txSubmitQuote'", TVRobotoBold.class);
    target.et_search = Utils.findRequiredViewAsType(source, R.id.et_search, "field 'et_search'", EditText.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    QuoteActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.toolbar = null;
    target.roomList = null;
    target.buBehaviorChange = null;
    target.lyBottomView = null;
    target.imBehaviorChange = null;
    target.cdSubmitQuote = null;
    target.txAllTotal = null;
    target.etInstallationFee = null;
    target.etTransportFee = null;
    target.etRemovableFee = null;
    target.etCleaningFee = null;
    target.etOtherFee = null;
    target.etDiscountPer = null;
    target.txSubmitQuote = null;
    target.et_search = null;
  }
}
