// Generated code from Butter Knife. Do not modify!
package com.agtapp.lightandshade.adapter;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.support.v7.widget.CardView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.agtapp.lightandshade.R;
import com.agtapp.lightandshade.util.edit_text.ETRobotoBold;
import com.agtapp.lightandshade.util.text_view.TVRobotoBold;
import com.agtapp.lightandshade.util.text_view.TVRobotoRegular;
import java.lang.IllegalStateException;
import java.lang.Override;

public class WallsListAdapter$ViewHolder_ViewBinding implements Unbinder {
  private WallsListAdapter.ViewHolder target;

  @UiThread
  public WallsListAdapter$ViewHolder_ViewBinding(WallsListAdapter.ViewHolder target, View source) {
    this.target = target;

    target.etWallName = Utils.findRequiredViewAsType(source, R.id.et_wall_name, "field 'etWallName'", ETRobotoBold.class);
    target.wallProductImageLoad = Utils.findRequiredViewAsType(source, R.id.wall_product_image_load, "field 'wallProductImageLoad'", ProgressBar.class);
    target.imWallProductImage = Utils.findRequiredViewAsType(source, R.id.im_wall_product_image, "field 'imWallProductImage'", ImageView.class);
    target.txWallProductId = Utils.findRequiredViewAsType(source, R.id.tx_wall_product_id, "field 'txWallProductId'", TVRobotoRegular.class);
    target.txWallProductTitle = Utils.findRequiredViewAsType(source, R.id.tx_wall_product_title, "field 'txWallProductTitle'", TVRobotoBold.class);
    target.txWallArea = Utils.findRequiredViewAsType(source, R.id.tx_wall_area, "field 'txWallArea'", TVRobotoBold.class);
    target.txWallPrice = Utils.findRequiredViewAsType(source, R.id.tx_wall_price, "field 'txWallPrice'", TVRobotoBold.class);
    target.cdAddProduct = Utils.findRequiredViewAsType(source, R.id.cd_add_product, "field 'cdAddProduct'", CardView.class);
    target.txCaptureImage = Utils.findRequiredViewAsType(source, R.id.tx_capture_image, "field 'txCaptureImage'", TVRobotoBold.class);
    target.cdCaptureImage = Utils.findRequiredViewAsType(source, R.id.cd_capture_image, "field 'cdCaptureImage'", CardView.class);
    target.lyEditView = Utils.findRequiredViewAsType(source, R.id.ly_edit_view, "field 'lyEditView'", LinearLayout.class);
    target.imWallAddImage = Utils.findRequiredViewAsType(source, R.id.im_wall_add_image, "field 'imWallAddImage'", ImageView.class);
    target.imWallDelete = Utils.findRequiredViewAsType(source, R.id.bu_wall_delete, "field 'imWallDelete'", ImageView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    WallsListAdapter.ViewHolder target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.etWallName = null;
    target.wallProductImageLoad = null;
    target.imWallProductImage = null;
    target.txWallProductId = null;
    target.txWallProductTitle = null;
    target.txWallArea = null;
    target.txWallPrice = null;
    target.cdAddProduct = null;
    target.txCaptureImage = null;
    target.cdCaptureImage = null;
    target.lyEditView = null;
    target.imWallAddImage = null;
    target.imWallDelete = null;
  }
}
