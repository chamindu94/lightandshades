// Generated code from Butter Knife. Do not modify!
package com.agtapp.lightandshade;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.LinearLayout;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.agtapp.lightandshade.util.text_view.TVRobotoRegular;
import java.lang.IllegalStateException;
import java.lang.Override;

public class InquiriesActivity_ViewBinding implements Unbinder {
  private InquiriesActivity target;

  @UiThread
  public InquiriesActivity_ViewBinding(InquiriesActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public InquiriesActivity_ViewBinding(InquiriesActivity target, View source) {
    this.target = target;

    target.toolbar = Utils.findRequiredViewAsType(source, R.id.toolbar, "field 'toolbar'", Toolbar.class);
    target.list = Utils.findRequiredViewAsType(source, R.id.list, "field 'list'", RecyclerView.class);
    target.txShortTitle = Utils.findRequiredViewAsType(source, R.id.tx_short_title, "field 'txShortTitle'", TVRobotoRegular.class);
    target.lyShort = Utils.findRequiredViewAsType(source, R.id.ly_short, "field 'lyShort'", LinearLayout.class);
    target.txSelectedDate = Utils.findRequiredViewAsType(source, R.id.tx_selected_date, "field 'txSelectedDate'", TVRobotoRegular.class);
    target.lyDate = Utils.findRequiredViewAsType(source, R.id.ly_date, "field 'lyDate'", LinearLayout.class);
    target.swipeRefreshLayout = Utils.findRequiredViewAsType(source, R.id.swipe_refresh_layout, "field 'swipeRefreshLayout'", SwipeRefreshLayout.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    InquiriesActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.toolbar = null;
    target.list = null;
    target.txShortTitle = null;
    target.lyShort = null;
    target.txSelectedDate = null;
    target.lyDate = null;
    target.swipeRefreshLayout = null;
  }
}
