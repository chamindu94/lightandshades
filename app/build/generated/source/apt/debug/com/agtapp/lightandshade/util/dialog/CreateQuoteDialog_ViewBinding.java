// Generated code from Butter Knife. Do not modify!
package com.agtapp.lightandshade.util.dialog;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.support.v7.widget.CardView;
import android.view.View;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.agtapp.lightandshade.R;
import com.agtapp.lightandshade.util.edit_text.ETRobotoRegular;
import com.agtapp.lightandshade.util.text_view.TVRobotoBold;
import java.lang.IllegalStateException;
import java.lang.Override;

public class CreateQuoteDialog_ViewBinding implements Unbinder {
  private CreateQuoteDialog target;

  @UiThread
  public CreateQuoteDialog_ViewBinding(CreateQuoteDialog target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public CreateQuoteDialog_ViewBinding(CreateQuoteDialog target, View source) {
    this.target = target;

    target.txTitle = Utils.findRequiredViewAsType(source, R.id.tx_title, "field 'txTitle'", TVRobotoBold.class);
    target.etHeight = Utils.findRequiredViewAsType(source, R.id.et_height, "field 'etHeight'", ETRobotoRegular.class);
    target.etWidth = Utils.findRequiredViewAsType(source, R.id.et_width, "field 'etWidth'", ETRobotoRegular.class);
    target.etRedHeight = Utils.findRequiredViewAsType(source, R.id.et_red_height, "field 'etRedHeight'", ETRobotoRegular.class);
    target.etRedWidth = Utils.findRequiredViewAsType(source, R.id.et_red_width, "field 'etRedWidth'", ETRobotoRegular.class);
    target.etNoWalls = Utils.findRequiredViewAsType(source, R.id.et_no_walls, "field 'etNoWalls'", ETRobotoRegular.class);
    target.cdCancel = Utils.findRequiredViewAsType(source, R.id.cd_cancel, "field 'cdCancel'", CardView.class);
    target.cdAddItemQuote = Utils.findRequiredViewAsType(source, R.id.cd_add_item_quote, "field 'cdAddItemQuote'", CardView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    CreateQuoteDialog target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.txTitle = null;
    target.etHeight = null;
    target.etWidth = null;
    target.etRedHeight = null;
    target.etRedWidth = null;
    target.etNoWalls = null;
    target.cdCancel = null;
    target.cdAddItemQuote = null;
  }
}
