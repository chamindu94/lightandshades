// Generated code from Butter Knife. Do not modify!
package com.agtapp.lightandshade;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.agtapp.lightandshade.util.CircleImageView;
import com.agtapp.lightandshade.util.text_view.TVRobotoBold;
import com.agtapp.lightandshade.util.text_view.TVRobotoRegular;
import java.lang.IllegalStateException;
import java.lang.Override;

public class MainActivity_ViewBinding implements Unbinder {
  private MainActivity target;

  @UiThread
  public MainActivity_ViewBinding(MainActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public MainActivity_ViewBinding(MainActivity target, View source) {
    this.target = target;

    target.toolbar = Utils.findRequiredViewAsType(source, R.id.toolbar, "field 'toolbar'", Toolbar.class);
    target.txName = Utils.findRequiredViewAsType(source, R.id.tx_name, "field 'txName'", TVRobotoBold.class);
    target.txJobCount = Utils.findRequiredViewAsType(source, R.id.tx_job_count, "field 'txJobCount'", TVRobotoBold.class);
    target.cdJobCount = Utils.findRequiredViewAsType(source, R.id.cd_job_count, "field 'cdJobCount'", CardView.class);
    target.navigationView = Utils.findRequiredViewAsType(source, R.id.navigation_view, "field 'navigationView'", NavigationView.class);
    target.drawerLayout = Utils.findRequiredViewAsType(source, R.id.drawer_layout, "field 'drawerLayout'", DrawerLayout.class);
    target.navImageLoad = Utils.findRequiredViewAsType(source, R.id.nav_image_load, "field 'navImageLoad'", ProgressBar.class);
    target.navProfile = Utils.findRequiredViewAsType(source, R.id.nav_profile, "field 'navProfile'", CircleImageView.class);
    target.navTxUserName = Utils.findRequiredViewAsType(source, R.id.nav_tx_user_name, "field 'navTxUserName'", TVRobotoRegular.class);
    target.navTxUserEmail = Utils.findRequiredViewAsType(source, R.id.nav_tx_user_email, "field 'navTxUserEmail'", TVRobotoRegular.class);
    target.navProduct = Utils.findRequiredViewAsType(source, R.id.nav_product, "field 'navProduct'", LinearLayout.class);
    target.navLogout = Utils.findRequiredViewAsType(source, R.id.nav_logout, "field 'navLogout'", LinearLayout.class);
    target.swipeRefreshLayout = Utils.findRequiredViewAsType(source, R.id.swipe_refresh_layout, "field 'swipeRefreshLayout'", SwipeRefreshLayout.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    MainActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.toolbar = null;
    target.txName = null;
    target.txJobCount = null;
    target.cdJobCount = null;
    target.navigationView = null;
    target.drawerLayout = null;
    target.navImageLoad = null;
    target.navProfile = null;
    target.navTxUserName = null;
    target.navTxUserEmail = null;
    target.navProduct = null;
    target.navLogout = null;
    target.swipeRefreshLayout = null;
  }
}
