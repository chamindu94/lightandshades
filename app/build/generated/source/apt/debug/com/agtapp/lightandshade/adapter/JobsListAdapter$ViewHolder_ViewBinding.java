// Generated code from Butter Knife. Do not modify!
package com.agtapp.lightandshade.adapter;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.agtapp.lightandshade.R;
import com.agtapp.lightandshade.util.text_view.TVRobotoRegular;
import java.lang.IllegalStateException;
import java.lang.Override;

public class JobsListAdapter$ViewHolder_ViewBinding implements Unbinder {
  private JobsListAdapter.ViewHolder target;

  @UiThread
  public JobsListAdapter$ViewHolder_ViewBinding(JobsListAdapter.ViewHolder target, View source) {
    this.target = target;

    target.txInquireId = Utils.findRequiredViewAsType(source, R.id.tx_inquire_id, "field 'txInquireId'", TVRobotoRegular.class);
    target.txNameAndRole = Utils.findRequiredViewAsType(source, R.id.tx_name_and_role, "field 'txNameAndRole'", TVRobotoRegular.class);
    target.txDate = Utils.findRequiredViewAsType(source, R.id.tx_date, "field 'txDate'", TVRobotoRegular.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    JobsListAdapter.ViewHolder target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.txInquireId = null;
    target.txNameAndRole = null;
    target.txDate = null;
  }
}
