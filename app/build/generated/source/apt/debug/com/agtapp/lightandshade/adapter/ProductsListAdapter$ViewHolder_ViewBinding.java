// Generated code from Butter Knife. Do not modify!
package com.agtapp.lightandshade.adapter;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.support.v7.widget.CardView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.agtapp.lightandshade.R;
import com.agtapp.lightandshade.util.text_view.TVRobotoBold;
import com.agtapp.lightandshade.util.text_view.TVRobotoRegular;
import java.lang.IllegalStateException;
import java.lang.Override;

public class ProductsListAdapter$ViewHolder_ViewBinding implements Unbinder {
  private ProductsListAdapter.ViewHolder target;

  @UiThread
  public ProductsListAdapter$ViewHolder_ViewBinding(ProductsListAdapter.ViewHolder target,
      View source) {
    this.target = target;

    target.imageLoad = Utils.findRequiredViewAsType(source, R.id.image_load, "field 'imageLoad'", ProgressBar.class);
    target.imImage = Utils.findRequiredViewAsType(source, R.id.im_image, "field 'imImage'", ImageView.class);
    target.txTitle = Utils.findRequiredViewAsType(source, R.id.tx_title, "field 'txTitle'", TVRobotoBold.class);
    target.txUnit = Utils.findRequiredViewAsType(source, R.id.tx_unit, "field 'txUnit'", TVRobotoRegular.class);
    target.txPrice = Utils.findRequiredViewAsType(source, R.id.tx_price, "field 'txPrice'", TVRobotoRegular.class);
    target.cdMore = Utils.findRequiredViewAsType(source, R.id.cd_more, "field 'cdMore'", CardView.class);
    target.cdAddQuote = Utils.findRequiredViewAsType(source, R.id.cd_add_quote, "field 'cdAddQuote'", CardView.class);
    target.lyProcess = Utils.findRequiredViewAsType(source, R.id.ly_process, "field 'lyProcess'", LinearLayout.class);
    target.cdNormalMore = Utils.findRequiredViewAsType(source, R.id.cd_normal_more, "field 'cdNormalMore'", CardView.class);
    target.lyNormal = Utils.findRequiredViewAsType(source, R.id.ly_normal, "field 'lyNormal'", LinearLayout.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    ProductsListAdapter.ViewHolder target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.imageLoad = null;
    target.imImage = null;
    target.txTitle = null;
    target.txUnit = null;
    target.txPrice = null;
    target.cdMore = null;
    target.cdAddQuote = null;
    target.lyProcess = null;
    target.cdNormalMore = null;
    target.lyNormal = null;
  }
}
