// Generated code from Butter Knife. Do not modify!
package com.agtapp.lightandshade.adapter;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.support.v7.widget.CardView;
import android.view.View;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.agtapp.lightandshade.R;
import com.agtapp.lightandshade.util.text_view.TVRobotoRegular;
import java.lang.IllegalStateException;
import java.lang.Override;

public class InquireDetailsQuotationListAdapter$ViewHolder_ViewBinding implements Unbinder {
  private InquireDetailsQuotationListAdapter.ViewHolder target;

  @UiThread
  public InquireDetailsQuotationListAdapter$ViewHolder_ViewBinding(InquireDetailsQuotationListAdapter.ViewHolder target,
      View source) {
    this.target = target;

    target.txQuoteType = Utils.findRequiredViewAsType(source, R.id.tx_quote_type, "field 'txQuoteType'", TVRobotoRegular.class);
    target.cdQuoteType = Utils.findRequiredViewAsType(source, R.id.cd_quote_type, "field 'cdQuoteType'", CardView.class);
    target.txQuoteId = Utils.findRequiredViewAsType(source, R.id.tx_quote_id, "field 'txQuoteId'", TVRobotoRegular.class);
    target.txDate = Utils.findRequiredViewAsType(source, R.id.tx_date, "field 'txDate'", TVRobotoRegular.class);
    target.txTime = Utils.findRequiredViewAsType(source, R.id.tx_time, "field 'txTime'", TVRobotoRegular.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    InquireDetailsQuotationListAdapter.ViewHolder target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.txQuoteType = null;
    target.cdQuoteType = null;
    target.txQuoteId = null;
    target.txDate = null;
    target.txTime = null;
  }
}
