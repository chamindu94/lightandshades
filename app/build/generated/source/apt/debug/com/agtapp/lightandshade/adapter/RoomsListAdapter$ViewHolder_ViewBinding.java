// Generated code from Butter Knife. Do not modify!
package com.agtapp.lightandshade.adapter;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.agtapp.lightandshade.R;
import com.agtapp.lightandshade.util.edit_text.ETRobotoBold;
import com.agtapp.lightandshade.util.edit_text.ETRobotoRegular;
import com.agtapp.lightandshade.util.text_view.TVRobotoBold;
import java.lang.IllegalStateException;
import java.lang.Override;

public class RoomsListAdapter$ViewHolder_ViewBinding implements Unbinder {
  private RoomsListAdapter.ViewHolder target;

  @UiThread
  public RoomsListAdapter$ViewHolder_ViewBinding(RoomsListAdapter.ViewHolder target, View source) {
    this.target = target;

    target.buRoomAdd = Utils.findRequiredViewAsType(source, R.id.bu_room_add, "field 'buRoomAdd'", ImageView.class);
    target.etRoomName = Utils.findRequiredViewAsType(source, R.id.et_room_name, "field 'etRoomName'", ETRobotoBold.class);
    target.txRoomTotal = Utils.findRequiredViewAsType(source, R.id.tx_room_total, "field 'txRoomTotal'", TVRobotoBold.class);
    target.etRoomDes = Utils.findRequiredViewAsType(source, R.id.et_room_des, "field 'etRoomDes'", ETRobotoRegular.class);
    target.wallList = Utils.findRequiredViewAsType(source, R.id.wall_list, "field 'wallList'", RecyclerView.class);
    target.buRoomDelete = Utils.findRequiredViewAsType(source, R.id.bu_room_delete, "field 'buRoomDelete'", ImageView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    RoomsListAdapter.ViewHolder target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.buRoomAdd = null;
    target.etRoomName = null;
    target.txRoomTotal = null;
    target.etRoomDes = null;
    target.wallList = null;
    target.buRoomDelete = null;
  }
}
