/*
 * Copyright (c) 2017. A.G.THAMAYS <gthamays@gmail.com>
 * This file is part of Light & Shade.
 * Light & Shade can not be copied and/or distribute without the express
 * permission of A.G.THAMAYS
 */

package com.agtapp.lightandshade;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomSheetBehavior;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.agtapp.lightandshade.adapter.RoomsListAdapter;
import com.agtapp.lightandshade.model.Product;
import com.agtapp.lightandshade.util.Helper;
import com.agtapp.lightandshade.util.ImagePicker;
import com.agtapp.lightandshade.util.Loading;
import com.agtapp.lightandshade.util.MySharedPreferences;
import com.agtapp.lightandshade.util.MyToast;
import com.agtapp.lightandshade.util.dialog.CreateQuoteDialog;
import com.agtapp.lightandshade.util.edit_text.ETRobotoRegular;
import com.agtapp.lightandshade.util.interfaces.QuoteListTextChange;
import com.agtapp.lightandshade.util.interfaces.item_click_listener.WallItemImageClickListener;
import com.agtapp.lightandshade.util.text_view.TVRobotoBold;
import com.agtapp.lightandshade.web_service.ApiClient;
import com.agtapp.lightandshade.web_service.ApiInterface;
import com.agtapp.lightandshade.web_service.request.QuoteRequest;
import com.agtapp.lightandshade.web_service.response.QuoteResponse;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.text.DecimalFormat;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.agtapp.lightandshade.util.interfaces.QuoteActivityConstances.EDITABLE;
import static com.agtapp.lightandshade.util.interfaces.QuoteActivityConstances.INQUIRE_ID;
import static com.agtapp.lightandshade.util.interfaces.QuoteActivityConstances.IN_THE_ACTIVITY;
import static com.agtapp.lightandshade.util.interfaces.QuoteActivityConstances.PRODUCT;

public class QuoteActivity extends AppCompatActivity implements WallItemImageClickListener, QuoteListTextChange {

    private static final String TAG = "QuoteActivity";
    private static final int PICK_IMAGE_ID = 234;
    private String wallName = "";
    private String roomName = "";
    private String roomDisctiption = "";
    private boolean state = true;
    private DecimalFormat decimalFormat = new DecimalFormat("###,##0.00");
    private static final int UPDATE_CODE = 10001;

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.room_list)
    RecyclerView roomList;
    @BindView(R.id.bu_behavior_change)
    LinearLayout buBehaviorChange;
    @BindView(R.id.ly_bottom_view)
    NestedScrollView lyBottomView;
    @BindView(R.id.im_behavior_change)
    ImageView imBehaviorChange;
    @BindView(R.id.cd_submit_quote)
    CardView cdSubmitQuote;
    @BindView(R.id.tx_all_total)
    TVRobotoBold txAllTotal;
    @BindView(R.id.et_installation_fee)
    ETRobotoRegular etInstallationFee;
    @BindView(R.id.et_transport_fee)
    ETRobotoRegular etTransportFee;
    @BindView(R.id.et_removable_fee)
    ETRobotoRegular etRemovableFee;
    @BindView(R.id.et_cleaning_fee)
    ETRobotoRegular etCleaningFee;
    @BindView(R.id.et_other_fee)
    ETRobotoRegular etOtherFee;
    @BindView(R.id.et_discount_per)
    ETRobotoRegular etDiscountPer;
    @BindView(R.id.tx_submit_quote)
    TVRobotoBold txSubmitQuote;
    @BindView(R.id.et_search)
    EditText et_search;

    private Context context;
    private Loading loading;
    private MySharedPreferences sharedPreferences;
    private Product product;
    private float room_total = 0, TransportFee = 0, RemovableFee = 0, OtherFee = 0, CleaningFee = 0, InstallationFee = 0, DiscountPer = 0, allocatedDis = 0;
    private String inquire_id;
    private WallItemImageClickListener wallItemImageClickListener;
    private QuoteListTextChange quoteListTextChange;
    private int room_position, wall_position;
    private boolean editable;
    private int quote_id, process_type;
    private Menu menu;
    String webResponse;
    private MainApplication application;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quote);
        ButterKnife.bind(this);

        context = this;
        sharedPreferences = new MySharedPreferences(this);
        loading = new Loading(context);
        wallItemImageClickListener = this;
        quoteListTextChange = this;

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Toast.makeText(QuoteActivity.this, "Please submit quote", Toast.LENGTH_LONG).show();
                finish();
            }
        });

        application = (MainApplication) getApplication();

        if (!sharedPreferences.getQuotValues(IN_THE_ACTIVITY).equals("1")) {
            product = new Gson().fromJson(getIntent().getStringExtra("PRODUCT"), Product.class);
            inquire_id = getIntent().getStringExtra("INQUIRE_ID");
            editable = getIntent().getBooleanExtra("EDITABLE", true);

        } else {
            product = new Gson().fromJson(sharedPreferences.getQuotValues(PRODUCT), Product.class);
            inquire_id = sharedPreferences.getQuotValues(INQUIRE_ID);
            editable = sharedPreferences.getQuotValuesBool(EDITABLE);
        }

        //        1 - first create, 2 -
        process_type = getIntent().getIntExtra("PROCESS_TYPE", 1);
        quote_id = getIntent().getIntExtra("QUOTE_ID", 0);

        getUI();

        roomList.setLayoutManager(new LinearLayoutManager(context));
        roomList.setHasFixedSize(true);
        roomList.setLayoutManager(new GridLayoutManager(context, 1, GridLayoutManager.VERTICAL, false));

        final BottomSheetBehavior bottomSheetBehavior = BottomSheetBehavior.from(lyBottomView);
        bottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
        imBehaviorChange.setImageDrawable(getResources().getDrawable(R.drawable.ic_up_arrow));

        bottomSheetBehavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {
                if (bottomSheetBehavior.getState() == BottomSheetBehavior.STATE_COLLAPSED) {
                    imBehaviorChange.setImageDrawable(getResources().getDrawable(R.drawable.ic_up_arrow));
                }

                if (bottomSheetBehavior.getState() == BottomSheetBehavior.STATE_EXPANDED) {
                    imBehaviorChange.setImageDrawable(getResources().getDrawable(R.drawable.ic_down_arrow));
                }
            }

            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {

            }
        });

        buBehaviorChange.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (bottomSheetBehavior.getState() == BottomSheetBehavior.STATE_COLLAPSED) {
                    imBehaviorChange.setImageDrawable(getResources().getDrawable(R.drawable.ic_down_arrow));
                    bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                }

                if (bottomSheetBehavior.getState() == BottomSheetBehavior.STATE_EXPANDED) {
                    imBehaviorChange.setImageDrawable(getResources().getDrawable(R.drawable.ic_up_arrow));
                    bottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
                }

                if (bottomSheetBehavior.getState() == BottomSheetBehavior.STATE_HIDDEN) {
                    bottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
                }

            }
        });

        roomList.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                bottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
            }

            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                bottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
                super.onScrollStateChanged(recyclerView, newState);
            }
        });

        if (!editable) {
            getDetails(quote_id);
        } else {
            getDetails();
        }

        etCleaningFee.addTextChangedListener(watcher);
        etInstallationFee.addTextChangedListener(watcher);
        etOtherFee.addTextChangedListener(watcher);
        etRemovableFee.addTextChangedListener(watcher);
        etTransportFee.addTextChangedListener(watcher);
        etDiscountPer.addTextChangedListener(watcher);

        cdSubmitQuote.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                sharedPreferences.setQuotValues(IN_THE_ACTIVITY, "0");

                showDialogUserValueUpdate();


//                if(TransportFee!=0 && RemovableFee!=0 && OtherFee !=0 && CleaningFee!=0 && InstallationFee != 0 && DiscountPer !=0 ){
//                    showDialogUserValueUpdate();
//                }else{
//                    new MyToast(context, "Please Fill Values");
//                }


            }
        });

        et_search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }

    @Override
    public void onBackPressed() {
        sharedPreferences.setQuotValues(IN_THE_ACTIVITY, "1");
        sharedPreferences.setQuotValues(PRODUCT, new Gson().toJson(product));
        sharedPreferences.setQuotValues(INQUIRE_ID, inquire_id);
        sharedPreferences.setQuotValues(EDITABLE, editable);

//        Toast.makeText(this, "Please submit quote", Toast.LENGTH_LONG).show();
        finish();
    }

    private void setQuote(QuoteResponse quoteResponse) {
        loading.show();

        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<JsonObject> call = apiService.setQuote("Bearer " + sharedPreferences.getToken(), new QuoteResponse(quoteResponse.getStatus(), quoteResponse.getCode(), quoteResponse.getMessage(), quoteResponse.getData()));
        Log.e(TAG, "onResponse: " + quoteResponse.getData().toString());


        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {

//                Log.e(TAG, "onResponse: " + response);
//
//                Log.e(TAG, "onResponse: " + response.body());

                if (response.body().getAsJsonPrimitive("status") != null) {
                    if (response.body().getAsJsonPrimitive("status").getAsString().equals("success")) {
                        new MyToast(context, response.body().getAsJsonPrimitive("message").getAsString());
                        new MyToast(context, "Email has been successfully sent.");

                        Intent intent = new Intent(context, InquireDetailActivity.class);
                        intent.putExtra("INQUIRE_ID", inquire_id);
                        startActivity(intent);
                        finish();
                    }
                } else {
                    try {
                        Log.i(TAG, "onResponse: " + response.errorBody().string());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }

                loading.cancel();
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                Log.e(TAG, t.toString());
                loading.cancel();
            }
        });
    }

    private void getDetails(int id) {
        loading.show();

        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<JsonObject> call = apiService.getQuote("Bearer " + sharedPreferences.getToken(), new QuoteRequest(String.valueOf(id)));
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> json) {
                webResponse = json.body().toString();
                QuoteResponse response = new Gson().fromJson(json.body(), QuoteResponse.class);
                if (response.getStatus().equals("success")) {

                    room_total = 0;
                    for (int i = 0; i < response.getData().getRoom().size(); i++) {
                        float wall_total = 0;
                        for (int j = 0; j < response.getData().getRoom().get(i).getWalls().size(); j++) {
                            wall_total += Float.valueOf(response.getData().getRoom().get(i).getWalls().get(j).getWallCost());
                        }
                        room_total += wall_total;
                    }

                    product = new Product();
                    product.setImage(response.getData().getRoom().get(0).getWalls().get(0).getProductImage());
                    product.setId(response.getData().getRoom().get(0).getWalls().get(0).getProductId());
                    product.setProductId(String.valueOf(response.getData().getRoom().get(0).getWalls().get(0).getProductId()));
                    product.setName(response.getData().getRoom().get(0).getWalls().get(0).getProductName());
//                    product.setBrand(response.getData().getRoom().get(0).getWalls().get(0).getProductImage());
//                    product.setBrandId(response.getData().getRoom().get(0).getWalls().get(0).getProductImage());
//                    product.setCategory(response.getData().getRoom().get(0).getWalls().get(0).getProductImage());
//                    product.setCategoryId(response.getData().getRoom().get(0).getWalls().get(0).getProductImage());
//                    product.setDescription("");
                    product.setProductPriceCountry("LKR");
                    product.setUnitPrice(response.getData().getRoom().get(0).getWalls().get(0).getProductUnitPrice());
                    product.setUom("Sq.Feet");
                    Double dvalue = Double.parseDouble(response.getData().getTotalCost());
                    String value1 = decimalFormat.format(dvalue);
                    txAllTotal.setText(response.getData().getTotalCostCountry() + " " + value1);
                    etCleaningFee.setText(response.getData().getCleaningFee());
                    etDiscountPer.setText(response.getData().getDiscount());
                    etInstallationFee.setText(response.getData().getInstallationFee());
                    etRemovableFee.setText(response.getData().getRemovableFee());
                    etTransportFee.setText(response.getData().getTransportFee());
                    etOtherFee.setText(response.getData().getOtherFee());

                   allocatedDis = Float.valueOf(response.getData().getDiscount());



                    RoomsListAdapter adapter = new RoomsListAdapter(editable, response.getData().getRoom(), context, product, inquire_id, wallItemImageClickListener, quoteListTextChange);
                    roomList.setAdapter(adapter);

                } else {
                    if (response.getCode() == 401) {
                        sharedPreferences.logout(context);
                    } else {
                        new MyToast(context, response.getMessage());
                    }
                }

                loading.cancel();

            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                Log.e(TAG, t.toString());
                loading.cancel();
            }
        });
    }

    private void getDetails() {
        QuoteResponse response = new Gson().fromJson(Helper.jsonFileRead(context, "first_quote.json"), QuoteResponse.class);
        if (response.getStatus().equals("success")) {

            room_total = 0;
            for (int i = 0; i < response.getData().getRoom().size(); i++) {
                float wall_total = 0;
                for (int j = 0; j < response.getData().getRoom().get(i).getWalls().size(); j++) {
                    wall_total += Float.valueOf(response.getData().getRoom().get(i).getWalls().get(j).getWallCost());
                }
                room_total += wall_total;
            }

            String value1 = decimalFormat.format(room_total);
            txAllTotal.setText("" + value1);

            RoomsListAdapter adapter = new RoomsListAdapter(editable, response.getData().getRoom(), context, product, inquire_id, wallItemImageClickListener, quoteListTextChange);
            roomList.setAdapter(adapter);
            state = false;
        } else {
            new MyToast(context, response.getMessage());
        }
    }

    private void getUI() {
        etCleaningFee.setEnabled(editable);
        etDiscountPer.setEnabled(editable);
        etInstallationFee.setEnabled(editable);
        etOtherFee.setEnabled(editable);
        etRemovableFee.setEnabled(editable);
        etTransportFee.setEnabled(editable);

        if (editable) {
            cdSubmitQuote.setVisibility(View.VISIBLE);
            if (quote_id != 0)
                txSubmitQuote.setText("Save Quote");
            else
                txSubmitQuote.setText("Submit Quote");
        } else {
            cdSubmitQuote.setVisibility(View.GONE);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_quote, menu);
        this.menu = menu;
        menu.findItem(R.id.action_add_room).setVisible(editable);
        menu.findItem(R.id.action_edit).setVisible(!editable);
        menu.findItem(R.id.action_delete).setVisible(!editable);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_add_room) {
//            Intent intent = new Intent(context, ProductsActivity.class);
//            intent.putExtra("INQUIRE_ID", inquire_id);
//            startActivity(intent);
            new CreateQuoteDialog(context, 3, product, 0, inquire_id, 0).show();

            return true;
        }

        if (id == R.id.action_edit) {
            Helper.jsonFileWrite(context, webResponse, "first_quote.json");

            Intent intent = new Intent(context, QuoteActivity.class);
            intent.putExtra("PRODUCT", (new Gson()).toJson(product));
            intent.putExtra("EDITABLE", true);
            intent.putExtra("INQUIRE_ID", inquire_id);
            intent.putExtra("QUOTE_ID", quote_id);
            startActivity(intent);
            finish();

            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    TextWatcher watcher = new TextWatcher() {
        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            //YOUR CODE
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            //YOUR CODE
        }

        @Override
        public void afterTextChanged(Editable s) {

            if (!TextUtils.isEmpty(etTransportFee.getText().toString()))
                TransportFee = Float.parseFloat(etTransportFee.getText().toString());

            if (!TextUtils.isEmpty(etRemovableFee.getText().toString()))
                RemovableFee = Float.parseFloat(etRemovableFee.getText().toString());

            if (!TextUtils.isEmpty(etOtherFee.getText().toString()))
                OtherFee = Float.parseFloat(etOtherFee.getText().toString());

            if (!TextUtils.isEmpty(etCleaningFee.getText().toString()))
                CleaningFee = Float.parseFloat(etCleaningFee.getText().toString());

            if (!TextUtils.isEmpty(etInstallationFee.getText().toString()))
                InstallationFee = Float.parseFloat(etInstallationFee.getText().toString());

            if (!TextUtils.isEmpty(etDiscountPer.getText().toString()) && Float.valueOf(etDiscountPer.getText().toString()) < application.me.getDiscountRange() ){

                DiscountPer = Float.parseFloat(etDiscountPer.getText().toString());
            }else{
                DiscountPer = 0;
                new MyToast(context, "Discount Cannot Exceed Allocated Range");
            }

            float value = (room_total + TransportFee + RemovableFee + OtherFee + CleaningFee + InstallationFee) -
                    ((room_total + TransportFee + RemovableFee + OtherFee + CleaningFee + InstallationFee) * DiscountPer / 100);
            String value1 = decimalFormat.format(value);
            txAllTotal.setText("" + value1);
        }
    };

    @Override
    public void imageCapture(View v, int wall_position, int room_position) {
        this.wall_position = wall_position;
        this.room_position = room_position;
        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());
        Intent chooseImageIntent = ImagePicker.getPickImageIntent(context);
        startActivityForResult(chooseImageIntent, PICK_IMAGE_ID);
    }

    @Override
    public void addQuote(View v, int position, int room_position) {

            Intent intent = new Intent(context, ProductsActivity.class);
            intent.putExtra("INQUIRE_ID", inquire_id);
            intent.putExtra("update", true);
            intent.putExtra("wallPosition", position);
            intent.putExtra("roomPosition", room_position);
            startActivityForResult(intent, UPDATE_CODE);


    }

    @Override
    public void wallNameChange(int position, int room_position, String charSeq) {
        wallName = charSeq;
        if (state) {

        } else {
            setData("wallname", room_position, position);
        }
    }

    @Override
    public void deleteWall(int position, int room_position) {
        deleteData("walldelete", room_position, position);
    }

    @Override
    public void viewImageCapture(Bitmap v, int position, int room_position) {
        showDialogImageZoomView(v);
    }

    @Override
    public void deleteRoom(int room_position) {
        deleteData("roomdelete", room_position, -1);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case PICK_IMAGE_ID:
                Bitmap bitmap = ImagePicker.getImageFromResult(context, resultCode, data);

                Log.i(TAG, "onActivityResult: " + bitmap);

                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();

                if (bitmap != null) {
                    bitmap.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
                    byte[] byteArray = byteArrayOutputStream.toByteArray();

                    String encoded = Base64.encodeToString(byteArray, Base64.DEFAULT);

                    Log.i(TAG, "onActivityResult: " + encoded);

                    try {
                        QuoteResponse response = new Gson().fromJson(Helper.jsonFileRead(context, "first_quote.json"), QuoteResponse.class);

                        JSONObject mainObject = new JSONObject();
                        mainObject.put("status", "success");
                        mainObject.put("code", 200);
                        mainObject.put("message", "success");

                        JSONObject dataObject = new JSONObject();
                        JSONArray roomArray = new JSONArray();

                        for (int j = 0; j < response.getData().getRoom().size(); j++) {

                            JSONObject roomObject = new JSONObject();
                            roomObject.put("name", response.getData().getRoom().get(j).getName());
                            roomObject.put("description", response.getData().getRoom().get(j).getDescription());
                            roomObject.put("total_price", response.getData().getRoom().get(j).getTotalPrice());
                            roomObject.put("total_price_country", response.getData().getRoom().get(j).getTotalPriceCountry());

                            JSONArray wallArray = new JSONArray();

                            for (int i = 0; i < response.getData().getRoom().get(j).getWalls().size(); i++) {

                                JSONObject wallObject = new JSONObject();

                                wallObject.put("name", response.getData().getRoom().get(j).getWalls().get(i).getName());//
                                wallObject.put("product_id", response.getData().getRoom().get(j).getWalls().get(i).getProductId());
                                wallObject.put("product_code", response.getData().getRoom().get(j).getWalls().get(i).getProductCode());
                                wallObject.put("product_image", response.getData().getRoom().get(j).getWalls().get(i).getProductImage());
                                wallObject.put("product_name", response.getData().getRoom().get(j).getWalls().get(i).getProductName());
                                wallObject.put("wall_area", response.getData().getRoom().get(j).getWalls().get(i).getWallArea());
                                wallObject.put("wall_area_unit", 1);
                                wallObject.put("wall_cost", response.getData().getRoom().get(j).getWalls().get(i).getWallCost());
                                wallObject.put("wall_cost_country", response.getData().getRoom().get(j).getWalls().get(i).getWallCostCountry());

                                JSONArray imagesArray = new JSONArray();
                                if (response.getData().getRoom().get(j).getWalls().get(i).getWallImage() != null) {
                                    imagesArray.put(response.getData().getRoom().get(j).getWalls().get(i).getWallImage().get(0));
                                }
                                if (j == room_position && i == wall_position) {
                                    imagesArray.put(0, encoded);
                                }

                                wallObject.put("wall_image", imagesArray);

                                wallArray.put(wallObject);
                            }

                            roomObject.put("walls", wallArray);

                            roomArray.put(roomObject);

                        }

                        dataObject.put("rooms", roomArray);

                        dataObject.put("total_cost", txAllTotal.getText().toString());
                        dataObject.put("total_cost_country", product.getProductPriceCountry());
                        dataObject.put("installation_fee", InstallationFee);
                        dataObject.put("transport_fee", TransportFee);
                        dataObject.put("removable_fee", RemovableFee);
                        dataObject.put("cleaning_fee", CleaningFee);
                        dataObject.put("other_fee", OtherFee);
                        dataObject.put("discount", DiscountPer);
                        dataObject.put("inquiry_id", inquire_id);

                        mainObject.put("data", dataObject);

                        Helper.jsonFileWrite(context, mainObject.toString(), "first_quote.json");

                        getDetails();

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                break;
            case UPDATE_CODE:
                getDetails();
                break;
            default:
                super.onActivityResult(requestCode, resultCode, data);
                break;
        }
    }

    @Override
    public void onRoomNameChange(int position, String room_nname) {
        Log.i(TAG, "onTextChanged: " + room_nname);
        roomName = room_nname;
        if (state) {

        } else {
            setData("roomname", position, 0);
        }

        //response.getData().getRoom().get(position).setName(room_nname);


    }

    @Override
    public void onRoomDesChange(int position, String room_des) {
        Log.i(TAG, "onTextChanged: " + room_des);
        roomDisctiption = room_des;
        if (state) {

        } else {
            setData("roomdiscription", position, -1);
        }

        // response.getData().getRoom().get(position).setDescription(room_des);
    }

    private void setData(String state, int position, int wall_position) {
        try {
            QuoteResponse response = new Gson().fromJson(Helper.jsonFileRead(context, "first_quote.json"), QuoteResponse.class);

            JSONObject mainObject = new JSONObject();
            mainObject.put("status", "success");
            mainObject.put("code", 200);
            mainObject.put("message", "success");

            JSONObject dataObject = new JSONObject();
            JSONArray roomArray = new JSONArray();

            for (int j = 0; j < response.getData().getRoom().size(); j++) {

                JSONObject roomObject = new JSONObject();
                if (j == position) {
                    if (state.equals("roomname")) {
                        roomObject.put("name", roomName);
                        roomObject.put("description", response.getData().getRoom().get(j).getDescription());
                    } else if (state.equals("roomdiscription")) {
                        roomObject.put("name", response.getData().getRoom().get(j).getName());
                        roomObject.put("description", roomDisctiption);
                    } else {
                        roomObject.put("name", response.getData().getRoom().get(j).getName());
                        roomObject.put("description", response.getData().getRoom().get(j).getDescription());
                    }
                } else {
                    roomObject.put("name", response.getData().getRoom().get(j).getName());
                    roomObject.put("description", response.getData().getRoom().get(j).getDescription());
                }


                roomObject.put("total_price", response.getData().getRoom().get(j).getTotalPrice());
                roomObject.put("total_price_country", response.getData().getRoom().get(j).getTotalPriceCountry());

                JSONArray wallArray = new JSONArray();

                for (int i = 0; i < response.getData().getRoom().get(j).getWalls().size(); i++) {

                    JSONObject wallObject = new JSONObject();
                    if (j == position && i == wall_position) {
                        if (state.equals("wallname")) {
                            wallObject.put("name", wallName);
                        } else {
                            wallObject.put("name", response.getData().getRoom().get(j).getWalls().get(i).getName());
                        }
                    } else {
                        wallObject.put("name", response.getData().getRoom().get(j).getWalls().get(i).getName());//
                    }

                    wallObject.put("product_id", response.getData().getRoom().get(j).getWalls().get(i).getProductId());
                    wallObject.put("product_code", response.getData().getRoom().get(j).getWalls().get(i).getProductCode());
                    wallObject.put("product_image", response.getData().getRoom().get(j).getWalls().get(i).getProductImage());
                    wallObject.put("product_name", response.getData().getRoom().get(j).getWalls().get(i).getProductName());
                    wallObject.put("wall_area", response.getData().getRoom().get(j).getWalls().get(i).getWallArea());
                    wallObject.put("wall_area_unit", 1);
                    wallObject.put("wall_cost", response.getData().getRoom().get(j).getWalls().get(i).getWallCost());
                    wallObject.put("wall_cost_country", response.getData().getRoom().get(j).getWalls().get(i).getWallCostCountry());
                    JSONArray imagesArray = new JSONArray();
                    if (response.getData().getRoom().get(j).getWalls().get(i).getWallImage() != null) {
                        if (response.getData().getRoom().get(j).getWalls().get(i).getWallImage().size() > 0) {
                            imagesArray.put(response.getData().getRoom().get(j).getWalls().get(i).getWallImage().get(0));
                            wallObject.put("wall_image", imagesArray);
                        }
                    }

                    wallArray.put(wallObject);
                }

                roomObject.put("walls", wallArray);

                roomArray.put(roomObject);

            }

            dataObject.put("rooms", roomArray);

            dataObject.put("total_cost", txAllTotal.getText().toString());
            dataObject.put("total_cost_country", product.getProductPriceCountry());
            dataObject.put("installation_fee", InstallationFee);
            dataObject.put("transport_fee", TransportFee);
            dataObject.put("removable_fee", RemovableFee);
            dataObject.put("cleaning_fee", CleaningFee);
            dataObject.put("other_fee", OtherFee);
            dataObject.put("discount", DiscountPer);
            dataObject.put("inquiry_id", inquire_id);

            mainObject.put("data", dataObject);

            Helper.jsonFileWrite(context, mainObject.toString(), "first_quote.json");

            // getDetails();

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void deleteData(String state, int position, int wall_position) {
        try {
            String falg = "";
            QuoteResponse response = new Gson().fromJson(Helper.jsonFileRead(context, "first_quote.json"), QuoteResponse.class);
            if (state.equals("roomdelete")) {
                if (response.getData().getRoom().size() > 1) {
                    falg = "true";
                } else {
                    new MyToast(context, "Can not remove Room.");
                }
            } else if (state.equals("walldelete")) {
                if (response.getData().getRoom().get(position).getWalls().size() > 1) {
                    falg = "true";
                } else {
                    new MyToast(context, "Can not remove Wall.");
                }
            } else {

            }

            if (falg.equals("true")) {
                JSONObject mainObject = new JSONObject();
                mainObject.put("status", "success");
                mainObject.put("code", 200);
                mainObject.put("message", "success");

                JSONObject dataObject = new JSONObject();
                JSONArray roomArray = new JSONArray();

                for (int j = 0; j < response.getData().getRoom().size(); j++) {

                    JSONObject roomObject = new JSONObject();
                    if (j == position) {
                        if (state.equals("roomdelete")) {


                        } else {
                            roomObject.put("name", response.getData().getRoom().get(j).getName());
                            roomObject.put("description", response.getData().getRoom().get(j).getDescription());
                            roomObject.put("total_price", response.getData().getRoom().get(j).getTotalPrice());
                            roomObject.put("total_price_country", response.getData().getRoom().get(j).getTotalPriceCountry());

                            JSONArray wallArray = new JSONArray();

                            for (int i = 0; i < response.getData().getRoom().get(j).getWalls().size(); i++) {

                                JSONObject wallObject = new JSONObject();
                                if (j == position && i == wall_position) {
                                    if (state.equals("walldelete")) {


                                    } else {
                                        wallObject.put("name", response.getData().getRoom().get(j).getWalls().get(i).getName());//
                                        wallObject.put("product_id", response.getData().getRoom().get(j).getWalls().get(i).getProductId());
                                        wallObject.put("product_code", response.getData().getRoom().get(j).getWalls().get(i).getProductCode());
                                        wallObject.put("product_image", response.getData().getRoom().get(j).getWalls().get(i).getProductImage());
                                        wallObject.put("product_name", response.getData().getRoom().get(j).getWalls().get(i).getProductName());
                                        wallObject.put("wall_area", response.getData().getRoom().get(j).getWalls().get(i).getWallArea());
                                        wallObject.put("wall_area_unit", 1);
                                        wallObject.put("wall_cost", response.getData().getRoom().get(j).getWalls().get(i).getWallCost());
                                        wallObject.put("wall_cost_country", response.getData().getRoom().get(j).getWalls().get(i).getWallCostCountry());
                                        JSONArray imagesArray = new JSONArray();
                                        if (response.getData().getRoom().get(j).getWalls().get(i).getWallImage() != null) {
                                            if (response.getData().getRoom().get(j).getWalls().get(i).getWallImage().size() > 0) {
                                                imagesArray.put(response.getData().getRoom().get(j).getWalls().get(i).getWallImage().get(0));
                                                wallObject.put("wall_image", imagesArray);
                                            }
                                        }

                                        wallArray.put(wallObject);
                                    }
                                } else {
                                    wallObject.put("name", response.getData().getRoom().get(j).getWalls().get(i).getName());//
                                    wallObject.put("product_id", response.getData().getRoom().get(j).getWalls().get(i).getProductId());
                                    wallObject.put("product_code", response.getData().getRoom().get(j).getWalls().get(i).getProductCode());
                                    wallObject.put("product_image", response.getData().getRoom().get(j).getWalls().get(i).getProductImage());
                                    wallObject.put("product_name", response.getData().getRoom().get(j).getWalls().get(i).getProductName());
                                    wallObject.put("wall_area", response.getData().getRoom().get(j).getWalls().get(i).getWallArea());
                                    wallObject.put("wall_area_unit", 1);
                                    wallObject.put("wall_cost", response.getData().getRoom().get(j).getWalls().get(i).getWallCost());
                                    wallObject.put("wall_cost_country", response.getData().getRoom().get(j).getWalls().get(i).getWallCostCountry());
                                    JSONArray imagesArray = new JSONArray();
                                    if (response.getData().getRoom().get(j).getWalls().get(i).getWallImage() != null) {
                                        if (response.getData().getRoom().get(j).getWalls().get(i).getWallImage().size() > 0) {
                                            imagesArray.put(response.getData().getRoom().get(j).getWalls().get(i).getWallImage().get(0));
                                            wallObject.put("wall_image", imagesArray);
                                        }
                                    }

                                    wallArray.put(wallObject);
                                }
                            }

                            roomObject.put("walls", wallArray);
                            roomArray.put(roomObject);
                        }
                    } else {
                        roomObject.put("name", response.getData().getRoom().get(j).getName());
                        roomObject.put("description", response.getData().getRoom().get(j).getDescription());
                        roomObject.put("total_price", response.getData().getRoom().get(j).getTotalPrice());
                        roomObject.put("total_price_country", response.getData().getRoom().get(j).getTotalPriceCountry());

                        JSONArray wallArray = new JSONArray();

                        for (int i = 0; i < response.getData().getRoom().get(j).getWalls().size(); i++) {

                            JSONObject wallObject = new JSONObject();
                            if (j == position && i == wall_position) {
                                if (state.equals("walldelete")) {


                                } else {
                                    wallObject.put("name", response.getData().getRoom().get(j).getWalls().get(i).getName());//
                                    wallObject.put("product_id", response.getData().getRoom().get(j).getWalls().get(i).getProductId());
                                    wallObject.put("product_code", response.getData().getRoom().get(j).getWalls().get(i).getProductCode());
                                    wallObject.put("product_image", response.getData().getRoom().get(j).getWalls().get(i).getProductImage());
                                    wallObject.put("product_name", response.getData().getRoom().get(j).getWalls().get(i).getProductName());
                                    wallObject.put("wall_area", response.getData().getRoom().get(j).getWalls().get(i).getWallArea());
                                    wallObject.put("wall_area_unit", 1);
                                    wallObject.put("wall_cost", response.getData().getRoom().get(j).getWalls().get(i).getWallCost());
                                    wallObject.put("wall_cost_country", response.getData().getRoom().get(j).getWalls().get(i).getWallCostCountry());
                                    JSONArray imagesArray = new JSONArray();
                                    if (response.getData().getRoom().get(j).getWalls().get(i).getWallImage() != null) {
                                        if (response.getData().getRoom().get(j).getWalls().get(i).getWallImage().size() > 0) {
                                            imagesArray.put(response.getData().getRoom().get(j).getWalls().get(i).getWallImage().get(0));
                                            wallObject.put("wall_image", imagesArray);
                                        }
                                    }

                                    wallArray.put(wallObject);
                                }
                            } else {
                                wallObject.put("name", response.getData().getRoom().get(j).getWalls().get(i).getName());//
                                wallObject.put("product_id", response.getData().getRoom().get(j).getWalls().get(i).getProductId());
                                wallObject.put("product_code", response.getData().getRoom().get(j).getWalls().get(i).getProductCode());
                                wallObject.put("product_image", response.getData().getRoom().get(j).getWalls().get(i).getProductImage());
                                wallObject.put("product_name", response.getData().getRoom().get(j).getWalls().get(i).getProductName());
                                wallObject.put("wall_area", response.getData().getRoom().get(j).getWalls().get(i).getWallArea());
                                wallObject.put("wall_area_unit", 1);
                                wallObject.put("wall_cost", response.getData().getRoom().get(j).getWalls().get(i).getWallCost());
                                wallObject.put("wall_cost_country", response.getData().getRoom().get(j).getWalls().get(i).getWallCostCountry());
                                JSONArray imagesArray = new JSONArray();
                                if (response.getData().getRoom().get(j).getWalls().get(i).getWallImage() != null) {
                                    if (response.getData().getRoom().get(j).getWalls().get(i).getWallImage().size() > 0) {
                                        imagesArray.put(response.getData().getRoom().get(j).getWalls().get(i).getWallImage().get(0));
                                        wallObject.put("wall_image", imagesArray);
                                    }
                                }

                                wallArray.put(wallObject);
                            }
                        }

                        roomObject.put("walls", wallArray);
                        roomArray.put(roomObject);
                    }

                }

                dataObject.put("rooms", roomArray);

                dataObject.put("total_cost", txAllTotal.getText().toString());
                dataObject.put("total_cost_country", product.getProductPriceCountry());
                dataObject.put("installation_fee", InstallationFee);
                dataObject.put("transport_fee", TransportFee);
                dataObject.put("removable_fee", RemovableFee);
                dataObject.put("cleaning_fee", CleaningFee);
                dataObject.put("other_fee", OtherFee);
                dataObject.put("discount", DiscountPer);
                dataObject.put("inquiry_id", inquire_id);

                mainObject.put("data", dataObject);

                Helper.jsonFileWrite(context, mainObject.toString(), "first_quote.json");

                getDetails();
            } else {

            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void showDialogUserValueUpdate() {
        final Dialog dialog = new Dialog(QuoteActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_user_update);
       /* View v = getWindow().getDecorView();
        v.setBackgroundResource(android.R.color.transparent);*/
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);

       /* WindowManager.LayoutParams lp = dialog.getWindow().getAttributes();
        lp.dimAmount=0.7f;
        dialog.getWindow().setAttributes(lp);
        dialog.getWindow().addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);*/
        // dialog.setCanceledOnTouchOutside(false);
        TextView textViewName = (TextView) dialog.findViewById(R.id.tx_name);
        TextView textViewEmail = (TextView) dialog.findViewById(R.id.tx_email);

        android.support.v7.widget.CardView textViewOk = (android.support.v7.widget.CardView) dialog.findViewById(R.id.cd_ok);
        android.support.v7.widget.CardView textViewCancle = (android.support.v7.widget.CardView) dialog.findViewById(R.id.cd_cancel);

        textViewName.setText(application.cus.getName().toString());
        textViewEmail.setText(application.cus.getEmail().toString());

        textViewOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();

                try {

                    QuoteResponse response = new Gson().fromJson(Helper.jsonFileRead(context, "first_quote.json"), QuoteResponse.class);

                    JSONObject mainObject = new JSONObject();
                    mainObject.put("status", "success");
                    mainObject.put("code", 200);
                    mainObject.put("message", "success");

                    JSONObject dataObject = new JSONObject();
                    JSONArray roomArray = new JSONArray();

                    for (int j = 0; j < response.getData().getRoom().size(); j++) {

                        JSONObject roomObject = new JSONObject();
                        roomObject.put("name", response.getData().getRoom().get(j).getName());
                        roomObject.put("description", response.getData().getRoom().get(j).getDescription());
                        roomObject.put("total_price", response.getData().getRoom().get(j).getTotalPrice());
                        roomObject.put("total_price_country", response.getData().getRoom().get(j).getTotalPriceCountry());

                        JSONArray wallArray = new JSONArray();

                        for (int i = 0; i < response.getData().getRoom().get(j).getWalls().size(); i++) {

                            JSONObject wallObject = new JSONObject();

                            wallObject.put("name", response.getData().getRoom().get(j).getWalls().get(i).getName());
                            wallObject.put("product_id", response.getData().getRoom().get(j).getWalls().get(i).getProductId());
                            wallObject.put("product_code", response.getData().getRoom().get(j).getWalls().get(i).getProductCode());
                            wallObject.put("product_image", response.getData().getRoom().get(j).getWalls().get(i).getProductImage());
                            wallObject.put("product_name", response.getData().getRoom().get(j).getWalls().get(i).getProductName());
                            wallObject.put("wall_area", response.getData().getRoom().get(j).getWalls().get(i).getWallArea());
                            wallObject.put("wall_area_unit", 1);
                            wallObject.put("wall_cost", response.getData().getRoom().get(j).getWalls().get(i).getWallCost());
                            wallObject.put("wall_cost_country", response.getData().getRoom().get(j).getWalls().get(i).getWallCostCountry());

                            JSONArray imagesArray = new JSONArray();
                            if (response.getData().getRoom().get(j).getWalls().get(i).getWallImage() != null) {
                                if (response.getData().getRoom().get(j).getWalls().get(i).getWallImage().size() > 0) {
                                    imagesArray.put(response.getData().getRoom().get(j).getWalls().get(i).getWallImage().get(0));
                                    wallObject.put("wall_image", imagesArray);
                                }
                            }

                            wallArray.put(wallObject);
                        }

                        roomObject.put("walls", wallArray);

                        roomArray.put(roomObject);

                    }

                    dataObject.put("rooms", roomArray);

                    dataObject.put("total_cost", txAllTotal.getText().toString());
                    dataObject.put("total_cost_country", product.getProductPriceCountry());
                    dataObject.put("installation_fee", InstallationFee);
                    dataObject.put("transport_fee", TransportFee);
                    dataObject.put("removable_fee", RemovableFee);
                    dataObject.put("cleaning_fee", CleaningFee);
                    dataObject.put("other_fee", OtherFee);
                    dataObject.put("discount", DiscountPer);
                    dataObject.put("inquiry_id", inquire_id);
                    dataObject.put("quote_id", quote_id);

                    mainObject.put("data", dataObject);

                    Log.e("main object", mainObject.toString());

                    Helper.jsonFileWrite(context, mainObject.toString(), "first_quote.json");

                    QuoteResponse quoteResponse = new Gson().fromJson(Helper.jsonFileRead(context, "first_quote.json"), QuoteResponse.class);
                    setQuote(quoteResponse);

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });

        textViewCancle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();


            }
        });
        if (!(this).isFinishing()) {
            dialog.show();
        }

    }

    public void showDialogImageZoomView(Bitmap v) {
        final Dialog dialog = new Dialog(QuoteActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_image);
       /* View v = getWindow().getDecorView();
        v.setBackgroundResource(android.R.color.transparent);*/
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);

       /* WindowManager.LayoutParams lp = dialog.getWindow().getAttributes();
        lp.dimAmount=0.7f;
        dialog.getWindow().setAttributes(lp);
        dialog.getWindow().addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);*/
        // dialog.setCanceledOnTouchOutside(false);
        ImageView textViewName = (ImageView) dialog.findViewById(R.id.add_image);
        textViewName.setImageBitmap(v);

     /*   textViewCancle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();


            }
        });*/
        if (!(this).isFinishing()) {
            dialog.show();
        }

    }
}
