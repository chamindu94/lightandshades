/*
 * Copyright (c) 2017. A.G.THAMAYS <gthamays@gmail.com>
 * This file is part of Light & Shade.
 * Light & Shade can not be copied and/or distribute without the express
 * permission of A.G.THAMAYS
 */

package com.agtapp.lightandshade;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import com.agtapp.lightandshade.model.Product;
import com.agtapp.lightandshade.util.Loading;
import com.agtapp.lightandshade.util.MySharedPreferences;
import com.agtapp.lightandshade.util.MyToast;
import com.agtapp.lightandshade.util.dialog.CreateQuoteDialog;
import com.agtapp.lightandshade.util.text_view.TVRobotoBold;
import com.agtapp.lightandshade.util.text_view.TVRobotoRegular;
import com.agtapp.lightandshade.web_service.ApiClient;
import com.agtapp.lightandshade.web_service.ApiInterface;
import com.agtapp.lightandshade.web_service.request.ProductDetailsRequest;
import com.agtapp.lightandshade.web_service.response.ProductDetailsResponse;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.squareup.picasso.Picasso;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProductDetailsActivity extends AppCompatActivity {

    private static final String TAG = "ProductDetailsActivity";

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.image_load)
    ProgressBar imageLoad;
    @BindView(R.id.im_image)
    ImageView imImage;
    @BindView(R.id.tx_product_id)
    TVRobotoRegular txProductId;
    @BindView(R.id.tx_product_title)
    TVRobotoBold txProductTitle;
    @BindView(R.id.tx_product_category)
    TVRobotoRegular txProductCategory;
    @BindView(R.id.tx_product_brand)
    TVRobotoRegular txProductBrand;
    @BindView(R.id.tx_product_unit)
    TVRobotoRegular txProductUnit;
    @BindView(R.id.tx_product_price)
    TVRobotoRegular txProductPrice;
    @BindView(R.id.tx_product_des)
    TVRobotoRegular txProductDes;
    @BindView(R.id.cd_all_products)
    CardView cdAllProducts;
    @BindView(R.id.cd_add_quote)
    CardView cdAddQuote;
    @BindView(R.id.ly_normal_view)
    LinearLayout lyNormalView;
    @BindView(R.id.swipe_refresh_layout)
    SwipeRefreshLayout swipeRefreshLayout;

    private Context context;
    private Loading loading;
    private MySharedPreferences sharedPreferences;
    private String product_id;
    private Product product;
    private boolean normal_view;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_details);
        ButterKnife.bind(this);

        context = this;
        sharedPreferences = new MySharedPreferences(this);
        loading = new Loading(context);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        normal_view = getIntent().getBooleanExtra("NORMAL_VIEW", false);
        product_id = getIntent().getStringExtra("PRODUCT_ID");

        if (!normal_view) {
            lyNormalView.setVisibility(View.VISIBLE);
        } else {
            lyNormalView.setVisibility(View.GONE);
        }

        getDetails(true);

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getDetails(false);
            }
        });

        cdAddQuote.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new CreateQuoteDialog(context, 1, product, 0, getIntent().getStringExtra("INQUIRE_ID"),0).show();
            }
        });
    }

    private void getDetails(boolean normal_loading) {
        if (normal_loading)
            loading.show();

        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<JsonObject> call = apiService.getProduct("Bearer " + sharedPreferences.getToken(), new ProductDetailsRequest(product_id));
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> json) {

                ProductDetailsResponse response = new Gson().fromJson(json.body(), ProductDetailsResponse.class);
                if (response.getStatus().equals("success")) {

                    product = response.getData();

                    getSupportActionBar().setTitle(response.getData().getName());

                    Picasso.with(context).load(response.getData().getImage())
                            .into(imImage, new com.squareup.picasso.Callback() {
                                @Override
                                public void onSuccess() {
                                    imageLoad.setVisibility(View.GONE);
                                }

                                @Override
                                public void onError() {
                                    imImage.setImageResource(R.drawable.bg_splash);
                                    imageLoad.setVisibility(View.GONE);
                                }
                            });

                    txProductId.setText(response.getData().getProductId());
                    txProductTitle.setText(response.getData().getName());
                    txProductCategory.setText(String.valueOf(response.getData().getCategory().getName()));
                    txProductBrand.setText(String.valueOf(response.getData().getBrand().getName()));
                    txProductUnit.setText("Sq.Feet");
                    txProductPrice.setText(response.getData().getProductPriceCountry() + " " + response.getData().getUnitPrice());
                    txProductDes.setText(response.getData().getDescription());

                } else {
                    if (response.getCode().equals("401")) {
                        sharedPreferences.logout(context);
                    } else {
                        new MyToast(context, response.getMessage());
                    }
                }

                loading.cancel();
                swipeRefreshLayout.setRefreshing(false);

            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                Log.e(TAG, t.toString());
                loading.cancel();
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
