/*
 * Copyright (c) 2017. A.G.THAMAYS <gthamays@gmail.com>
 * This file is part of Light & Shade.
 * Light & Shade can not be copied and/or distribute without the express
 * permission of A.G.THAMAYS
 */

package com.agtapp.lightandshade;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.support.v13.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;

import com.agtapp.lightandshade.adapter.InquireDetailsQuotationListAdapter;
import com.agtapp.lightandshade.model.Customer;
import com.agtapp.lightandshade.util.Loading;
import com.agtapp.lightandshade.util.MySharedPreferences;
import com.agtapp.lightandshade.util.MyToast;
import com.agtapp.lightandshade.util.interfaces.item_click_listener.InquiriesDetailQuotationItemClickListener;
import com.agtapp.lightandshade.util.location.LocationHelper;
import com.agtapp.lightandshade.util.text_view.TVRobotoBold;
import com.agtapp.lightandshade.util.text_view.TVRobotoRegular;
import com.agtapp.lightandshade.web_service.ApiClient;
import com.agtapp.lightandshade.web_service.ApiInterface;
import com.agtapp.lightandshade.web_service.request.CheckInRequest;
import com.agtapp.lightandshade.web_service.request.InquireDetailsRequest;
import com.agtapp.lightandshade.web_service.response.CheckInResponse;
import com.agtapp.lightandshade.web_service.response.InquireDetailsResponse;
import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class InquireDetailActivity extends AppCompatActivity implements InquiriesDetailQuotationItemClickListener {

    private static final String TAG = "InquireDetailActivity";

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.tx_inquire_id)
    TVRobotoBold txInquireId;
    @BindView(R.id.tx_created_name_and_role)
    TVRobotoRegular txCreatedNameAndRole;
    @BindView(R.id.tx_created_date)
    TVRobotoRegular txCreatedDate;
    @BindView(R.id.tx_created_time)
    TVRobotoRegular txCreatedTime;
    @BindView(R.id.tx_customer_name)
    TVRobotoBold txCustomerName;
    @BindView(R.id.tx_customer_phone)
    TVRobotoRegular txCustomerPhone;
    @BindView(R.id.tx_customer_email)
    TVRobotoRegular txCustomerEmail;
    @BindView(R.id.tx_customer_address)
    TVRobotoRegular txCustomerAddress;
    @BindView(R.id.quotation_list)
    RecyclerView quotationList;
    @BindView(R.id.cd_quotation_view)
    CardView cdQuotationView;
    @BindView(R.id.cd_check_in)
    CardView cdCheckIn;
    @BindView(R.id.cd_create_quote)
    CardView cdCreateQuote;
    @BindView(R.id.ly_step_1)
    LinearLayout lyStep1;
    @BindView(R.id.cd_end_inquiry)
    CardView cdEndInquiry;
    @BindView(R.id.cd_cancel_inquiry)
    CardView cdCancelInquiry;
    @BindView(R.id.ly_step_2)
    LinearLayout lyStep2;
    @BindView(R.id.swipe_refresh_layout)
    SwipeRefreshLayout swipeRefreshLayout;
    @BindView(R.id.cd_quote_new)
    CardView cdQuoteNew;

    private Context context;
    private Loading loading;
    private MySharedPreferences sharedPreferences;
    private InquiriesDetailQuotationItemClickListener inquiriesDetailQuotationItemClickListener;
    private String inquire_id;
    private boolean x = false;
    private Location mLastLocation;
    private double latitude, longitude;
    private LocationHelper locationHelper;
    private MainApplication application;

    private FusedLocationProviderClient fusedLocationProviderClient;

    private FusedLocationProviderClient mFusedLocationClient;

    private RequestQueue requestQueue;

    private String customer_id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inquire_detail);
        ButterKnife.bind(this);

        cdCreateQuote.setEnabled(false);
        cdCheckIn.setEnabled(true);
        context = this;
        sharedPreferences = new MySharedPreferences(this);
        loading = new Loading(context);
        inquiriesDetailQuotationItemClickListener = this;
        application = (MainApplication) getApplication();

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        requestQueue = Volley.newRequestQueue(this);

        inquire_id = getIntent().getStringExtra("INQUIRE_ID");

        quotationList.setLayoutManager(new LinearLayoutManager(context));
        quotationList.setHasFixedSize(true);
        quotationList.setLayoutManager(new GridLayoutManager(context, 1, GridLayoutManager.VERTICAL, false));

        fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this);

        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);

        getDetails(true);

//        locationHelper = new LocationHelper(this);
//        locationHelper.checkpermission();


        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getDetails(false);
            }
        });

        cdCheckIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Log.i(TAG, "Check In Click");

                updateCusLocation();

                new MyToast(context, "you have now successfully checked in");
                cdCreateQuote.setEnabled(true);
                x = true;
                cdCheckIn.setEnabled(false);

            }
        });

        cdCreateQuote.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (x) {
                    Intent intent = new Intent(context, ProductsActivity.class);
                    intent.putExtra("INQUIRE_ID", inquire_id);
                    intent.putExtra("update", false);
                    startActivity(intent);
                } else {
                    new MyToast(context, "Please Check-In, and continue.");
                }
            }
        });

        cdQuoteNew.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, ProductsActivity.class);
                intent.putExtra("INQUIRE_ID", inquire_id);
                intent.putExtra("update", false);
                startActivity(intent);
            }
        });

        cdEndInquiry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        cdCancelInquiry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private void getDetails(boolean normal_loading) {
        if (normal_loading)
            loading.show();

        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<JsonObject> call = apiService.getInquire("Bearer " + sharedPreferences.getToken(), new InquireDetailsRequest(inquire_id));
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> json) {

                Log.e("Inquiry response", "" + json.body());

                InquireDetailsResponse response = new Gson().fromJson(json.body(), InquireDetailsResponse.class);
                if (response.getStatus().equals("success")) {

                    getSupportActionBar().setTitle("Inquiry | " + response.getData().getInquiry().getInquiryCode());

                    txInquireId.setText(response.getData().getInquiry().getInquiryCode());
                    txCreatedNameAndRole.setText(response.getData().getAgent().getAgentName()/* + " - " + response.getData().getInquiry().getCreatedUserType()*/);
                    txCreatedDate.setText(response.getData().getInquiry().getCreatedDate());
                    txCreatedTime.setText(response.getData().getInquiry().getCreatedTime());

                    txCustomerName.setText(response.getData().getCustomer().getCustomerName());
                    txCustomerPhone.setText(response.getData().getCustomer().getCustomerMobile());
                    txCustomerEmail.setText(response.getData().getCustomer().getCustomerEmail());
                    txCustomerAddress.setText(response.getData().getCustomer().getCustomerAddress());
                    customer_id = String.valueOf(response.getData().getCustomer().getCustomerId());


                    application.me.setDiscountRange(Float.valueOf(response.getData().getInquiry().getMaximumDiscount()));
                    application.cus.setName(response.getData().getCustomer().getCustomerName());
                    application.cus.setEmail(response.getData().getCustomer().getCustomerEmail());


                    //application.cus.setEmail(response.getData().getCustomer().getCustomerEmail().toString());
                    // application.cus.setName(response.getData().getCustomer().getCustomerName().toString());

                    Log.i(TAG, "onResponse: " + response.getData().isQuotationCreated());
                    if (response.getData().isQuotationCreated()) {
                        cdQuotationView.setVisibility(View.VISIBLE);
                        lyStep1.setVisibility(View.GONE);
                        lyStep2.setVisibility(View.VISIBLE);
                        InquireDetailsQuotationListAdapter adapter = new InquireDetailsQuotationListAdapter(response.getData().getQuotation(), context, inquiriesDetailQuotationItemClickListener);
                        quotationList.setAdapter(adapter);
                    } else {
                        lyStep1.setVisibility(View.VISIBLE);
                        lyStep2.setVisibility(View.GONE);
                        cdQuotationView.setVisibility(View.GONE);
                    }

                } else {
                    if (response.getCode().equals("401")) {
                        sharedPreferences.logout(context);
                    } else {
                        new MyToast(context, response.getMessage());
                    }
                }

                loading.cancel();
                swipeRefreshLayout.setRefreshing(false);

            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                Log.e(TAG, t.toString());
                loading.cancel();
            }
        });


    }

    @Override
    public void recyclerViewListClicked(View v, int position, int id) {
        Intent intent = new Intent(context, QuoteActivity.class);
        intent.putExtra("INQUIRE_ID", inquire_id);
        intent.putExtra("EDITABLE", false);
        intent.putExtra("QUOTE_ID", id);
        context.startActivity(intent);
        ((Activity) context).finish();
    }
/*
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        locationHelper.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        // Once connected with google api, get the location
        mLastLocation = locationHelper.getLocation();
    }

    @Override
    public void onConnectionSuspended(int i) {
        locationHelper.connectApiClient();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        // redirects to utils
        locationHelper.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }
    */

    private void updateCusLocation() {
        //get current location
        if (ContextCompat.checkSelfPermission(InquireDetailActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(InquireDetailActivity.this,
                    new String[]{Manifest.permission.ACCESS_COARSE_LOCATION},
                    3);
        } else {
            //get current location
            mFusedLocationClient.getLastLocation().addOnSuccessListener(InquireDetailActivity.this, new OnSuccessListener<Location>() {
                @Override
                public void onSuccess(final Location location) {
                    if (location != null) {
                        Log.e(TAG, "Latitude : " + location.getLatitude());
                        Log.e(TAG, "Longitude : " + location.getLongitude());

                        String url = "http://applns.lnsdigital.com/wallapi/public/updateCus";

                        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                                new com.android.volley.Response.Listener<String>() {
                                    @Override
                                    public void onResponse(String response) {
                                        Log.e("Response Loc Update", response);
                                    }
                                },
                                new com.android.volley.Response.ErrorListener() {
                                    @Override
                                    public void onErrorResponse(VolleyError error) {
                                        Log.e("Error Location Update", error.getMessage());
                                    }
                                }) {
                            @Override
                            protected Map<String, String> getParams() throws AuthFailureError {
                                Map<String,String> params = new HashMap<>();
                                params.put("id", customer_id);
                                params.put("cus_latt", "" + location.getLatitude());
                                params.put("cus_long", "" + location.getLongitude());

                                Log.e("Params", params.toString());

                                return params;
                            }

                            @Override
                            public Map<String, String> getHeaders() throws AuthFailureError {
                                Map<String,String> params = new HashMap<String,String>();
                                params.put("Authorization", "Bearer " + sharedPreferences.getToken());
                                return params;
                            }
                        };

                        requestQueue.add(stringRequest);
                    } else {
                        Log.e(TAG, "Location Null");
                    }
                }
            });
        }
    }

}
