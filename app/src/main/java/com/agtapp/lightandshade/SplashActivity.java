/*
 * Copyright (c) 2017. A.G.THAMAYS <gthamays@gmail.com>
 * This file is part of Light & Shade.
 * Light & Shade can not be copied and/or distribute without the express
 * permission of A.G.THAMAYS
 */

package com.agtapp.lightandshade;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;

import com.agtapp.lightandshade.util.MySharedPreferences;

import static com.agtapp.lightandshade.util.interfaces.QuoteActivityConstances.IN_THE_ACTIVITY;

public class SplashActivity extends AppCompatActivity {

    private static int SPLASH_TIME_OUT = 1500;
    private MySharedPreferences sharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_splash);

        sharedPreferences = new MySharedPreferences(this);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Log.e("In the activity", sharedPreferences.getQuotValues(IN_THE_ACTIVITY));
                if (sharedPreferences.getLoggedIn()) {
                    if (sharedPreferences.getQuotValues(IN_THE_ACTIVITY).equals("1")) {
                        startActivity(new Intent(SplashActivity.this, QuoteActivity.class));
                        finish();
                    } else {
                        startActivity(new Intent(SplashActivity.this, MainActivity.class));
                        finish();
                    }
                } else {
                    startActivity(new Intent(SplashActivity.this, LoginActivity.class));
                    finish();
                }
            }
        }, SPLASH_TIME_OUT);
    }
}
