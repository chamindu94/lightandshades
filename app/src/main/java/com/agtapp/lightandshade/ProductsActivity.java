/*
 * Copyright (c) 2017. A.G.THAMAYS <gthamays@gmail.com>
 * This file is part of Light & Shade.
 * Light & Shade can not be copied and/or distribute without the express
 * permission of A.G.THAMAYS
 */

package com.agtapp.lightandshade;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;

import com.agtapp.lightandshade.adapter.ProductsListAdapter;
import com.agtapp.lightandshade.model.Product;
import com.agtapp.lightandshade.util.Loading;
import com.agtapp.lightandshade.util.MySharedPreferences;
import com.agtapp.lightandshade.util.MyToast;
import com.agtapp.lightandshade.util.dialog.CreateQuoteDialog;
import com.agtapp.lightandshade.util.interfaces.item_click_listener.ProductsItemClickListener;
import com.agtapp.lightandshade.web_service.ApiClient;
import com.agtapp.lightandshade.web_service.ApiInterface;
import com.agtapp.lightandshade.web_service.response.ProductsListResponse;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProductsActivity extends AppCompatActivity implements ProductsItemClickListener {

    private static final String TAG = "ProductsActivity";

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.list)
    RecyclerView list;
    @BindView(R.id.swipe_refresh_layout)
    SwipeRefreshLayout swipeRefreshLayout;

    private Context context;
    private Loading loading;
    private MySharedPreferences sharedPreferences;
    private ProductsItemClickListener productsItemClickListener;
    private List<Product> productList;
    private boolean normal_view;
    private boolean update;
    private int wall_position;
    private int room_position;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_products);
        ButterKnife.bind(this);

        context = this;
        sharedPreferences = new MySharedPreferences(this);
        loading = new Loading(context);
        productsItemClickListener = this;

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        normal_view = getIntent().getBooleanExtra("NORMAL_VIEW", false);
        update = getIntent().getBooleanExtra("update", false);
        if(update){
            wall_position = getIntent().getIntExtra("wallPosition",0);
            room_position = getIntent().getIntExtra("roomPosition",0);
        }

        list.setLayoutManager(new LinearLayoutManager(context));
        list.setHasFixedSize(true);
        list.setLayoutManager(new GridLayoutManager(context, 2, GridLayoutManager.VERTICAL, false));

        getDetails(true);

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getDetails(false);
            }
        });
    }

    private void getDetails(boolean normal_loading) {
        if (normal_loading)
            loading.show();

        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<JsonObject> call = apiService.getProductsList("Bearer " + sharedPreferences.getToken());
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> json) {

                ProductsListResponse response = new Gson().fromJson(json.body(), ProductsListResponse.class);
                if (response.getStatus().equals("success")) {
                    ProductsListAdapter adapter = new ProductsListAdapter(response.getData().getProducts(), context, productsItemClickListener, normal_view);
                    productList = response.getData().getProducts();
                    list.setAdapter(adapter);
                } else {
                    if (response.getCode().equals("401")) {
                        sharedPreferences.logout(context);
                    } else {
                        new MyToast(context, response.getMessage());
                    }
                }

                loading.cancel();
                swipeRefreshLayout.setRefreshing(false);

            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                Log.e(TAG, t.toString());
                loading.cancel();
            }
        });
    }

    @Override
    public void recyclerViewListClicked(View v, int type, int position, String id) {
        if (type == 1) {
            Intent intent = new Intent(context, ProductDetailsActivity.class);
            intent.putExtra("PRODUCT_ID", id);
            intent.putExtra("INQUIRE_ID", getIntent().getStringExtra("INQUIRE_ID"));
            startActivity(intent);
        }

        if (type == 2) {
            if(update){
                new CreateQuoteDialog(context, 4, productList.get(position), room_position, getIntent().getStringExtra("INQUIRE_ID"),wall_position).show();
            }else {
                new CreateQuoteDialog(context, 1, productList.get(position), 0, getIntent().getStringExtra("INQUIRE_ID"),0).show();
            }

        }

        if (type == 3) {
            Intent intent = new Intent(context, ProductDetailsActivity.class);
            intent.putExtra("PRODUCT_ID", id);
            intent.putExtra("NORMAL_VIEW", true);
            startActivity(intent);
        }

    }
}