/*
 * Copyright (c) 2017. A.G.THAMAYS <gthamays@gmail.com>
 * This file is part of Light & Shade.
 * Light & Shade can not be copied and/or distribute without the express
 * permission of A.G.THAMAYS
 */

package com.agtapp.lightandshade.util.interfaces.item_click_listener;

import android.graphics.Bitmap;
import android.view.View;

/**
 * Created by solomoIT on 9/7/2017.
 */

public interface WallItemImageClickListener {
    void imageCapture(View v, int position, int room_position);
    void addQuote(View v, int position, int room_position);
    void wallNameChange( int position, int room_position,String charSeq);
    void deleteWall( int position, int room_position);
    void viewImageCapture(Bitmap v, int position, int room_position);

}
