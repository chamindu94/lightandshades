/*
 * Copyright (c) 2017. A.G.THAMAYS <gthamays@gmail.com>
 * This file is part of Light & Shade.
 * Light & Shade can not be copied and/or distribute without the express
 * permission of A.G.THAMAYS
 */

package com.agtapp.lightandshade.util;

import android.content.Context;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStreamWriter;

import static android.content.ContentValues.TAG;

/**
 * Created by solomoIT on 10/5/2017.
 */

public class Helper {

    public static boolean isValidEmail(CharSequence target) {
        if (target == null) {
            return false;
        } else {
            return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
        }
    }

    public static void jsonFileWrite(Context context, String data, String file_name) {
        try {
            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(context.openFileOutput(file_name, Context.MODE_PRIVATE));
            outputStreamWriter.write(data);
            outputStreamWriter.close();
            Log.i("Exception", "File write success");
            Log.i(TAG, "jsonFileWrite: " + jsonFileRead(context, file_name));
        } catch (IOException e) {
            Log.e("Exception", "File write failed: " + e.toString());
        }
    }

    public static String jsonFileRead(Context context, String file) {
        String json = null;
        try {
            InputStream is = context.openFileInput(file);
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
            Log.i(TAG, "jsonFileRead: " + json);
            Log.i(TAG, " ----------- START---------------");
            final int chunkSize = 2048;
            for (int i = 0; i < json.length(); i += chunkSize) {
                Log.i(TAG, json.substring(i, Math.min(json.length(), i + chunkSize)));
            }
            Log.i(TAG, " ----------- END---------------");
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return json;
    }

    public static String jsonFileReadFromAssets(Context context, String file) {
        String json = null;
        try {
            InputStream is = context.getAssets().open(file);
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return json;
    }

    public static void floatingButtonScrollHide(RecyclerView list, final FloatingActionButton fab) {
        list.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                if (dy > 0 || dy < 0 && fab.isShown()) {
                    fab.hide();
                }
            }

            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                if (newState == RecyclerView.SCROLL_STATE_IDLE) {
                    fab.show();
                }

                super.onScrollStateChanged(recyclerView, newState);
            }
        });
    }

}
