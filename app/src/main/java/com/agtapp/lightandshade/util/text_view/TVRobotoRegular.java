/*
 * Copyright (c) 2017. A.G.THAMAYS <gthamays@gmail.com>
 * This file is part of Light & Shade.
 * Light & Shade can not be copied and/or distribute without the express
 * permission of A.G.THAMAYS
 */

package com.agtapp.lightandshade.util.text_view;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;

/**
 * Created by A.G.THAMAYS on 7/16/2017.
 */

public class TVRobotoRegular extends android.support.v7.widget.AppCompatTextView {

    public TVRobotoRegular(Context context) {
        super(context);
        init();
    }

    public TVRobotoRegular(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public TVRobotoRegular(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        Typeface typeface = Typeface.createFromAsset(getContext().getAssets(), "fonts/Roboto-Regular.ttf");
        setTypeface(typeface);
    }
}
