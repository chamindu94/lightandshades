/*
 * Copyright (c) 2017. A.G.THAMAYS <gthamays@gmail.com>
 * This file is part of Light & Shade.
 * Light & Shade can not be copied and/or distribute without the express
 * permission of A.G.THAMAYS
 */

package com.agtapp.lightandshade.util.interfaces.item_click_listener;

import android.view.View;

/**
 * Created by solomoIT on 9/7/2017.
 */

public interface InquiriesItemClickListener {
    void recyclerViewListClicked(View v, int position, String id);
}
