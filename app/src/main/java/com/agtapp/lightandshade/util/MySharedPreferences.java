/*
 * Copyright (c) 2017. A.G.THAMAYS <gthamays@gmail.com>
 * This file is part of Light & Shade.
 * Light & Shade can not be copied and/or distribute without the express
 * permission of A.G.THAMAYS
 */

package com.agtapp.lightandshade.util;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

import com.agtapp.lightandshade.LoginActivity;

/**
 * Created by solomoIT on 9/12/2017.
 */

public class MySharedPreferences {

    private static SharedPreferences sharedPreferences;
    private static SharedPreferences.Editor editor;
    private static final String PREF_NAME = "LightAndShade";

    private int PRIVATE_MODE = 0;
    private Context context;

    public MySharedPreferences(Context context) {
        this.context = context;
        sharedPreferences = context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = sharedPreferences.edit();
    }

    private final static String LOGGED_IN = "LOGGED_IN";
    private final static String TOKEN = "TOKEN";

    public boolean getLoggedIn() {
        return sharedPreferences.getBoolean(LOGGED_IN, false);
    }

    public String getToken() {
        return sharedPreferences.getString(TOKEN, "");
    }

    public void setLoggedIn(boolean b, String token) {
        editor.putBoolean(LOGGED_IN, b);
        editor.putString(TOKEN, token);
        editor.commit();
    }

    public boolean logout(Context context) {
        editor.clear().commit();
        System.exit(0);
        Intent intent = new Intent(context, LoginActivity.class);
        context.startActivity(intent);
        ((Activity) context).finish();
        return true;
    }

    //to store add quot dialog box values
    public void setQuotValues(String key, String value) {
        editor.putString(key, value);
        editor.apply();
    }

    public void setQuotValues(String key, Boolean value) {
        editor.putBoolean(key, value);
        editor.apply();
    }

    //to get add quot dialog box values
    public String getQuotValues(String key) {
        String value = sharedPreferences.getString(key, "");
        return value;
    }

    public Boolean getQuotValuesBool(String key) {
        Boolean value = sharedPreferences.getBoolean(key, true);
        return value;
    }

    //to store user category
    public void setUserCategory(String key, String value) {
        editor.putString(key, value);
        editor.apply();
    }

    public String getUserCategory(String key) {
        String value = sharedPreferences.getString(key, "");
        return value;
    }

}
