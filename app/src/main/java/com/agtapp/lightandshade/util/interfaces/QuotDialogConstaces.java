package com.agtapp.lightandshade.util.interfaces;

public interface QuotDialogConstaces {
    String HEIGHT = "height";
    String WIDTH = "width";
    String REDUCTION_HEIGHT = "reduction_height";
    String REDUCTION_WIDTH = "reduction_width";
    String NO_OF_SURFACES = "no_of_surfaces";
    String INQUIRE_ID = "inquire_id";
    String PRODUCT_ID = "product_id";
}
