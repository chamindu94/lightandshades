/*
 * Copyright (c) 2017. A.G.THAMAYS <gthamays@gmail.com>
 * This file is part of Light & Shade.
 * Light & Shade can not be copied and/or distribute without the express
 * permission of A.G.THAMAYS
 */

package com.agtapp.lightandshade.util;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.agtapp.lightandshade.R;

/**
 * Created by A.G.THAMAYS on 9/15/2017.
 */

public class MyToast extends Toast {

    public MyToast(Context context, String message) {
        super(context);
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.my_toast, null);

        TextView tx_message = (TextView) view.findViewById(R.id.tx_message);
        tx_message.setText(message);

        this.setView(view);
        this.setDuration(Toast.LENGTH_SHORT);
        this.show();
    }
}
