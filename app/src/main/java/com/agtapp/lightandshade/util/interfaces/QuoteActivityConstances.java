package com.agtapp.lightandshade.util.interfaces;

public interface QuoteActivityConstances {
    String IN_THE_ACTIVITY = "in_the_activity";
    String PRODUCT = "product";
    String INQUIRE_ID = "inq_id";
    String EDITABLE = "editable";
    String QUOT_ID = "quot_id";
}
