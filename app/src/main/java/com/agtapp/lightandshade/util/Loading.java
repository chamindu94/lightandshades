/*
 * Copyright (c) 2017. A.G.THAMAYS <gthamays@gmail.com>
 * This file is part of Light & Shade.
 * Light & Shade can not be copied and/or distribute without the express
 * permission of A.G.THAMAYS
 */

package com.agtapp.lightandshade.util;

import android.app.Dialog;
import android.content.Context;
import android.view.WindowManager;

import com.agtapp.lightandshade.R;
import com.wang.avi.AVLoadingIndicatorView;

public class Loading extends Dialog {

    public Loading(Context context) {
        super(context);
        this.setContentView(R.layout.dialog_loading);
        this.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        this.setCancelable(false);

        AVLoadingIndicatorView loading = (AVLoadingIndicatorView) findViewById(R.id.loading);
        loading.smoothToShow();

        WindowManager.LayoutParams layoutParams = new WindowManager.LayoutParams();
        layoutParams.copyFrom(this.getWindow().getAttributes());
        layoutParams.width = WindowManager.LayoutParams.MATCH_PARENT;
        layoutParams.height = WindowManager.LayoutParams.MATCH_PARENT;

        this.getWindow().setAttributes(layoutParams);

    }
}
