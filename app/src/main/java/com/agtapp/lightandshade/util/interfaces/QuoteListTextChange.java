/*
 * Copyright (c) 2017. A.G.THAMAYS <gthamays@gmail.com>
 * This file is part of Light & Shade.
 * Light & Shade can not be copied and/or distribute without the express
 * permission of A.G.THAMAYS
 */

package com.agtapp.lightandshade.util.interfaces;

/**
 * Created by A.G.THAMAYS on 10/26/2017.
 */

public interface QuoteListTextChange {
    void onRoomNameChange(int position, String charSeq);
    void onRoomDesChange(int position, String charSeq);
    void deleteRoom( int room_position);
}
