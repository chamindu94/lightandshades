/*
 * Copyright (c) 2017. A.G.THAMAYS <gthamays@gmail.com>
 * This file is part of Light & Shade.
 * Light & Shade can not be copied and/or distribute without the express
 * permission of A.G.THAMAYS
 */

package com.agtapp.lightandshade.util.dialog;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.TextView;

import com.agtapp.lightandshade.QuoteActivity;
import com.agtapp.lightandshade.R;
import com.agtapp.lightandshade.model.Product;
import com.agtapp.lightandshade.util.Helper;
import com.agtapp.lightandshade.util.MySharedPreferences;
import com.agtapp.lightandshade.util.MyToast;
import com.agtapp.lightandshade.util.edit_text.ETRobotoRegular;
import com.agtapp.lightandshade.util.text_view.TVRobotoBold;
import com.agtapp.lightandshade.web_service.response.QuoteResponse;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.agtapp.lightandshade.util.interfaces.QuotDialogConstaces.HEIGHT;
import static com.agtapp.lightandshade.util.interfaces.QuotDialogConstaces.INQUIRE_ID;
import static com.agtapp.lightandshade.util.interfaces.QuotDialogConstaces.NO_OF_SURFACES;
import static com.agtapp.lightandshade.util.interfaces.QuotDialogConstaces.PRODUCT_ID;
import static com.agtapp.lightandshade.util.interfaces.QuotDialogConstaces.REDUCTION_HEIGHT;
import static com.agtapp.lightandshade.util.interfaces.QuotDialogConstaces.REDUCTION_WIDTH;
import static com.agtapp.lightandshade.util.interfaces.QuotDialogConstaces.WIDTH;

/**
 * Created by solomoIT on 10/9/2017.
 */

public class CreateQuoteDialog extends Dialog {

    private static final String TAG = "CreateQuoteDialog";

    @BindView(R.id.tx_title)
    TVRobotoBold txTitle;
    @BindView(R.id.et_height)
    ETRobotoRegular etHeight;
    @BindView(R.id.et_width)
    ETRobotoRegular etWidth;
    @BindView(R.id.et_red_height)
    ETRobotoRegular etRedHeight;
    @BindView(R.id.et_red_width)
    ETRobotoRegular etRedWidth;
    @BindView(R.id.et_no_walls)
    ETRobotoRegular etNoWalls;
    @BindView(R.id.cd_cancel)
    CardView cdCancel;
    @BindView(R.id.cd_add_item_quote)
    CardView cdAddItemQuote;

    private Context context;
    private Product product;
    private int current_room, type, current_wall;
    private String inquire_id;
    private MySharedPreferences mySharedPreferences;

    //    1 - first_time, 2 - add_wall, 3 - add_room, 4 - update_wall
    public CreateQuoteDialog(@NonNull Context context, int type, Product product, int current_room, String inquire_id,int current_wall) {
        super(context);
        this.context = context;
        this.product = product;
        this.type = type;
        this.current_room = current_room;
        this.inquire_id = inquire_id;
        this.current_wall = current_wall;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        storeFieldsData();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_create_first_quote);
        ButterKnife.bind(this);

        setCanceledOnTouchOutside(false);
        getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        setCancelable(false);

        TextView dialog_title = (TextView) findViewById(R.id.tx_title);
        dialog_title.setText(product.getName());

        CardView cancel = (CardView) findViewById(R.id.cd_cancel);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                storeFieldsData();
                cancel();
            }
        });

        getFieldsData();

        CardView add_item_quote = (CardView) findViewById(R.id.cd_add_item_quote);
        add_item_quote.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                clearCachedData();

                    if (!TextUtils.isEmpty(etHeight.getText().toString()) &&
                            !TextUtils.isEmpty(etWidth.getText().toString()) &&
                            !TextUtils.isEmpty(etRedHeight.getText().toString()) &&
                            !TextUtils.isEmpty(etRedWidth.getText().toString()) &&
                            !TextUtils.isEmpty(etNoWalls.getText().toString())) {

                        float full_area = Float.valueOf(etHeight.getText().toString()) * Float.valueOf(etWidth.getText().toString());
                        float red_area = Float.valueOf(etRedHeight.getText().toString()) * Float.valueOf(etRedWidth.getText().toString());
                        float available_area = full_area - red_area;
                        float total_area = available_area * Float.valueOf(etNoWalls.getText().toString());
                        float wall_cost = total_area * Float.valueOf(product.getUnitPrice());
                        int wall_count = Integer.parseInt(etNoWalls.getText().toString());

                        try {
                            if (type == 1) {
                                JSONObject mainObject = new JSONObject();
                                mainObject.put("status", "success");
                                mainObject.put("code", 200);
                                mainObject.put("message", "success");

                                JSONObject dataObject = new JSONObject();
                                dataObject.put("total_cost", wall_cost);
                                dataObject.put("total_cost_country", product.getProductPriceCountry());
                                dataObject.put("installation_fee", "");
                                dataObject.put("transport_fee", "");
                                dataObject.put("removable_fee", "");
                                dataObject.put("cleaning_fee", "");
                                dataObject.put("other_fee", "");
                                dataObject.put("discount", "");

                                JSONArray roomArray = new JSONArray();

                                JSONObject roomObject = new JSONObject();
                                roomObject.put("name", "Room - 1");
                                roomObject.put("description", "");
                                roomObject.put("total_price", wall_cost * wall_count);
                                roomObject.put("total_price_country", product.getProductPriceCountry());

                                JSONArray wallArray = new JSONArray();

                                for (int i = 0; i < wall_count; i++) {
                                    JSONObject wallObject = new JSONObject();
                                    wallObject.put("name", "Wall - " + (i + 1));
                                    wallObject.put("product_id", product.getId());
                                    wallObject.put("product_code", product.getProductId());
                                    wallObject.put("product_image", product.getImage());
                                    wallObject.put("product_name", product.getName());
                                    wallObject.put("wall_area", total_area);
                                    wallObject.put("wall_area_unit", "Sq.Feet");
                                    wallObject.put("wall_cost", wall_cost);
                                    wallObject.put("wall_cost_country", product.getProductPriceCountry());

                                    JSONArray imagesArray = new JSONArray();
                                    imagesArray.put("_");
                                    wallObject.put("wall_image", imagesArray);

                                    wallArray.put(wallObject);
                                }

                                roomObject.put("walls", wallArray);

                                roomArray.put(roomObject);


                                dataObject.put("rooms", roomArray);
                                mainObject.put("data", dataObject);

                                Helper.jsonFileWrite(context, mainObject.toString(), "first_quote.json");

                            } else if (type == 2) {

                                QuoteResponse response = new Gson().fromJson(Helper.jsonFileRead(context, "first_quote.json"), QuoteResponse.class);

                                JSONObject mainObject = new JSONObject();
                                mainObject.put("status", "success");
                                mainObject.put("code", 200);
                                mainObject.put("message", "success");

                                JSONObject dataObject = new JSONObject();
                                dataObject.put("total_cost", wall_cost);
                                dataObject.put("total_cost_country", product.getProductPriceCountry());
                                dataObject.put("installation_fee", response.getData().getInstallationFee());
                                dataObject.put("transport_fee", response.getData().getTransportFee());
                                dataObject.put("removable_fee", response.getData().getRemovableFee());
                                dataObject.put("cleaning_fee", response.getData().getCleaningFee());
                                dataObject.put("other_fee", response.getData().getOtherFee());
                                dataObject.put("discount", response.getData().getDiscount());

                                JSONArray roomArray = new JSONArray();

                                for (int i = 0; i < response.getData().getRoom().size(); i++) {
                                    JSONObject roomObject = new JSONObject();
                                    if (i == current_room) {

                                        roomObject.put("name", response.getData().getRoom().get(i).getName());
                                        roomObject.put("description", response.getData().getRoom().get(i).getDescription());
                                        roomObject.put("total_price", response.getData().getRoom().get(i).getTotalPrice());
                                        roomObject.put("total_price_country", response.getData().getRoom().get(i).getTotalPriceCountry());

                                        JSONArray wallArray = new JSONArray();

                                        for (int j = 0; j < wall_count + response.getData().getRoom().get(current_room).getWalls().size(); j++) {
                                            JSONObject wallObject = new JSONObject();

                                            if (j < response.getData().getRoom().get(current_room).getWalls().size()) {
                                                wallObject.put("name", response.getData().getRoom().get(i).getWalls().get(j).getName());
                                                wallObject.put("product_id", response.getData().getRoom().get(i).getWalls().get(j).getProductId());
                                                wallObject.put("product_code", response.getData().getRoom().get(i).getWalls().get(j).getProductCode());
                                                wallObject.put("product_image", response.getData().getRoom().get(i).getWalls().get(j).getProductImage());
                                                wallObject.put("product_name", response.getData().getRoom().get(i).getWalls().get(j).getProductName());
                                                wallObject.put("wall_area", response.getData().getRoom().get(i).getWalls().get(j).getWallArea());
                                                wallObject.put("wall_area_unit", response.getData().getRoom().get(i).getWalls().get(j).getWallAreaUnit());
                                                wallObject.put("wall_cost", response.getData().getRoom().get(i).getWalls().get(j).getWallCost());
                                                wallObject.put("wall_cost_country", response.getData().getRoom().get(i).getWalls().get(j).getWallCostCountry());
                                                if (response.getData().getRoom().get(i).getWalls().get(j).getWallImage() != null) {
                                                    if (response.getData().getRoom().get(i).getWalls().get(j).getWallImage().size() > 0) {
                                                        JSONArray imagesArray = new JSONArray();
                                                        imagesArray.put(response.getData().getRoom().get(i).getWalls().get(j).getWallImage().get(0));
                                                        wallObject.put("wall_image", imagesArray);

                                                    }
                                                }
                                            } else {
                                                wallObject.put("name", "Wall - " + (j + 1));
                                                wallObject.put("product_id", product.getId());
                                                wallObject.put("product_code", product.getProductId());
                                                wallObject.put("product_image", product.getImage());
                                                wallObject.put("product_name", product.getName());
                                                wallObject.put("wall_area", total_area);
                                                wallObject.put("wall_area_unit", "Sq.Feet");
                                                wallObject.put("wall_cost", wall_cost);
                                                wallObject.put("wall_cost_country", product.getProductPriceCountry());

                                                JSONArray imagesArray = new JSONArray();
                                                imagesArray.put("_");
                                                wallObject.put("wall_image", imagesArray);
                                            }

                                            wallArray.put(wallObject);
                                        }

                                        roomObject.put("walls", wallArray);
                                    } else {

                                        roomObject.put("name", response.getData().getRoom().get(i).getName());
                                        roomObject.put("description", response.getData().getRoom().get(i).getDescription());
                                        roomObject.put("total_price", response.getData().getRoom().get(i).getTotalPrice());
                                        roomObject.put("total_price_country", response.getData().getRoom().get(i).getTotalPriceCountry());

                                        JSONArray wallArray = new JSONArray();

                                        for (int j = 0; j < response.getData().getRoom().get(i).getWalls().size(); j++) {

                                            JSONObject wallObject = new JSONObject();

                                            wallObject.put("name", response.getData().getRoom().get(i).getWalls().get(j).getName());
                                            wallObject.put("product_id", response.getData().getRoom().get(i).getWalls().get(j).getProductId());
                                            wallObject.put("product_code", response.getData().getRoom().get(i).getWalls().get(j).getProductCode());
                                            wallObject.put("product_image", response.getData().getRoom().get(i).getWalls().get(j).getProductImage());
                                            wallObject.put("product_name", response.getData().getRoom().get(i).getWalls().get(j).getProductName());
                                            wallObject.put("wall_area", response.getData().getRoom().get(i).getWalls().get(j).getWallArea());
                                            wallObject.put("wall_area_unit", response.getData().getRoom().get(i).getWalls().get(j).getWallAreaUnit());
                                            wallObject.put("wall_cost", response.getData().getRoom().get(i).getWalls().get(j).getWallCost());
                                            wallObject.put("wall_cost_country", response.getData().getRoom().get(i).getWalls().get(j).getWallCostCountry());
                                            if (response.getData().getRoom().get(i).getWalls().get(j).getWallImage() != null) {
                                                if (response.getData().getRoom().get(i).getWalls().get(j).getWallImage().size() > 0) {
                                                    JSONArray imagesArray = new JSONArray();
                                                    imagesArray.put(response.getData().getRoom().get(i).getWalls().get(j).getWallImage().get(0));
                                                    wallObject.put("wall_image", imagesArray);
                                                }
                                            }
                                            wallArray.put(wallObject);
                                        }

                                        roomObject.put("walls", wallArray);
                                    }

                                    roomArray.put(roomObject);
                                }
                            /*JSONObject roomObject = new JSONObject();
                            roomObject.put("name", "Room - 1");
                            roomObject.put("description", "");
                            roomObject.put("total_price", wall_cost * wall_count);
                            roomObject.put("total_price_country", product.getProductPriceCountry());

                            JSONArray wallArray = new JSONArray();

                            for (int i = 0; i < wall_count + response.getData().getRoom().get(current_room).getWalls().size(); i++) {
                                JSONObject wallObject = new JSONObject();

                                if (i < response.getData().getRoom().get(current_room).getWalls().size()) {
                                    wallObject.put("name", response.getData().getRoom().get(current_room).getWalls().get(i).getName());
                                    wallObject.put("product_id", product.getId());
                                    wallObject.put("product_code", product.getProductId());
                                    wallObject.put("product_image", product.getImage());
                                    wallObject.put("product_name", product.getName());
                                    wallObject.put("wall_area", response.getData().getRoom().get(current_room).getWalls().get(i).getWallArea());
                                    wallObject.put("wall_area_unit", "Sq.Feet");
                                    wallObject.put("wall_cost", response.getData().getRoom().get(current_room).getWalls().get(i).getWallCost());
                                    wallObject.put("wall_cost_country", product.getProductPriceCountry());
                                } else {
                                    wallObject.put("name", "Wall - " + (i + 1));
                                    wallObject.put("product_id", product.getId());
                                    wallObject.put("product_code", product.getProductId());
                                    wallObject.put("product_image", product.getImage());
                                    wallObject.put("product_name", product.getName());
                                    wallObject.put("wall_area", total_area);
                                    wallObject.put("wall_area_unit", "Sq.Feet");
                                    wallObject.put("wall_cost", wall_cost);
                                    wallObject.put("wall_cost_country", product.getProductPriceCountry());
                                }

                                wallArray.put(wallObject);
                            }

                            roomObject.put("walls", wallArray);*/


                                dataObject.put("rooms", roomArray);
                                mainObject.put("data", dataObject);

                                Helper.jsonFileWrite(context, mainObject.toString(), "first_quote.json");

                            } else if (type == 3) {

                                QuoteResponse response = new Gson().fromJson(Helper.jsonFileRead(context, "first_quote.json"), QuoteResponse.class);

                                JSONObject mainObject = new JSONObject();
                                mainObject.put("status", "success");
                                mainObject.put("code", 200);
                                mainObject.put("message", "success");

                                JSONObject dataObject = new JSONObject();
                                dataObject.put("total_cost", wall_cost);
                                dataObject.put("total_cost_country", product.getProductPriceCountry());
                                dataObject.put("installation_fee", response.getData().getInstallationFee());
                                dataObject.put("transport_fee", response.getData().getTransportFee());
                                dataObject.put("removable_fee", response.getData().getRemovableFee());
                                dataObject.put("cleaning_fee", response.getData().getCleaningFee());
                                dataObject.put("other_fee", response.getData().getOtherFee());
                                dataObject.put("discount", response.getData().getDiscount());

                                JSONArray roomArray = new JSONArray();

                                for (int j = 0; j < response.getData().getRoom().size() + 1; j++) {

                                    JSONObject roomObject = new JSONObject();
                                    if (j < response.getData().getRoom().size()) {

                                        roomObject.put("name", response.getData().getRoom().get(j).getName());
                                        roomObject.put("description", response.getData().getRoom().get(j).getDescription());
                                        roomObject.put("total_price", response.getData().getRoom().get(j).getTotalPrice());
                                        roomObject.put("total_price_country", response.getData().getRoom().get(j).getTotalPriceCountry());

                                        JSONArray wallArray = new JSONArray();

                                        for (int i = 0; i < response.getData().getRoom().get(j).getWalls().size(); i++) {

                                            JSONObject wallObject = new JSONObject();

                                            wallObject.put("name", response.getData().getRoom().get(j).getWalls().get(i).getName());
                                            wallObject.put("product_id", response.getData().getRoom().get(j).getWalls().get(i).getProductId());
                                            wallObject.put("product_code", response.getData().getRoom().get(j).getWalls().get(i).getProductCode());
                                            wallObject.put("product_image", response.getData().getRoom().get(j).getWalls().get(i).getProductImage());
                                            wallObject.put("product_name", response.getData().getRoom().get(j).getWalls().get(i).getProductName());
                                            wallObject.put("wall_area", response.getData().getRoom().get(j).getWalls().get(i).getWallArea());
                                            wallObject.put("wall_area_unit", response.getData().getRoom().get(j).getWalls().get(i).getWallAreaUnit());
                                            wallObject.put("wall_cost", response.getData().getRoom().get(j).getWalls().get(i).getWallCost());
                                            wallObject.put("wall_cost_country", response.getData().getRoom().get(j).getWalls().get(i).getWallCostCountry());
                                            if (response.getData().getRoom().get(j).getWalls().get(i).getWallImage() != null) {
                                                if (response.getData().getRoom().get(j).getWalls().get(i).getWallImage().size() > 0) {
                                                    JSONArray imagesArray = new JSONArray();
                                                    imagesArray.put(response.getData().getRoom().get(j).getWalls().get(i).getWallImage().get(0));
                                                    wallObject.put("wall_image", imagesArray);
                                                }
                                            }
                                            wallArray.put(wallObject);
                                        }

                                        roomObject.put("walls", wallArray);
                                    } else {
                                        roomObject.put("name", "Room - " + (response.getData().getRoom().size() + 1));
                                        roomObject.put("description", "");
                                        roomObject.put("total_price", wall_cost * wall_count);
                                        roomObject.put("total_price_country", product.getProductPriceCountry());

                                        JSONArray wallArray = new JSONArray();

                                        for (int k = 0; k < wall_count; k++) {
                                            JSONObject wallObject = new JSONObject();
                                            wallObject.put("name", "Wall - " + (k + 1));
                                            wallObject.put("product_id", product.getId());
                                            wallObject.put("product_code", product.getProductId());
                                            wallObject.put("product_image", product.getImage());
                                            wallObject.put("product_name", product.getName());
                                            wallObject.put("wall_area", total_area);
                                            wallObject.put("wall_area_unit", "Sq.Feet");
                                            wallObject.put("wall_cost", wall_cost);
                                            wallObject.put("wall_cost_country", product.getProductPriceCountry());

                                            JSONArray imagesArray = new JSONArray();
                                            imagesArray.put("_");
                                            wallObject.put("wall_image", imagesArray);

                                            wallArray.put(wallObject);
                                        }

                                        roomObject.put("walls", wallArray);
                                    }

                                    roomArray.put(roomObject);

                                }

                                dataObject.put("rooms", roomArray);
                                mainObject.put("data", dataObject);

                                Helper.jsonFileWrite(context, mainObject.toString(), "first_quote.json");
                            } else if (type == 4) {

                                QuoteResponse response = new Gson().fromJson(Helper.jsonFileRead(context, "first_quote.json"), QuoteResponse.class);

                                JSONObject mainObject = new JSONObject();
                                mainObject.put("status", "success");
                                mainObject.put("code", 200);
                                mainObject.put("message", "success");

                                JSONObject dataObject = new JSONObject();
                                dataObject.put("total_cost", wall_cost);
                                dataObject.put("total_cost_country", product.getProductPriceCountry());
                                dataObject.put("installation_fee", response.getData().getInstallationFee());
                                dataObject.put("transport_fee", response.getData().getTransportFee());
                                dataObject.put("removable_fee", response.getData().getRemovableFee());
                                dataObject.put("cleaning_fee", response.getData().getCleaningFee());
                                dataObject.put("other_fee", response.getData().getOtherFee());
                                dataObject.put("discount", response.getData().getDiscount());

                                JSONArray roomArray = new JSONArray();

                                for (int i = 0; i < response.getData().getRoom().size(); i++) {
                                    JSONObject roomObject = new JSONObject();
                                    if (i == current_room) {

                                        roomObject.put("name", response.getData().getRoom().get(i).getName());
                                        roomObject.put("description", response.getData().getRoom().get(i).getDescription());
                                        roomObject.put("total_price", response.getData().getRoom().get(i).getTotalPrice());
                                        roomObject.put("total_price_country", response.getData().getRoom().get(i).getTotalPriceCountry());

                                        JSONArray wallArray = new JSONArray();

                                        for (int j = 0; j < (wall_count - 1) + response.getData().getRoom().get(current_room).getWalls().size(); j++) {
                                            JSONObject wallObject = new JSONObject();

                                            if (j < response.getData().getRoom().get(current_room).getWalls().size()) {
                                                if (j == current_wall) {
                                                    wallObject.put("name", response.getData().getRoom().get(current_room).getWalls().get(j).getName());
                                                    wallObject.put("product_id", product.getId());
                                                    wallObject.put("product_code", product.getProductId());
                                                    wallObject.put("product_image", product.getImage());
                                                    wallObject.put("product_name", product.getName());
                                                    wallObject.put("wall_area", total_area);
                                                    wallObject.put("wall_area_unit", "Sq.Feet");
                                                    wallObject.put("wall_cost", wall_cost);
                                                    wallObject.put("wall_cost_country", product.getProductPriceCountry());
                                                    if (response.getData().getRoom().get(i).getWalls().get(j).getWallImage() != null) {
                                                        if (response.getData().getRoom().get(current_room).getWalls().get(j).getWallImage().size() > 0) {
                                                            JSONArray imagesArray = new JSONArray();
                                                            imagesArray.put(response.getData().getRoom().get(current_room).getWalls().get(j).getWallImage().get(0));
                                                            wallObject.put("wall_image", imagesArray);
                                                        }
                                                    }

                                                } else {
                                                    wallObject.put("name", response.getData().getRoom().get(i).getWalls().get(j).getName());
                                                    wallObject.put("product_id", response.getData().getRoom().get(i).getWalls().get(j).getProductId());
                                                    wallObject.put("product_code", response.getData().getRoom().get(i).getWalls().get(j).getProductCode());
                                                    wallObject.put("product_image", response.getData().getRoom().get(i).getWalls().get(j).getProductImage());
                                                    wallObject.put("product_name", response.getData().getRoom().get(i).getWalls().get(j).getProductName());
                                                    wallObject.put("wall_area", response.getData().getRoom().get(i).getWalls().get(j).getWallArea());
                                                    wallObject.put("wall_area_unit", response.getData().getRoom().get(i).getWalls().get(j).getWallAreaUnit());
                                                    wallObject.put("wall_cost", response.getData().getRoom().get(i).getWalls().get(j).getWallCost());
                                                    wallObject.put("wall_cost_country", response.getData().getRoom().get(i).getWalls().get(j).getWallCostCountry());
                                                    if (response.getData().getRoom().get(i).getWalls().get(j).getWallImage() != null) {
                                                        if (response.getData().getRoom().get(i).getWalls().get(j).getWallImage().size() > 0) {
                                                            JSONArray imagesArray = new JSONArray();
                                                            imagesArray.put(response.getData().getRoom().get(i).getWalls().get(j).getWallImage().get(0));
                                                            wallObject.put("wall_image", imagesArray);
                                                        }
                                                    }
                                                }

                                            } else {
                                                wallObject.put("name", "Wall - " + (j + 1));
                                                wallObject.put("product_id", product.getId());
                                                wallObject.put("product_code", product.getProductId());
                                                wallObject.put("product_image", product.getImage());
                                                wallObject.put("product_name", product.getName());
                                                wallObject.put("wall_area", total_area);
                                                wallObject.put("wall_area_unit", "Sq.Feet");
                                                wallObject.put("wall_cost", wall_cost);
                                                wallObject.put("wall_cost_country", product.getProductPriceCountry());

                                                JSONArray imagesArray = new JSONArray();
                                                imagesArray.put("_");
                                                wallObject.put("wall_image", imagesArray);
                                            }

                                            wallArray.put(wallObject);
                                        }

                                        roomObject.put("walls", wallArray);
                                    } else {

                                        roomObject.put("name", response.getData().getRoom().get(i).getName());
                                        roomObject.put("description", response.getData().getRoom().get(i).getDescription());
                                        roomObject.put("total_price", response.getData().getRoom().get(i).getTotalPrice());
                                        roomObject.put("total_price_country", response.getData().getRoom().get(i).getTotalPriceCountry());

                                        JSONArray wallArray = new JSONArray();

                                        for (int j = 0; j < response.getData().getRoom().get(i).getWalls().size(); j++) {

                                            JSONObject wallObject = new JSONObject();

                                            wallObject.put("name", response.getData().getRoom().get(i).getWalls().get(j).getName());
                                            wallObject.put("product_id", response.getData().getRoom().get(i).getWalls().get(j).getProductId());
                                            wallObject.put("product_code", response.getData().getRoom().get(i).getWalls().get(j).getProductCode());
                                            wallObject.put("product_image", response.getData().getRoom().get(i).getWalls().get(j).getProductImage());
                                            wallObject.put("product_name", response.getData().getRoom().get(i).getWalls().get(j).getProductName());
                                            wallObject.put("wall_area", response.getData().getRoom().get(i).getWalls().get(j).getWallArea());
                                            wallObject.put("wall_area_unit", response.getData().getRoom().get(i).getWalls().get(j).getWallAreaUnit());
                                            wallObject.put("wall_cost", response.getData().getRoom().get(i).getWalls().get(j).getWallCost());
                                            wallObject.put("wall_cost_country", response.getData().getRoom().get(i).getWalls().get(j).getWallCostCountry());
                                            if (response.getData().getRoom().get(i).getWalls().get(j).getWallImage() != null) {
                                                if (response.getData().getRoom().get(i).getWalls().get(j).getWallImage().size() > 0) {
                                                    JSONArray imagesArray = new JSONArray();
                                                    imagesArray.put(response.getData().getRoom().get(i).getWalls().get(j).getWallImage().get(0));
                                                    wallObject.put("wall_image", imagesArray);
                                                }
                                            }

                                            wallArray.put(wallObject);
                                        }

                                        roomObject.put("walls", wallArray);
                                    }

                                    roomArray.put(roomObject);
                                }

                                dataObject.put("rooms", roomArray);
                                mainObject.put("data", dataObject);

                                Helper.jsonFileWrite(context, mainObject.toString(), "first_quote.json");

                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    } else {
                        new MyToast(context, "All Filed Required.");
                    }


                cancel();
                if(type==4){
                    ((Activity) context).setResult(10001);
                    ((Activity) context).finish();
                }else {
                    Intent intent = new Intent(context, QuoteActivity.class);
                    intent.putExtra("PRODUCT", (new Gson()).toJson(product));
                    intent.putExtra("EDITABLE", true);
                    intent.putExtra("INQUIRE_ID", inquire_id);
                    context.startActivity(intent);
                    ((Activity) context).finish();
                }

            }
        });

        WindowManager.LayoutParams layoutParams = new WindowManager.LayoutParams();
        layoutParams.copyFrom(getWindow().getAttributes());
        layoutParams.width = WindowManager.LayoutParams.MATCH_PARENT;
        layoutParams.height = WindowManager.LayoutParams.MATCH_PARENT;

        getWindow().setAttributes(layoutParams);
    }

    //add typed data to the shared preferance
    private void storeFieldsData() {
        mySharedPreferences = new MySharedPreferences(context);

//        if (mySharedPreferences.getQuotValues(INQUIRE_ID).equals("")) {
            mySharedPreferences.setQuotValues(HEIGHT, etHeight.getText().toString().trim());
            mySharedPreferences.setQuotValues(WIDTH, etWidth.getText().toString().trim());
            mySharedPreferences.setQuotValues(REDUCTION_HEIGHT, etRedHeight.getText().toString().trim());
            mySharedPreferences.setQuotValues(REDUCTION_WIDTH, etRedWidth.getText().toString().trim());
            mySharedPreferences.setQuotValues(NO_OF_SURFACES, etNoWalls.getText().toString().trim());
            mySharedPreferences.setQuotValues(INQUIRE_ID, inquire_id);
            mySharedPreferences.setQuotValues(PRODUCT_ID, product.getProductId());
//        }
    }

    //get typed data from shared preferances
    private void getFieldsData() {
        mySharedPreferences = new MySharedPreferences(context);

        String shared_inquire_id = mySharedPreferences.getQuotValues(INQUIRE_ID);
        String shared_product_id = mySharedPreferences.getQuotValues(PRODUCT_ID);

        Log.e("InQ ID", mySharedPreferences.getQuotValues(INQUIRE_ID));

        if (shared_inquire_id.equals(inquire_id) && shared_product_id.equals(product.getProductId())) {
            etHeight.setText(mySharedPreferences.getQuotValues(HEIGHT));
            etWidth.setText(mySharedPreferences.getQuotValues(WIDTH));
            etRedHeight.setText(mySharedPreferences.getQuotValues(REDUCTION_HEIGHT));
            etRedWidth.setText(mySharedPreferences.getQuotValues(REDUCTION_WIDTH));
            etNoWalls.setText(mySharedPreferences.getQuotValues(NO_OF_SURFACES));
        }

    }

    //clear datat from shared preferances
    private void clearCachedData() {
        mySharedPreferences = new MySharedPreferences(context);

        mySharedPreferences.setQuotValues(INQUIRE_ID, "");
        mySharedPreferences.setQuotValues(PRODUCT_ID, "");
    }

}
