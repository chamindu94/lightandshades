/*
 * Copyright (c) 2017. A.G.THAMAYS <gthamays@gmail.com>
 * This file is part of Light & Shade.
 * Light & Shade can not be copied and/or distribute without the express
 * permission of A.G.THAMAYS
 */

package com.agtapp.lightandshade;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class GPRSDeactiveActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gprsdeactive);
    }

    public void goToLogin(View view) {
        startActivity(new Intent(GPRSDeactiveActivity.this, LoginActivity.class));
    }
}
