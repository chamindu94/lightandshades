/*
 * Copyright (c) 2018. A.G.THAMAYS <gthamays@gmail.com>
 * This file is part of Light & Shade.
 * Light & Shade can not be copied and/or distribute without the express
 * permission of A.G.THAMAYS
 */

package com.agtapp.lightandshade;


import android.support.multidex.MultiDexApplication;


import com.agtapp.lightandshade.model.Customer;
import com.agtapp.lightandshade.model.Me;

public class MainApplication extends MultiDexApplication {

     public Me me = new Me();
     public Customer cus = new Customer();


    @Override
    public void onCreate() {
        super.onCreate();

    }


}
