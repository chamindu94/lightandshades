/*
 * Copyright (c) 2017. A.G.THAMAYS <gthamays@gmail.com>
 * This file is part of Light & Shade.
 * Light & Shade can not be copied and/or distribute without the express
 * permission of A.G.THAMAYS
 */

package com.agtapp.lightandshade.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by A.G.THAMAYS on 10/9/2017.
 */

public class Brand {

    @SerializedName("id")
    private int id;
    @SerializedName("name")
    private String name;
    @SerializedName("description")
    private String description;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
