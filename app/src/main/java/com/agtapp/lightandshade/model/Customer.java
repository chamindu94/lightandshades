package com.agtapp.lightandshade.model;

/**
 * Created by fawazjainudeen on 19/04/2018.
 */

public class Customer {
    int id ;
    private String name;
    private   String email;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
