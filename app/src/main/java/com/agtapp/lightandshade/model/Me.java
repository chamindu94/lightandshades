/*
 * Copyright (c) 2018. A.G.THAMAYS <gthamays@gmail.com>
 * This file is part of Light & Shade.
 * Light & Shade can not be copied and/or distribute without the express
 * permission of A.G.THAMAYS
 */

package com.agtapp.lightandshade.model;

/**
 * Created by Mayumi on 3/2/2018.
 */

public class Me {

    int id ;
    String name;
    String email;

    public float getDiscountRange() {
        return discountRange;
    }

    public void setDiscountRange(float discountRange) {
        this.discountRange = discountRange;
    }

    float discountRange;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
