/*
 * Copyright (c) 2017. A.G.THAMAYS <gthamays@gmail.com>
 * This file is part of Light & Shade.
 * Light & Shade can not be copied and/or distribute without the express
 * permission of A.G.THAMAYS
 */

package com.agtapp.lightandshade.model;

import com.google.gson.annotations.SerializedName;

public class Quotation {

    @SerializedName("quotation_type")
    private String quotationType;
    @SerializedName("quotation_id")
    private String quotationId;
    @SerializedName("quotation_create_date")
    private String quotationCreateDate;
    @SerializedName("quotation_create_time")
    private String quotationCreateTime;
    @SerializedName("total_cost")
    private String quotationTotalCost;

    public String getQuotationType() {
        return quotationType;
    }

    public void setQuotationType(String quotationType) {
        this.quotationType = quotationType;
    }

    public String getQuotationId() {
        return quotationId;
    }

    public void setQuotationId(String quotationId) {
        this.quotationId = quotationId;
    }

    public String getQuotationCreateDate() {
        return quotationCreateDate;
    }

    public void setQuotationCreateDate(String quotationCreateDate) {
        this.quotationCreateDate = quotationCreateDate;
    }

    public String getQuotationCreateTime() {
        return quotationCreateTime;
    }

    public void setQuotationCreateTime(String quotationCreateTime) {
        this.quotationCreateTime = quotationCreateTime;
    }

    public String getQuotationTotalCost() {
        return quotationTotalCost;
    }

    public void setQuotationTotalCost(String quotationTotalCost) {
        this.quotationTotalCost = quotationTotalCost;
    }
}
