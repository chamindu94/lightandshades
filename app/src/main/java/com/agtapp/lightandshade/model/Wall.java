/*
 * Copyright (c) 2017. A.G.THAMAYS <gthamays@gmail.com>
 * This file is part of Light & Shade.
 * Light & Shade can not be copied and/or distribute without the express
 * permission of A.G.THAMAYS
 */

package com.agtapp.lightandshade.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Wall {

    @SerializedName("id")
    private int id;
    @SerializedName("name")
    private String name;
    @SerializedName("wall_area")
    private String wallArea;
    @SerializedName("wall_area_unit")
    private String wallAreaUnit;
    @SerializedName("wall_cost")
    private String wallCost;
    @SerializedName("wall_cost_country")
    private String wallCostCountry;
    @SerializedName("product_id")
    private int productId;
    @SerializedName("product_name")
    private String productName;
    @SerializedName("product_code")
    private String productCode;
    @SerializedName("product_image")
    private String productImage;
    @SerializedName("product_unit_price")
    private String productUnitPrice;
    @SerializedName("product_category")
    private String productCategory;
    @SerializedName("product_brand")
    private String productBrand;
    @SerializedName("wall_image")
    private List<String> wallImage;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getWallArea() {
        return wallArea;
    }

    public void setWallArea(String wallArea) {
        this.wallArea = wallArea;
    }

    public String getWallAreaUnit() {
        return wallAreaUnit;
    }

    public void setWallAreaUnit(String wallAreaUnit) {
        this.wallAreaUnit = wallAreaUnit;
    }

    public String getWallCost() {
        return wallCost;
    }

    public void setWallCost(String wallCost) {
        this.wallCost = wallCost;
    }

    public String getWallCostCountry() {
        return wallCostCountry;
    }

    public void setWallCostCountry(String wallCostCountry) {
        this.wallCostCountry = wallCostCountry;
    }

    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getProductCode() {
        return productCode;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    public String getProductImage() {
        return productImage;
    }

    public void setProductImage(String productImage) {
        this.productImage = productImage;
    }

    public String getProductUnitPrice() {
        return productUnitPrice;
    }

    public void setProductUnitPrice(String productUnitPrice) {
        this.productUnitPrice = productUnitPrice;
    }

    public String getProductCategory() {
        return productCategory;
    }

    public void setProductCategory(String productCategory) {
        this.productCategory = productCategory;
    }

    public String getProductBrand() {
        return productBrand;
    }

    public void setProductBrand(String productBrand) {
        this.productBrand = productBrand;
    }

    public List<String> getWallImage() {
        return wallImage;
    }

    public void setWallImage(List<String> wallImage) {
        this.wallImage = wallImage;
    }
}
