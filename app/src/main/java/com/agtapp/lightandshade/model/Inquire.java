/*
 * Copyright (c) 2017. A.G.THAMAYS <gthamays@gmail.com>
 * This file is part of Light & Shade.
 * Light & Shade can not be copied and/or distribute without the express
 * permission of A.G.THAMAYS
 */

package com.agtapp.lightandshade.model;

import com.google.gson.annotations.SerializedName;

public class Inquire {

    @SerializedName("inquiry_id")
    private String inquiryId;
    @SerializedName("created_user")
    private String createdUser;
    @SerializedName("created_user_type")
    private String createdUserType;
    @SerializedName("created_date")
    private String createdDate;

    @SerializedName("installation_id")
    private String jobId;
    @SerializedName("visit_date")
    private String visitDate;

    public String getInquiryId() {
        return inquiryId;
    }

    public void setInquiryId(String inquiryId) {
        this.inquiryId = inquiryId;
    }

    public String getCreatedUser() {
        return createdUser;
    }

    public void setCreatedUser(String createdUser) {
        this.createdUser = createdUser;
    }

    public String getCreatedUserType() {
        return createdUserType;
    }

    public void setCreatedUserType(String createdUserType) {
        this.createdUserType = createdUserType;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public String getJobId() {
        return jobId;
    }

    public void setJobId(String jobId) {
        this.jobId = jobId;
    }

    public String getVisitDate() {
        return visitDate;
    }

    public void setVisitDate(String visitDate) {
        this.visitDate = visitDate;
    }
}
