/*
 * Copyright (c) 2017. A.G.THAMAYS <gthamays@gmail.com>
 * This file is part of Light & Shade.
 * Light & Shade can not be copied and/or distribute without the express
 * permission of A.G.THAMAYS
 */

package com.agtapp.lightandshade;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import com.agtapp.lightandshade.util.CircleImageView;
import com.agtapp.lightandshade.util.Loading;
import com.agtapp.lightandshade.util.MySharedPreferences;
import com.agtapp.lightandshade.util.MyToast;
import com.agtapp.lightandshade.util.text_view.TVRobotoBold;
import com.agtapp.lightandshade.util.text_view.TVRobotoRegular;
import com.agtapp.lightandshade.web_service.ApiClient;
import com.agtapp.lightandshade.web_service.ApiInterface;
import com.agtapp.lightandshade.web_service.response.HomeResponse;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.squareup.picasso.Picasso;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = "MainActivity";
    private MainApplication application;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.tx_name)
    TVRobotoBold txName;
    @BindView(R.id.tx_job_count)
    TVRobotoBold txJobCount;
    @BindView(R.id.cd_job_count)
    CardView cdJobCount;
    @BindView(R.id.navigation_view)
    NavigationView navigationView;
    @BindView(R.id.drawer_layout)
    DrawerLayout drawerLayout;
    @BindView(R.id.nav_image_load)
    ProgressBar navImageLoad;
    @BindView(R.id.nav_profile)
    CircleImageView navProfile;
    @BindView(R.id.nav_tx_user_name)
    TVRobotoRegular navTxUserName;
    @BindView(R.id.nav_tx_user_email)
    TVRobotoRegular navTxUserEmail;
    @BindView(R.id.nav_product)
    LinearLayout navProduct;
    @BindView(R.id.nav_logout)
    LinearLayout navLogout;
    @BindView(R.id.swipe_refresh_layout)
    SwipeRefreshLayout swipeRefreshLayout;

    private Context context;
    private MySharedPreferences sharedPreferences;
    private Loading loading;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        context = this;
        sharedPreferences = new MySharedPreferences(this);
        loading = new Loading(context);

        setSupportActionBar(toolbar);

        ActionBarDrawerToggle mDrawerToggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar, R.string.app_name, R.string.app_name);
        application = (MainApplication) getApplication();
        drawerLayout.setDrawerListener(mDrawerToggle);
        mDrawerToggle.syncState();

        //get user category and hide product navigation item
        Log.e("User Category", sharedPreferences.getUserCategory("USER_CATEGORY"));
        if (!sharedPreferences.getUserCategory("USER_CATEGORY").contains("Sales Executive")) {
            navProduct.setVisibility(View.GONE);
        }

        getDetails(true);

        navProduct.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, ProductsActivity.class);
                intent.putExtra("NORMAL_VIEW", true);
                startActivity(intent);
            }
        });

        Log.i(TAG, "TOKEN: " + sharedPreferences.getToken());

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getDetails(false);
            }
        });

        navLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                sharedPreferences.logout(MainActivity.this);
            }
        });


    }

    private void getDetails(boolean normal_loading) {
        if (normal_loading)
            loading.show();

        if (!sharedPreferences.getUserCategory("USER_CATEGORY").contains("Sales Executive")) {
            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
            Call<JsonObject> call = apiService.getJobs("Bearer " + sharedPreferences.getToken());
            call.enqueue(new Callback<JsonObject>() {
                @Override
                public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {

                    HomeResponse json = new Gson().fromJson(response.body(), HomeResponse.class);

                    if (json.getStatus().equals("success")) {

                        Picasso.with(context).load(json.getData().getName())
                                .into(navProfile, new com.squareup.picasso.Callback() {
                                    @Override
                                    public void onSuccess() {
                                        navImageLoad.setVisibility(View.GONE);
                                    }

                                    @Override
                                    public void onError() {
                                        navProfile.setImageResource(R.drawable.ic_profile);
                                        navImageLoad.setVisibility(View.GONE);
                                    }
                                });

                        navTxUserName.setText(json.getData().getName());
                        navTxUserEmail.setText(json.getData().getEmail());
                        application.me.setName(json.getData().getName().toString());
                        application.me.setEmail(json.getData().getEmail().toString());
                        txName.setText(json.getData().getName());
                        if (json.getData().getJobCount() == 0) {
                            txJobCount.setText(String.valueOf(json.getData().getJobCount()));
                        } else {
                            txJobCount.setText(String.valueOf(json.getData().getJobCount()));
                            cdJobCount.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    Intent intent = new Intent(context, JobsActivity.class);
                                    startActivity(intent);
                                }
                            });
                        }
                    } else {
                        if (json.getCode().equals("401")) {
                            sharedPreferences.logout(context);
                        } else {
                            new MyToast(context, json.getMessage());
                        }
                    }

                    loading.cancel();
                    swipeRefreshLayout.setRefreshing(false);

                }

                @Override
                public void onFailure(Call<JsonObject> call, Throwable t) {
                    Log.e(TAG, t.toString());
                    loading.cancel();
                }
            });
        } else {
            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
            Call<JsonObject> call = apiService.getHome("Bearer " + sharedPreferences.getToken());
            call.enqueue(new Callback<JsonObject>() {
                @Override
                public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {

                    HomeResponse json = new Gson().fromJson(response.body(), HomeResponse.class);

                    Log.e("Home response", "" + response.body());

                    if (json.getStatus().equals("success")) {

                        Picasso.with(context).load(json.getData().getName())
                                .into(navProfile, new com.squareup.picasso.Callback() {
                                    @Override
                                    public void onSuccess() {
                                        navImageLoad.setVisibility(View.GONE);
                                    }

                                    @Override
                                    public void onError() {
                                        navProfile.setImageResource(R.drawable.ic_profile);
                                        navImageLoad.setVisibility(View.GONE);
                                    }
                                });

                        navTxUserName.setText(json.getData().getName());
                        navTxUserEmail.setText(json.getData().getEmail());
                        application.me.setName(json.getData().getName().toString());
                        application.me.setEmail(json.getData().getEmail().toString());
                        txName.setText(json.getData().getName());
                        if (json.getData().getJobCount() == 0) {
                            txJobCount.setText(String.valueOf(json.getData().getJobCount()));
                        } else {
                            txJobCount.setText(String.valueOf(json.getData().getJobCount()));
                            cdJobCount.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    Intent intent = new Intent(context, InquiriesActivity.class);
                                    startActivity(intent);
                                }
                            });
                        }
                    } else {
                        if (json.getCode().equals("401")) {
                            sharedPreferences.logout(context);
                        } else {
                            new MyToast(context, json.getMessage());
                        }
                    }

                    loading.cancel();
                    swipeRefreshLayout.setRefreshing(false);

                }

                @Override
                public void onFailure(Call<JsonObject> call, Throwable t) {
                    Log.e(TAG, t.toString());
                    loading.cancel();
                }
            });
        }


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
