package com.agtapp.lightandshade;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v13.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;

import com.agtapp.lightandshade.util.Loading;
import com.agtapp.lightandshade.util.MySharedPreferences;
import com.agtapp.lightandshade.util.location.LocationHelper;
import com.agtapp.lightandshade.util.text_view.TVRobotoBold;
import com.agtapp.lightandshade.util.text_view.TVRobotoRegular;
import com.agtapp.lightandshade.web_service.ApiClient;
import com.agtapp.lightandshade.web_service.ApiInterface;
import com.agtapp.lightandshade.web_service.request.InquireDetailsRequest;
import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.gson.JsonObject;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class JobDetailActivity extends AppCompatActivity {

    private static final String TAG = "JobDetailActivity";

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.tx_inquire_id)
    TVRobotoBold txInquireId;
    @BindView(R.id.tx_created_name_and_role)
    TVRobotoRegular txCreatedNameAndRole;
    @BindView(R.id.tx_created_date)
    TVRobotoRegular txCreatedDate;
    @BindView(R.id.tx_created_time)
    TVRobotoRegular txCreatedTime;
    @BindView(R.id.tx_customer_name)
    TVRobotoBold txCustomerName;
    @BindView(R.id.tx_customer_phone)
    TVRobotoRegular txCustomerPhone;
    @BindView(R.id.tx_customer_email)
    TVRobotoRegular txCustomerEmail;
    @BindView(R.id.tx_customer_address)
    TVRobotoRegular txCustomerAddress;
    @BindView(R.id.quotation_list)
    RecyclerView quotationList;
    @BindView(R.id.cd_quotation_view)
    CardView cdQuotationView;
    @BindView(R.id.cd_check_in)
    CardView cdCheckIn;
    @BindView(R.id.cd_create_quote)
    CardView cdCreateQuote;
    @BindView(R.id.ly_step_1)
    LinearLayout lyStep1;
    @BindView(R.id.cd_end_inquiry)
    CardView cdEndInquiry;
    @BindView(R.id.cd_cancel_inquiry)
    CardView cdCancelInquiry;
    @BindView(R.id.ly_step_2)
    LinearLayout lyStep2;
    @BindView(R.id.swipe_refresh_layout)
    SwipeRefreshLayout swipeRefreshLayout;
    @BindView(R.id.cd_quote_new)
    CardView cdQuoteNew;
    @BindView(R.id.btn_customer_location)
    Button btnCustomerLocation;

    private Context context;
    private Loading loading;
    private MySharedPreferences sharedPreferences;
    private String inquire_id;
    private boolean x = false;
    private String latitude, longitude;
    private LocationHelper locationHelper;
    private MainApplication application;

    private FusedLocationProviderClient mFusedLocationClient;

    private LocationManager locationManager;
    private Location mLastLocation;

    private String quotation_id;
    private String customer_id;

    private RequestQueue requestQueue;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_job_detail);
        ButterKnife.bind(this);

        cdCheckIn.setEnabled(true);
        context = this;
        sharedPreferences = new MySharedPreferences(this);
        loading = new Loading(context);
        application = (MainApplication) getApplication();

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        inquire_id = getIntent().getStringExtra("INQUIRE_ID");

        requestQueue = Volley.newRequestQueue(this);

        quotationList.setLayoutManager(new LinearLayoutManager(context));
        quotationList.setHasFixedSize(true);
        quotationList.setLayoutManager(new GridLayoutManager(context, 1, GridLayoutManager.VERTICAL, false));

        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);

        getDetails(true);

        //hide quotation view if not sales executive
        if (!sharedPreferences.getUserCategory("USER_CATEGORY").contains("Sales Executive")) {
            cdQuotationView.setVisibility(View.GONE);
        }

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getDetails(false);
            }
        });

        cdCheckIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.i(TAG, "Check In Click");

                Intent intent = new Intent(context, JobQuoteActivity.class);
                intent.putExtra("INQUIRE_ID", inquire_id);
                intent.putExtra("EDITABLE", false);
                intent.putExtra("QUOTE_ID", Integer.parseInt(quotation_id));
                context.startActivity(intent);

            }
        });

        cdCreateQuote.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        cdQuoteNew.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, ProductsActivity.class);
                intent.putExtra("INQUIRE_ID", inquire_id);
                intent.putExtra("update", false);
                startActivity(intent);
            }
        });

        cdEndInquiry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        cdCancelInquiry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        btnCustomerLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (ContextCompat.checkSelfPermission(JobDetailActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(JobDetailActivity.this,
                            new String[]{Manifest.permission.ACCESS_COARSE_LOCATION},
                            3);
                } else {
                    //get current location
                    mFusedLocationClient.getLastLocation().addOnSuccessListener(JobDetailActivity.this, new OnSuccessListener<Location>() {
                        @Override
                        public void onSuccess(Location location) {
                            if (location != null) {
                                Log.e(TAG, "Latitude : " + location.getLatitude());
                                Log.e(TAG, "Longitude : " + location.getLongitude());

                                getCustomerLocation();

                                Intent intent = new Intent(android.content.Intent.ACTION_VIEW,
                                        Uri.parse("http://maps.google.com/maps?saddr=" + location.getLatitude() + "," + location.getLongitude() + "&daddr="+latitude+","+longitude));
                                startActivity(intent);
                            } else {
                                Log.e(TAG, "Location Null");
                            }
                        }
                    });
                }

            }
        });
    }

    private void getDetails(boolean normal_loading) {
        if (normal_loading)
            loading.show();

        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<JsonObject> call = apiService.getJob("Bearer " + sharedPreferences.getToken(), new InquireDetailsRequest(inquire_id));
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> json) {

                Log.e("Jobs response", "" + json.body());

                JsonObject jsonObject_data = json.body().getAsJsonObject("data");

                quotation_id = jsonObject_data.get("quotation_id").toString();

                String inq_id = jsonObject_data.get("installation_id").toString();
                txInquireId.setText(inq_id.substring(1, inq_id.length() - 1));

                String visit_date = jsonObject_data.get("planned_visit_date").toString();
                txCreatedDate.setText(visit_date.substring(1, visit_date.length() - 1));

                String created_time = jsonObject_data.get("planned_visit_time").toString();
                txCreatedTime.setText(created_time.substring(1, created_time.length() - 1));

                customer_id = jsonObject_data.get("customer_id").toString();

                String cus_name = jsonObject_data.get("customer_name").toString();
                txCustomerName.setText(cus_name.substring(1, cus_name.length() - 1));

                String cus_mobile = jsonObject_data.get("customer_mobile").toString();
                txCustomerPhone.setText(cus_mobile.substring(1, cus_mobile.length() - 1));

                String cus_email = jsonObject_data.get("customer_email").toString();
                txCustomerEmail.setText(cus_email.substring(1, cus_email.length() - 1));

                String cus_address = jsonObject_data.get("customer_address").toString();
                txCustomerAddress.setText(cus_address.substring(1, cus_address.length() - 1));
//                    InquireDetailsResponse response = new Gson().fromJson(json.body(), InquireDetailsResponse.class);
//                    if (response.getStatus().equals("success")) {
//
//                        getSupportActionBar().setTitle("Inquiry | " + response.getData().getInquiry().getInquiryCode());
//
//                        txInquireId.setText(response.getData().getInquiry().getInquiryCode());
//                        txCreatedNameAndRole.setText(response.getData().getAgent().getAgentName()/* + " - " + response.getData().getInquiry().getCreatedUserType()*/);
//                        txCreatedDate.setText(response.getData().getInquiry().getCreatedDate());
//                        txCreatedTime.setText(response.getData().getInquiry().getCreatedTime());
//
//                        txCustomerName.setText(response.getData().getCustomer().getCustomerName());
//                        txCustomerPhone.setText(response.getData().getCustomer().getCustomerMobile());
//                        txCustomerEmail.setText(response.getData().getCustomer().getCustomerEmail());
//                        txCustomerAddress.setText(response.getData().getCustomer().getCustomerAddress());
//
//
//                        application.me.setDiscountRange(Float.valueOf(response.getData().getInquiry().getMaximumDiscount()));
//                        application.cus.setName(response.getData().getCustomer().getCustomerName());
//                        application.cus.setEmail(response.getData().getCustomer().getCustomerEmail());
//
//
//
//                        //application.cus.setEmail(response.getData().getCustomer().getCustomerEmail().toString());
//                        // application.cus.setName(response.getData().getCustomer().getCustomerName().toString());
//
//                        Log.i(TAG, "onResponse: " + response.getData().isQuotationCreated());
//                        if (response.getData().isQuotationCreated()) {
//                            cdQuotationView.setVisibility(View.VISIBLE);
//                            lyStep1.setVisibility(View.GONE);
//                            lyStep2.setVisibility(View.VISIBLE);
//                            InquireDetailsQuotationListAdapter adapter = new InquireDetailsQuotationListAdapter(response.getData().getQuotation(), context, inquiriesDetailQuotationItemClickListener);
//                            quotationList.setAdapter(adapter);
//                        } else {
//                            lyStep1.setVisibility(View.VISIBLE);
//                            lyStep2.setVisibility(View.GONE);
//                            cdQuotationView.setVisibility(View.GONE);
//                        }
//
//                    } else {
//                        if (response.getCode().equals("401")) {
//                            sharedPreferences.logout(context);
//                        } else {
//                            new MyToast(context, response.getMessage());
//                        }
//                    }

                loading.cancel();
                swipeRefreshLayout.setRefreshing(false);

            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                Log.e(TAG, t.toString());
                loading.cancel();
            }
        });

    }

    private void getCustomerLocation() {
        String url = "http://applns.lnsdigital.com/wallapi/public/getcuslocation";

        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new com.android.volley.Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("Response getLocation", response);
                        try {

                            JSONObject jsonObject = new JSONObject(response);
                            JSONObject data = jsonObject.getJSONObject("data");

                            latitude = data.getString("cus_latt");
                            longitude = data.getString("cus_long");

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                },
                new com.android.volley.Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e("Error getLocation", error.getMessage());
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> params = new HashMap<>();
                params.put("id", customer_id);

                Log.e("Params", params.toString());

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String,String> params = new HashMap<String,String>();
                params.put("Authorization", "Bearer " + sharedPreferences.getToken());
                return params;
            }
        };

        requestQueue.add(stringRequest);
    }
}
