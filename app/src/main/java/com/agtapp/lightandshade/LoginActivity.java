/*
 * Copyright (c) 2017. A.G.THAMAYS <gthamays@gmail.com>
 * This file is part of Light & Shade.
 * Light & Shade can not be copied and/or distribute without the express
 * permission of A.G.THAMAYS
 */

package com.agtapp.lightandshade;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.text.TextUtils;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.Toast;

import com.agtapp.lightandshade.util.Helper;
import com.agtapp.lightandshade.util.Loading;
import com.agtapp.lightandshade.util.MySharedPreferences;
import com.agtapp.lightandshade.util.MyToast;
import com.agtapp.lightandshade.util.edit_text.ETRobotoRegular;
import com.agtapp.lightandshade.util.text_view.TVRobotoRegular;
import com.agtapp.lightandshade.web_service.ApiClient;
import com.agtapp.lightandshade.web_service.ApiInterface;
import com.agtapp.lightandshade.web_service.request.LoginRequest;
import com.agtapp.lightandshade.web_service.response.LoginResponse;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.R.attr.password;

public class LoginActivity extends AppCompatActivity {

    private static final String TAG = "LoginActivity";

    @BindView(R.id.et_email)
    ETRobotoRegular etEmail;
    @BindView(R.id.et_password)
    ETRobotoRegular etPassword;
    @BindView(R.id.passwordView)
    CheckBox passwordView;
    @BindView(R.id.tx_forgot_password)
    TVRobotoRegular txForgotPassword;
    @BindView(R.id.bu_login)
    CardView buLogin;

    private Context context;
    private Loading loading;
    private MySharedPreferences sharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);

        context = this;
        sharedPreferences = new MySharedPreferences(this);
        loading = new Loading(context);

        passwordView.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                if (!isChecked) {
                    etPassword.setTransformationMethod(new PasswordTransformationMethod());
                    etPassword.setSelection(etPassword.getText().length());
                    passwordView.setCompoundDrawablesWithIntrinsicBounds(null, null, getResources().getDrawable(R.drawable.ic_password_view), null);
                } else {
                    etPassword.setTransformationMethod(null);
                    etPassword.setSelection(etPassword.getText().length());
                    passwordView.setCompoundDrawablesWithIntrinsicBounds(null, null, getResources().getDrawable(R.drawable.ic_password_hide), null);
                }
            }
        });

        buLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (TextUtils.isEmpty(etEmail.getText().toString())) {
                    new MyToast(context, "All fields are Required to be Filled.");
                } else if (!Helper.isValidEmail(etEmail.getText().toString())) {
                    new MyToast(context, "The Email you have entered is Invalid. Please try again.");
                } else if (TextUtils.isEmpty(etPassword.getText().toString())) {
                    new MyToast(context, "All fields are Required to be Filled.");
                } else {
                    doLogin();
                }
            }
        });

    }

    private void doLogin() {
        loading.show();

        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<JsonObject> call = apiService.doLogin(new LoginRequest(etEmail.getText().toString(), etPassword.getText().toString()));
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {

                if (response.body().getAsJsonPrimitive("status").getAsString().equals("success")) {
                    sharedPreferences.setLoggedIn(true, response.body().getAsJsonObject("data").getAsJsonPrimitive("api_token").getAsString());

                    //get user level
                    JsonArray userLevel_array = response.body().getAsJsonObject("data").getAsJsonArray("user_level");
                    for (int i = 0; i < userLevel_array.size(); i++) {
                        JsonObject userLevel_object = userLevel_array.get(i).getAsJsonObject();

                        if (userLevel_object.get("name").toString().equalsIgnoreCase("Sales Executive")
                                || userLevel_object.get("name").toString().contains("Sales Executive")) {
                            sharedPreferences.setUserCategory("USER_CATEGORY", userLevel_object.get("name").toString());
                        }
                    }

                    Intent intent = new Intent(context, MainActivity.class);
                    startActivity(intent);
                }

                new MyToast(context, response.body().getAsJsonPrimitive("message").getAsString());
                loading.cancel();

            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                Log.e(TAG, t.toString());
                loading.cancel();
            }
        });
    }
}
