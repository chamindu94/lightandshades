/*
 * Copyright (c) 2017. A.G.THAMAYS <gthamays@gmail.com>
 * This file is part of Light & Shade.
 * Light & Shade can not be copied and/or distribute without the express
 * permission of A.G.THAMAYS
 */

package com.agtapp.lightandshade;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.agtapp.lightandshade.adapter.InquiriesListAdapter;
import com.agtapp.lightandshade.util.Loading;
import com.agtapp.lightandshade.util.MySharedPreferences;
import com.agtapp.lightandshade.util.MyToast;
import com.agtapp.lightandshade.util.interfaces.item_click_listener.InquiriesItemClickListener;
import com.agtapp.lightandshade.util.text_view.TVRobotoRegular;
import com.agtapp.lightandshade.web_service.ApiClient;
import com.agtapp.lightandshade.web_service.ApiInterface;
import com.agtapp.lightandshade.web_service.response.InquiriesListResponse;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class InquiriesActivity extends AppCompatActivity implements InquiriesItemClickListener {

    private static final String TAG = "InquiriesActivity";

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.list)
    RecyclerView list;
    @BindView(R.id.tx_short_title)
    TVRobotoRegular txShortTitle;
    @BindView(R.id.ly_short)
    LinearLayout lyShort;
    @BindView(R.id.tx_selected_date)
    TVRobotoRegular txSelectedDate;
    @BindView(R.id.ly_date)
    LinearLayout lyDate;
    @BindView(R.id.swipe_refresh_layout)
    SwipeRefreshLayout swipeRefreshLayout;

    private Context context;
    private InquiriesItemClickListener inquiriesItemClickListener;
    private Loading loading;
    private MySharedPreferences sharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inquiries);
        ButterKnife.bind(this);

        context = this;
        sharedPreferences = new MySharedPreferences(this);
        loading = new Loading(context);
        inquiriesItemClickListener = this;

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        list.setLayoutManager(new LinearLayoutManager(context));
        list.setHasFixedSize(true);
        list.setLayoutManager(new GridLayoutManager(context, 1, GridLayoutManager.VERTICAL, false));

        getDetails(true);

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getDetails(false);
            }
        });
    }

    private void getDetails(boolean normal_loading) {
        if (normal_loading)
            loading.show();

        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<JsonObject> call = apiService.getInquiriesList("Bearer " + sharedPreferences.getToken());
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> json) {

                Log.e("Response", "" + json.body());

                InquiriesListResponse response = new Gson().fromJson(json.body(), InquiriesListResponse.class);
                if (response.getStatus().equals("success")) {
                    InquiriesListAdapter adapter = new InquiriesListAdapter(response.getData(), context, inquiriesItemClickListener);
                    list.setAdapter(adapter);
                } else {
                    if (response.getCode().equals("401")) {
                        sharedPreferences.logout(context);
                    } else {
                        new MyToast(context, response.getMessage());
                    }
                }

                loading.cancel();
                swipeRefreshLayout.setRefreshing(false);

            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                Log.e(TAG, t.toString());
                loading.cancel();
            }
        });
    }

    @Override
    public void recyclerViewListClicked(View v, int position, String id) {
        Intent intent = new Intent(context, InquireDetailActivity.class);
        intent.putExtra("INQUIRE_ID", id);
        startActivity(intent);

        Log.e("Inq ID", id);
    }
}
