/*
 * Copyright (c) 2017. A.G.THAMAYS <gthamays@gmail.com>
 * This file is part of Light & Shade.
 * Light & Shade can not be copied and/or distribute without the express
 * permission of A.G.THAMAYS
 */

package com.agtapp.lightandshade.web_service.request;

import com.google.gson.annotations.SerializedName;

/**
 * Created by A.G.THAMAYS on 10/8/2017.
 */

public class ProductDetailsRequest {

    @SerializedName("id")
    private String id;

    public ProductDetailsRequest(String id) {
        this.id = id;
    }
}
