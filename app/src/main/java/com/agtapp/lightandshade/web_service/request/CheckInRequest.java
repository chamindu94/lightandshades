package com.agtapp.lightandshade.web_service.request;

import com.google.gson.annotations.SerializedName;

/**
 * Created by fawazjainudeen on 04/06/2018.
 */

public class CheckInRequest {

    @SerializedName("id")
    private String id;

    public CheckInRequest(String id) {
        this.id = id;
    }
}
