/*
 * Copyright (c) 2017. A.G.THAMAYS <gthamays@gmail.com>
 * This file is part of Light & Shade.
 * Light & Shade can not be copied and/or distribute without the express
 * permission of A.G.THAMAYS
 */

package com.agtapp.lightandshade.web_service;

import com.agtapp.lightandshade.util.MySharedPreferences;
import com.agtapp.lightandshade.web_service.request.CheckInRequest;
import com.agtapp.lightandshade.web_service.request.InquireDetailsRequest;
import com.agtapp.lightandshade.web_service.request.LoginRequest;
import com.agtapp.lightandshade.web_service.request.ProductDetailsRequest;
import com.agtapp.lightandshade.web_service.request.QuoteRequest;
import com.agtapp.lightandshade.web_service.response.QuoteResponse;
import com.google.gson.JsonObject;

import org.json.JSONObject;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Header;
import retrofit2.http.POST;

public interface ApiInterface {

    @POST("login")
    Call<JsonObject> doLogin(@Body LoginRequest request);

    @POST("jobs")
    Call<JsonObject> getJobs(@Header("Authorization") String authHeader);

    @POST("home")
    Call<JsonObject> getHome(@Header("Authorization") String authHeader);

    @POST("viewinquiries")
    Call<JsonObject> getInquiriesList(@Header("Authorization") String authHeader);

    @POST("viewjobs")
    Call<JsonObject> getJobsList(@Header("Authorization") String authHeader);

    @POST("viewspecificinquiry")
    Call<JsonObject> getInquire(@Header("Authorization") String authHeader, @Body InquireDetailsRequest request);

    @POST("viewspecificjob")
    Call<JsonObject> getJob(@Header("Authorization") String authHeader, @Body InquireDetailsRequest request);

    @POST("updatecheckin")
    Call<JsonObject> setChekIn(@Header("Authorization") String authHeader, @Body CheckInRequest request);

    @POST("viewspecificquotation")
    Call<JsonObject> getQuote(@Header("Authorization") String authHeader, @Body QuoteRequest request);

    @POST("viewproducts")
    Call<JsonObject> getProductsList(@Header("Authorization") String authHeader);

    @POST("viewspecificproduct")
    Call<JsonObject> getProduct(@Header("Authorization") String authHeader, @Body ProductDetailsRequest request);

    @POST("quotation/create")
    Call<JsonObject> setQuote(@Header("Authorization") String authHeader, @Body QuoteResponse request);

}