/*
 * Copyright (c) 2017. A.G.THAMAYS <gthamays@gmail.com>
 * This file is part of Light & Shade.
 * Light & Shade can not be copied and/or distribute without the express
 * permission of A.G.THAMAYS
 */

package com.agtapp.lightandshade.web_service.response;

import com.agtapp.lightandshade.model.Quotation;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class InquireDetailsResponse {

    @SerializedName("status")
    private String status;
    @SerializedName("code")
    private String code;
    @SerializedName("message")
    private String message;
    @SerializedName("data")
    private DataBean data;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        @SerializedName("inquiry")
        private InquiryBean inquiry;
        @SerializedName("customer")
        private CustomerBean customer;
        @SerializedName("agent")
        private AgentBean agent;
        @SerializedName("quotation_created")
        private boolean quotationCreated;
        @SerializedName("quotation")
        private List<Quotation> quotation;

        public InquiryBean getInquiry() {
            return inquiry;
        }

        public void setInquiry(InquiryBean inquiry) {
            this.inquiry = inquiry;
        }

        public CustomerBean getCustomer() {
            return customer;
        }

        public void setCustomer(CustomerBean customer) {
            this.customer = customer;
        }

        public AgentBean getAgent() {
            return agent;
        }

        public void setAgent(AgentBean agent) {
            this.agent = agent;
        }

        public boolean isQuotationCreated() {
            return quotationCreated;
        }

        public void setQuotationCreated(boolean quotationCreated) {
            this.quotationCreated = quotationCreated;
        }

        public List<Quotation> getQuotation() {
            return quotation;
        }

        public void setQuotation(List<Quotation> quotation) {
            this.quotation = quotation;
        }

        public static class InquiryBean {
            @SerializedName("inquiry_id")
            private int inquiryId;
            @SerializedName("inquiry_code")
            private String inquiryCode;
            @SerializedName("planned_visit_date")
            private String plannedVisitDate;
            @SerializedName("planned_visit_time")
            private String plannedVisitTime;
            @SerializedName("actual_visit_date")
            private String actualVisitDate;
            @SerializedName("actual_visit_time")
            private String actualVisitTime;
            @SerializedName("inquiry_source")
            private String inquirySource;
            @SerializedName("maximum_discount")
            private int maximumDiscount;
            @SerializedName("cancel_reason")
            private Object cancelReason;
            @SerializedName("status")
            private String status;
            @SerializedName("created_date")
            private String createdDate;
            @SerializedName("created_time")
            private String createdTime;

            public int getInquiryId() {
                return inquiryId;
            }

            public void setInquiryId(int inquiryId) {
                this.inquiryId = inquiryId;
            }

            public String getInquiryCode() {
                return inquiryCode;
            }

            public void setInquiryCode(String inquiryCode) {
                this.inquiryCode = inquiryCode;
            }

            public String getPlannedVisitDate() {
                return plannedVisitDate;
            }

            public void setPlannedVisitDate(String plannedVisitDate) {
                this.plannedVisitDate = plannedVisitDate;
            }

            public String getPlannedVisitTime() {
                return plannedVisitTime;
            }

            public void setPlannedVisitTime(String plannedVisitTime) {
                this.plannedVisitTime = plannedVisitTime;
            }

            public String getActualVisitDate() {
                return actualVisitDate;
            }

            public void setActualVisitDate(String actualVisitDate) {
                this.actualVisitDate = actualVisitDate;
            }

            public String getActualVisitTime() {
                return actualVisitTime;
            }

            public void setActualVisitTime(String actualVisitTime) {
                this.actualVisitTime = actualVisitTime;
            }

            public String getInquirySource() {
                return inquirySource;
            }

            public void setInquirySource(String inquirySource) {
                this.inquirySource = inquirySource;
            }

            public int getMaximumDiscount() {
                return maximumDiscount;
            }

            public void setMaximumDiscount(int maximumDiscount) {
                this.maximumDiscount = maximumDiscount;
            }

            public Object getCancelReason() {
                return cancelReason;
            }

            public void setCancelReason(Object cancelReason) {
                this.cancelReason = cancelReason;
            }

            public String getStatus() {
                return status;
            }

            public void setStatus(String status) {
                this.status = status;
            }

            public String getCreatedDate() {
                return createdDate;
            }

            public void setCreatedDate(String createdDate) {
                this.createdDate = createdDate;
            }

            public String getCreatedTime() {
                return createdTime;
            }

            public void setCreatedTime(String createdTime) {
                this.createdTime = createdTime;
            }
        }

        public static class CustomerBean {
            @SerializedName("customer_id")
            private int customerId;
            @SerializedName("customer_name")
            private String customerName;
            @SerializedName("customer_address")
            private String customerAddress;
            @SerializedName("customer_mobile")
            private String customerMobile;
            @SerializedName("customer_telephone")
            private String customerTelephone;
            @SerializedName("customer_email")
            private String customerEmail;

            public int getCustomerId() {
                return customerId;
            }

            public void setCustomerId(int customerId) {
                this.customerId = customerId;
            }

            public String getCustomerName() {
                return customerName;
            }

            public void setCustomerName(String customerName) {
                this.customerName = customerName;
            }

            public String getCustomerAddress() {
                return customerAddress;
            }

            public void setCustomerAddress(String customerAddress) {
                this.customerAddress = customerAddress;
            }

            public String getCustomerMobile() {
                return customerMobile;
            }

            public void setCustomerMobile(String customerMobile) {
                this.customerMobile = customerMobile;
            }

            public String getCustomerTelephone() {
                return customerTelephone;
            }

            public void setCustomerTelephone(String customerTelephone) {
                this.customerTelephone = customerTelephone;
            }

            public String getCustomerEmail() {
                return customerEmail;
            }

            public void setCustomerEmail(String customerEmail) {
                this.customerEmail = customerEmail;
            }
        }

        public static class AgentBean {
            @SerializedName("agent_id")
            private int agentId;
            @SerializedName("agent_name")
            private String agentName;
            @SerializedName("agent_phone_number")
            private String agentPhoneNumber;
            @SerializedName("agent_email")
            private String agentEmail;

            public int getAgentId() {
                return agentId;
            }

            public void setAgentId(int agentId) {
                this.agentId = agentId;
            }

            public String getAgentName() {
                return agentName;
            }

            public void setAgentName(String agentName) {
                this.agentName = agentName;
            }

            public String getAgentPhoneNumber() {
                return agentPhoneNumber;
            }

            public void setAgentPhoneNumber(String agentPhoneNumber) {
                this.agentPhoneNumber = agentPhoneNumber;
            }

            public String getAgentEmail() {
                return agentEmail;
            }

            public void setAgentEmail(String agentEmail) {
                this.agentEmail = agentEmail;
            }
        }
    }
}
