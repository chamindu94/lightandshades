package com.agtapp.lightandshade.web_service.response;

import com.google.gson.annotations.SerializedName;

/**
 * Created by fawazjainudeen on 04/06/2018.
 */

public class CheckInResponse {


    @SerializedName("status")
    private String status;
    @SerializedName("code")
    private String code;
    @SerializedName("message")
    private String message;


    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

}
