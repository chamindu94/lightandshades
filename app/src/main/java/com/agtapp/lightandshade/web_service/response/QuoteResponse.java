/*
 * Copyright (c) 2017. A.G.THAMAYS <gthamays@gmail.com>
 * This file is part of Light & Shade.
 * Light & Shade can not be copied and/or distribute without the express
 * permission of A.G.THAMAYS
 */

package com.agtapp.lightandshade.web_service.response;

import com.agtapp.lightandshade.model.Room;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class QuoteResponse {

    @SerializedName("status")
    private String status;
    @SerializedName("code")
    private int code;
    @SerializedName("message")
    private String message;
    @SerializedName("data")
    private DataBean data;

    public QuoteResponse(String status, int code, String message, DataBean data) {
        this.status = status;
        this.code = code;
        this.message = message;
        this.data = data;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        @SerializedName("quote_id")
        private int quoteId;
        @SerializedName("installation_fee")
        private String installationFee;
        @SerializedName("transport_fee")
        private String transportFee;
        @SerializedName("removable_fee")
        private String removableFee;
        @SerializedName("cleaning_fee")
        private String cleaningFee;
        @SerializedName("other_fee")
        private String otherFee;
        @SerializedName("discount")
        private String discount;
        @SerializedName("total_cost")
        private String totalCost;
        @SerializedName("total_cost_country")
        private String totalCostCountry;
        @SerializedName("inquiry_id")
        private String inquiryId;
        @SerializedName("rooms")
        private List<Room> room;

        public int getQuoteId() {
            return quoteId;
        }

        public void setQuoteId(int quoteId) {
            this.quoteId = quoteId;
        }

        public String getInstallationFee() {
            return installationFee;
        }

        public void setInstallationFee(String installationFee) {
            this.installationFee = installationFee;
        }

        public String getTransportFee() {
            return transportFee;
        }

        public void setTransportFee(String transportFee) {
            this.transportFee = transportFee;
        }

        public String getRemovableFee() {
            return removableFee;
        }

        public void setRemovableFee(String removableFee) {
            this.removableFee = removableFee;
        }

        public String getCleaningFee() {
            return cleaningFee;
        }

        public void setCleaningFee(String cleaningFee) {
            this.cleaningFee = cleaningFee;
        }

        public String getOtherFee() {
            return otherFee;
        }

        public void setOtherFee(String otherFee) {
            this.otherFee = otherFee;
        }

        public String getDiscount() {
            return discount;
        }

        public void setDiscount(String discount) {
            this.discount = discount;
        }

        public String getTotalCost() {
            return totalCost;
        }

        public void setTotalCost(String totalCost) {
            this.totalCost = totalCost;
        }

        public String getTotalCostCountry() {
            return totalCostCountry;
        }

        public void setTotalCostCountry(String totalCostCountry) {
            this.totalCostCountry = totalCostCountry;
        }

        public String getInquiryId() {
            return inquiryId;
        }

        public void setInquiryId(String inquiryId) {
            this.inquiryId = inquiryId;
        }

        public List<Room> getRoom() {
            return room;
        }

        public void setRoom(List<Room> room) {
            this.room = room;
        }
    }
}
