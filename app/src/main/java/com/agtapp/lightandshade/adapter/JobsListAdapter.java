package com.agtapp.lightandshade.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;

import com.agtapp.lightandshade.R;
import com.agtapp.lightandshade.model.Inquire;
import com.agtapp.lightandshade.util.interfaces.item_click_listener.InquiriesItemClickListener;
import com.agtapp.lightandshade.util.text_view.TVRobotoRegular;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class JobsListAdapter extends RecyclerView.Adapter<JobsListAdapter.ViewHolder> implements Filterable {

    private static final String TAG = "JobsListAdapter";

    private Context context;
    private OrderListFilter inquireListFilter;
    private List<Inquire> inquireFilterList;
    private List<Inquire> inquireListOriginal;
    private List<Inquire> inquireListFiltered;
    private InquiriesItemClickListener itemListener;

    public JobsListAdapter(List<Inquire> inquireListOriginal, Context context, InquiriesItemClickListener itemListener) {
        this.inquireListOriginal = inquireListOriginal;
        this.inquireFilterList = inquireListOriginal;
        this.context = context;
        this.itemListener = itemListener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_job_list, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        holder.txInquireId.setText(inquireFilterList.get(position).getJobId());
        holder.txNameAndRole.setText(inquireFilterList.get(position).getCreatedUser()/* + "-" + inquireFilterList.get(position).getCreatedUserType()*/);
        holder.txDate.setText(inquireFilterList.get(position).getVisitDate());

    }

    @Override
    public int getItemCount() {
        return inquireFilterList.size();
    }

    @Override
    public Filter getFilter() {
        if (inquireListFilter == null)
            inquireListFilter = new OrderListFilter(this, inquireListOriginal);
        return inquireListFilter;
    }

    private class OrderListFilter extends Filter {

        private final JobsListAdapter adapter;
        private final List<Inquire> originalList;

        private OrderListFilter(JobsListAdapter adapter, List<Inquire> originalList) {
            super();
            this.adapter = adapter;
            this.originalList = new LinkedList<>(originalList);
            inquireListFiltered = new ArrayList<>();
        }

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            inquireListFiltered.clear();
            final FilterResults results = new FilterResults();

            if (constraint.length() == 0) {
                inquireListFiltered.addAll(originalList);
            } else {
                final String filterPattern = constraint.toString().toLowerCase().trim();

                for (final Inquire list : originalList) {
                    if (String.valueOf(list.getInquiryId()).toLowerCase().contains(filterPattern)) {
                        inquireListFiltered.add(list);
                    }
                }
            }
            results.values = inquireListFiltered;
            results.count = inquireListFiltered.size();
            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            adapter.inquireFilterList.clear();
            adapter.inquireFilterList.addAll((ArrayList<Inquire>) results.values);
            adapter.notifyDataSetChanged();
        }
    }

    class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        @BindView(R.id.tx_inquire_id)
        TVRobotoRegular txInquireId;
        @BindView(R.id.tx_name_and_role)
        TVRobotoRegular txNameAndRole;
        @BindView(R.id.tx_date)
        TVRobotoRegular txDate;

        ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
            view.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            itemListener.recyclerViewListClicked(v, this.getPosition(), inquireListOriginal.get(this.getPosition()).getJobId());
        }
    }
}
