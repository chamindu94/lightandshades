/*
 * Copyright (c) 2017. A.G.THAMAYS <gthamays@gmail.com>
 * This file is part of Light & Shade.
 * Light & Shade can not be copied and/or distribute without the express
 * permission of A.G.THAMAYS
 */

package com.agtapp.lightandshade.adapter;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;

import com.agtapp.lightandshade.R;
import com.agtapp.lightandshade.model.Quotation;
import com.agtapp.lightandshade.util.interfaces.item_click_listener.InquiriesDetailQuotationItemClickListener;
import com.agtapp.lightandshade.util.text_view.TVRobotoRegular;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class InquireDetailsQuotationListAdapter extends RecyclerView.Adapter<InquireDetailsQuotationListAdapter.ViewHolder> implements Filterable {

    private static final String TAG = "quotationDetailsQuotationListAdapter";

    private Context context;
    private OrderListFilter quotationListFilter;
    private List<Quotation> quotationFilterList;
    private List<Quotation> quotationListOriginal;
    private List<Quotation> quotationListFiltered;
    private InquiriesDetailQuotationItemClickListener itemListener;

    public InquireDetailsQuotationListAdapter(List<Quotation> quotationListOriginal, Context context, InquiriesDetailQuotationItemClickListener itemListener) {
        this.quotationListOriginal = quotationListOriginal;
        this.quotationFilterList = quotationListOriginal;
        this.context = context;
        this.itemListener = itemListener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_quotation_inquire_detail, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {

        holder.txQuoteType.setText(quotationFilterList.get(position).getQuotationType());
        holder.txQuoteId.setText(quotationFilterList.get(position).getQuotationId());
        holder.txDate.setText(quotationFilterList.get(position).getQuotationCreateDate());
        holder.txTime.setText(quotationFilterList.get(position).getQuotationTotalCost());

    }

    @Override
    public int getItemCount() {
        return quotationFilterList.size();
    }

    @Override
    public Filter getFilter() {
        if (quotationListFilter == null)
            quotationListFilter = new OrderListFilter(this, quotationListOriginal);
        return quotationListFilter;
    }

    private class OrderListFilter extends Filter {

        private final InquireDetailsQuotationListAdapter adapter;
        private final List<Quotation> originalList;

        private OrderListFilter(InquireDetailsQuotationListAdapter adapter, List<Quotation> originalList) {
            super();
            this.adapter = adapter;
            this.originalList = new LinkedList<>(originalList);
            quotationListFiltered = new ArrayList<>();
        }

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            quotationListFiltered.clear();
            final FilterResults results = new FilterResults();

            if (constraint.length() == 0) {
                quotationListFiltered.addAll(originalList);
            } else {
                final String filterPattern = constraint.toString().toLowerCase().trim();

                for (final Quotation list : originalList) {
                    if (String.valueOf(list.getQuotationId()).toLowerCase().contains(filterPattern)) {
                        quotationListFiltered.add(list);
                    }
                }
            }
            results.values = quotationListFiltered;
            results.count = quotationListFiltered.size();
            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            adapter.quotationFilterList.clear();
            adapter.quotationFilterList.addAll((ArrayList<Quotation>) results.values);
            adapter.notifyDataSetChanged();
        }
    }

    class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        @BindView(R.id.tx_quote_type)
        TVRobotoRegular txQuoteType;
        @BindView(R.id.cd_quote_type)
        CardView cdQuoteType;
        @BindView(R.id.tx_quote_id)
        TVRobotoRegular txQuoteId;
        @BindView(R.id.tx_date)
        TVRobotoRegular txDate;
        @BindView(R.id.tx_time)
        TVRobotoRegular txTime;

        ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
            view.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            itemListener.recyclerViewListClicked(v, this.getPosition(), Integer.parseInt(quotationListOriginal.get(this.getPosition()).getQuotationId()));
        }
    }

}