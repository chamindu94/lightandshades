/*
 * Copyright (c) 2017. A.G.THAMAYS <gthamays@gmail.com>
 * This file is part of Light & Shade.
 * Light & Shade can not be copied and/or distribute without the express
 * permission of A.G.THAMAYS
 */

package com.agtapp.lightandshade.adapter;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import com.agtapp.lightandshade.R;
import com.agtapp.lightandshade.model.Product;
import com.agtapp.lightandshade.util.interfaces.item_click_listener.ProductsItemClickListener;
import com.agtapp.lightandshade.util.text_view.TVRobotoBold;
import com.agtapp.lightandshade.util.text_view.TVRobotoRegular;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ProductsListAdapter extends RecyclerView.Adapter<ProductsListAdapter.ViewHolder> implements Filterable {

    private static final String TAG = "ProductsListAdapter";

    private Context context;
    private OrderListFilter productListFilter;
    private List<Product> productFilterList;
    private List<Product> productListOriginal;
    private List<Product> productListFiltered;
    private ProductsItemClickListener itemListener;
    private boolean normal_view;

    public ProductsListAdapter(List<Product> productListOriginal, Context context, ProductsItemClickListener itemListener, boolean normal_view) {
        this.productListOriginal = productListOriginal;
        this.productFilterList = productListOriginal;
        this.context = context;
        this.itemListener = itemListener;
        this.normal_view = normal_view;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_product_list, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {

        if (normal_view) {
            holder.lyNormal.setVisibility(View.VISIBLE);
            holder.lyProcess.setVisibility(View.GONE);
        } else {
            holder.lyNormal.setVisibility(View.GONE);
            holder.lyProcess.setVisibility(View.VISIBLE);
        }

        Picasso.with(context).load(productFilterList.get(position).getImage())
                .into(holder.imImage, new com.squareup.picasso.Callback() {
                    @Override
                    public void onSuccess() {
                        holder.imageLoad.setVisibility(View.GONE);
                    }

                    @Override
                    public void onError() {
                        holder.imImage.setImageResource(R.drawable.bg_splash);
                        holder.imageLoad.setVisibility(View.GONE);
                    }
                });

        holder.txTitle.setText(productFilterList.get(position).getName());
        holder.txPrice.setText(productFilterList.get(position).getProductPriceCountry() + " " + productFilterList.get(position).getUnitPrice());
        holder.txUnit.setText("Sq.Feet");

    }

    @Override
    public int getItemCount() {
        return productFilterList.size();
    }

    @Override
    public Filter getFilter() {
        if (productListFilter == null)
            productListFilter = new OrderListFilter(this, productListOriginal);
        return productListFilter;
    }

    private class OrderListFilter extends Filter {

        private final ProductsListAdapter adapter;
        private final List<Product> originalList;

        private OrderListFilter(ProductsListAdapter adapter, List<Product> originalList) {
            super();
            this.adapter = adapter;
            this.originalList = new LinkedList<>(originalList);
            productListFiltered = new ArrayList<>();
        }

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            productListFiltered.clear();
            final FilterResults results = new FilterResults();

            if (constraint.length() == 0) {
                productListFiltered.addAll(originalList);
            } else {
                final String filterPattern = constraint.toString().toLowerCase().trim();

                for (final Product list : originalList) {
                    if (String.valueOf(list.getName()).toLowerCase().contains(filterPattern)) {
                        productListFiltered.add(list);
                    }
                }
            }
            results.values = productListFiltered;
            results.count = productListFiltered.size();
            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            adapter.productFilterList.clear();
            adapter.productFilterList.addAll((ArrayList<Product>) results.values);
            adapter.notifyDataSetChanged();
        }
    }

    class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        @BindView(R.id.image_load)
        ProgressBar imageLoad;
        @BindView(R.id.im_image)
        ImageView imImage;
        @BindView(R.id.tx_title)
        TVRobotoBold txTitle;
        @BindView(R.id.tx_unit)
        TVRobotoRegular txUnit;
        @BindView(R.id.tx_price)
        TVRobotoRegular txPrice;
        @BindView(R.id.cd_more)
        CardView cdMore;
        @BindView(R.id.cd_add_quote)
        CardView cdAddQuote;
        @BindView(R.id.ly_process)
        LinearLayout lyProcess;
        @BindView(R.id.cd_normal_more)
        CardView cdNormalMore;
        @BindView(R.id.ly_normal)
        LinearLayout lyNormal;

        ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
            cdMore.setOnClickListener(this);
            cdAddQuote.setOnClickListener(this);
            cdNormalMore.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if (v.getId() == cdMore.getId())
                itemListener.recyclerViewListClicked(v, 1, this.getPosition(), String.valueOf(productListOriginal.get(this.getPosition()).getId()));

            if (v.getId() == cdAddQuote.getId())
                itemListener.recyclerViewListClicked(v, 2, this.getPosition(), String.valueOf(productListOriginal.get(this.getPosition()).getId()));

            if (v.getId() == cdNormalMore.getId())
                itemListener.recyclerViewListClicked(v, 3, this.getPosition(), String.valueOf(productListOriginal.get(this.getPosition()).getId()));
        }
    }

}