/*
 * Copyright (c) 2017. A.G.THAMAYS <gthamays@gmail.com>
 * This file is part of Light & Shade.
 * Light & Shade can not be copied and/or distribute without the express
 * permission of A.G.THAMAYS
 */

package com.agtapp.lightandshade.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.agtapp.lightandshade.R;
import com.agtapp.lightandshade.model.Wall;
import com.agtapp.lightandshade.util.MyToast;
import com.agtapp.lightandshade.util.edit_text.ETRobotoBold;
import com.agtapp.lightandshade.util.interfaces.item_click_listener.WallItemImageClickListener;
import com.agtapp.lightandshade.util.text_view.TVRobotoBold;
import com.agtapp.lightandshade.util.text_view.TVRobotoRegular;
import com.squareup.picasso.Picasso;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class WallsListAdapter extends RecyclerView.Adapter<WallsListAdapter.ViewHolder> implements Filterable {

    private Context context;
    private int room_position;
    private OrderListFilter wallListFilter;
    private List<Wall> wallFilterList;
    private List<Wall> wallListOriginal;
    private List<Wall> wallListFiltered;
    private WallItemImageClickListener wallItemImageClickListener;
    private boolean editable;
    private DecimalFormat decimalFormat = new DecimalFormat("###,##0.00");
    private Bitmap decodedByte;

    public WallsListAdapter(boolean editable, int room_position, List<Wall> wallListOriginal, Context context, WallItemImageClickListener wallItemImageClickListener) {
        this.editable = editable;
        this.wallListOriginal = wallListOriginal;
        this.wallFilterList = wallListOriginal;
        this.context = context;
        this.room_position = room_position;
        this.wallItemImageClickListener = wallItemImageClickListener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_quote_wall_list, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {

        Picasso.with(context).load(wallFilterList.get(position).getProductImage())
                .into(holder.imWallProductImage, new com.squareup.picasso.Callback() {
                    @Override
                    public void onSuccess() {
                        holder.wallProductImageLoad.setVisibility(View.GONE);
                    }

                    @Override
                    public void onError() {
                        holder.imWallProductImage.setImageResource(R.drawable.bg_splash);
                        holder.wallProductImageLoad.setVisibility(View.GONE);
                    }
                });
        if(wallFilterList.get(position).getWallImage()!=null) {
            if (wallFilterList.get(position).getWallImage().size() > 0) {
                String data = wallFilterList.get(position).getWallImage().get(0).toString();
                if(data.contains("http://")){
                    Picasso.with(context).load(wallFilterList.get(position).getWallImage().get(0))
                            .into(holder.imWallAddImage, new com.squareup.picasso.Callback() {
                                @Override
                                public void onSuccess() {
                                    BitmapDrawable drawable = (BitmapDrawable) holder.imWallAddImage.getDrawable();
                                    decodedByte = drawable.getBitmap();
                                    holder.imWallAddImage.setVisibility(View.VISIBLE);
                                }

                                @Override
                                public void onError() {
                                    holder.imWallAddImage.setVisibility(View.GONE);
                                }
                            });

                }else {
                    byte[] decodedString = Base64.decode(wallFilterList.get(position).getWallImage().get(0), Base64.DEFAULT);
                    decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
                    holder.imWallAddImage.setImageBitmap(decodedByte);
                    if(decodedString.length>0){
                        holder.imWallAddImage.setVisibility(View.VISIBLE);
                    }
                }



            }else {
                holder.imWallAddImage.setVisibility(View.GONE);
            }


        }else {
            holder.imWallAddImage.setVisibility(View.GONE);
        }
       /* Picasso.with(context).load(wallFilterList.get(position).getProductImage())
                .into(holder.imWallAddImage, new com.squareup.picasso.Callback() {
                    @Override
                    public void onSuccess() {
                        holder.imWallAddImage.setVisibility(View.VISIBLE);
                    }

                    @Override
                    public void onError() {
                        holder.imWallAddImage.setVisibility(View.GONE);
                    }
                });*/
        holder.etWallName.setEnabled(editable);
        holder.etWallName.setText(wallFilterList.get(position).getName());
        holder.txWallProductId.setText(wallFilterList.get(position).getProductCode());
        holder.txWallProductTitle.setText(wallFilterList.get(position).getProductName());
        holder.txWallArea.setText(wallFilterList.get(position).getWallArea());
        Double dvalue = Double.parseDouble(wallFilterList.get(position).getWallCost());
        String value1 = decimalFormat.format(dvalue);
        holder.txWallPrice.setText(wallFilterList.get(position).getWallCostCountry() + " " + value1);

        if (editable) {
            holder.lyEditView.setVisibility(View.VISIBLE);
            holder.imWallDelete.setVisibility(View.VISIBLE);
        } else {
            holder.lyEditView.setVisibility(View.GONE);
            holder.imWallDelete.setVisibility(View.GONE);
        }

        if (wallFilterList.get(position).getWallImage() != null)
            if (wallFilterList.get(position).getWallImage().size() > 0) {
                Log.i("IMAGE", "onBindViewHolder: " + wallFilterList.get(position).getWallImage().get(0));
                if (wallFilterList.get(position).getWallImage().get(0).equals("_")) {
                    holder.cdCaptureImage.setClickable(true);
                    holder.cdCaptureImage.setCardBackgroundColor(context.getResources().getColor(R.color.product_details_add_quote));
                    holder.txCaptureImage.setText("Capture Wall");
                } else {
                    holder.cdCaptureImage.setClickable(false);
                    holder.cdCaptureImage.setCardBackgroundColor(context.getResources().getColor(R.color.quote_wall));
                    holder.txCaptureImage.setText("Image Added");
                }
            }
      /*  holder.etWallName.setOnEditorActionListener(new EditText.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
                if (i == EditorInfo.IME_ACTION_DONE) {
                 *//*   quoteListTextChange.onRoomNameChange(position, textView.getText().toString());
                    roomNames.put(position, textView.getText().toString());*//*
                    wallItemImageClickListener.wallNameChange(textView, position, room_position,textView.getText().toString());
                    return true;
                }
                return false;
            }
        });*/
        holder.etWallName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                wallItemImageClickListener.wallNameChange( position, room_position,s.toString());
            }
        });
    }

    @Override
    public int getItemCount() {
        return wallFilterList.size();
    }

    @Override
    public Filter getFilter() {
        if (wallListFilter == null)
            wallListFilter = new OrderListFilter(this, wallListOriginal);
        return wallListFilter;
    }

    private class OrderListFilter extends Filter {

        private final WallsListAdapter adapter;
        private final List<Wall> originalList;

        private OrderListFilter(WallsListAdapter adapter, List<Wall> originalList) {
            super();
            this.adapter = adapter;
            this.originalList = new LinkedList<>(originalList);
            wallListFiltered = new ArrayList<>();
        }

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            wallListFiltered.clear();
            final FilterResults results = new FilterResults();

            if (constraint.length() == 0) {
                wallListFiltered.addAll(originalList);
            } else {
                final String filterPattern = constraint.toString().toLowerCase().trim();

                for (final Wall list : originalList) {
                    if (String.valueOf(list.getName()).toLowerCase().contains(filterPattern)) {
                        wallListFiltered.add(list);
                    }
                }
            }
            results.values = wallListFiltered;
            results.count = wallListFiltered.size();
            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            adapter.wallFilterList.clear();
            adapter.wallFilterList.addAll((ArrayList<Wall>) results.values);
            adapter.notifyDataSetChanged();
        }
    }

    class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        @BindView(R.id.et_wall_name)
        ETRobotoBold etWallName;
        @BindView(R.id.wall_product_image_load)
        ProgressBar wallProductImageLoad;
        @BindView(R.id.im_wall_product_image)
        ImageView imWallProductImage;
        @BindView(R.id.tx_wall_product_id)
        TVRobotoRegular txWallProductId;
        @BindView(R.id.tx_wall_product_title)
        TVRobotoBold txWallProductTitle;
        @BindView(R.id.tx_wall_area)
        TVRobotoBold txWallArea;
        @BindView(R.id.tx_wall_price)
        TVRobotoBold txWallPrice;
        @BindView(R.id.cd_add_product)
        CardView cdAddProduct;
        @BindView(R.id.tx_capture_image)
        TVRobotoBold txCaptureImage;
        @BindView(R.id.cd_capture_image)
        CardView cdCaptureImage;
        @BindView(R.id.ly_edit_view)
        LinearLayout lyEditView;
        @BindView(R.id.im_wall_add_image)
        ImageView imWallAddImage;
        @BindView(R.id.bu_wall_delete)
        ImageView imWallDelete;

        ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
            cdCaptureImage.setOnClickListener(this);
            cdAddProduct.setOnClickListener(this);
            imWallDelete.setOnClickListener(this);
            imWallAddImage.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if(v.getId()==R.id.cd_capture_image){
                wallItemImageClickListener.imageCapture(v, this.getPosition(), room_position);
            }else if(v.getId()==R.id.cd_add_product){
                wallItemImageClickListener.addQuote(v, this.getPosition(), room_position);
              //new MyToast(context, "Hello Text" + wallFilterList.get(this.getPosition()).getWallArea() );
            }else if(v.getId()==R.id.bu_wall_delete){
                wallItemImageClickListener.deleteWall( this.getPosition(), room_position);
            }else if(v.getId()==R.id.im_wall_add_image){
                wallItemImageClickListener.viewImageCapture(decodedByte, this.getPosition(), room_position);
            }

        }
    }

}