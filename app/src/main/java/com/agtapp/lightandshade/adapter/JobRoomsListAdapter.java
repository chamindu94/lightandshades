package com.agtapp.lightandshade.adapter;

import android.content.Context;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.Spinner;

import com.agtapp.lightandshade.R;
import com.agtapp.lightandshade.model.Product;
import com.agtapp.lightandshade.model.Room;
import com.agtapp.lightandshade.util.MySharedPreferences;
import com.agtapp.lightandshade.util.dialog.CreateQuoteDialog;
import com.agtapp.lightandshade.util.edit_text.ETRobotoBold;
import com.agtapp.lightandshade.util.edit_text.ETRobotoRegular;
import com.agtapp.lightandshade.util.interfaces.QuoteListTextChange;
import com.agtapp.lightandshade.util.interfaces.item_click_listener.WallItemImageClickListener;
import com.agtapp.lightandshade.util.text_view.TVRobotoBold;
import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;

public class JobRoomsListAdapter extends RecyclerView.Adapter<JobRoomsListAdapter.ViewHolder> implements Filterable {

    private Context context;
    private Product product;
    private String inquire_id;
    private OrderListFilter roomListFilter;
    private List<Room> roomFilterList;
    private List<Room> roomListOriginal;
    private List<Room> roomListFiltered;
    private WallItemImageClickListener wallItemImageClickListener;
    private boolean editable;
    private QuoteListTextChange quoteListTextChange;
    public HashMap<Integer, String> roomNames = new HashMap<>();
    public HashMap<Integer, String> roomDes = new HashMap<>();
    private DecimalFormat decimalFormat = new DecimalFormat("###,##0.00");
    private MySharedPreferences sharedPreferences;
    private RequestQueue requestQueue;


    public JobRoomsListAdapter(Context context) {
        this.context = context;
    }

    public JobRoomsListAdapter(boolean editable, List<Room> roomListOriginal, Context context, Product product, String inquire_id, WallItemImageClickListener wallItemImageClickListener, QuoteListTextChange quoteListTextChange) {
        this.editable = editable;
        this.quoteListTextChange = quoteListTextChange;
        this.roomListOriginal = roomListOriginal;
        this.roomFilterList = roomListOriginal;
        this.context = context;
        this.product = product;
        this.inquire_id = inquire_id;
        this.wallItemImageClickListener = wallItemImageClickListener;
        sharedPreferences = new MySharedPreferences(context);
        requestQueue = Volley.newRequestQueue(context);
    }

    @Override
    public JobRoomsListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_job_quote_room_list, parent, false);
        return new JobRoomsListAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {

        holder.etRoomName.setEnabled(editable);
        holder.etRoomDes.setEnabled(editable);
    /*    holder.etRoomName.setOnEditorActionListener(new EditText.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
                if (i == EditorInfo.IME_ACTION_DONE) {
                    quoteListTextChange.onRoomNameChange(position, textView.getText().toString());
                    roomNames.put(position, textView.getText().toString());
                    return true;
                }
                return false;
            }
        });*/

    /*    holder.etRoomDes.setOnEditorActionListener(new EditText.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
                if (i == EditorInfo.IME_ACTION_DONE) {
                    quoteListTextChange.onRoomDesChange(position, textView.getText().toString());
                    roomDes.put(position, textView.getText().toString());
                    return true;
                }
                return false;
            }
        });*/
        holder.etRoomName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                quoteListTextChange.onRoomNameChange(position, s.toString());
                roomNames.put(position, s.toString());
            }
        });

        holder.etRoomDes.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                quoteListTextChange.onRoomDesChange(position, s.toString());
                roomDes.put(position, s.toString());
            }
        });

        float wall_total = 0;
        for (int i = 0; i < roomFilterList.get(position).getWalls().size(); i++) {
            wall_total += Float.valueOf(roomFilterList.get(position).getWalls().get(i).getWallCost());
        }

        holder.etRoomName.setText(roomFilterList.get(position).getName());
        holder.etRoomDes.setText(roomFilterList.get(position).getDescription());

        roomNames.put(position, holder.etRoomName.getText().toString());
        roomNames.put(position, holder.etRoomDes.getText().toString());

        String value1 = decimalFormat.format(wall_total);
        holder.txRoomTotal.setText(roomFilterList.get(position).getTotalPriceCountry() + " " + value1);//wall_total

        holder.wallList.setLayoutManager(new LinearLayoutManager(context));
        holder.wallList.setHasFixedSize(true);
        holder.wallList.setLayoutManager(new GridLayoutManager(context, 1, GridLayoutManager.VERTICAL, false));

        Log.e("Room status", roomFilterList.get(position).getStatus());

        if (roomFilterList.get(position).getStatus().equals("Pending")) {
            holder.spStatus.setSelection(0);
        } else if (roomFilterList.get(position).getStatus().equals("InProgress")) {
            holder.spStatus.setSelection(1);
        } else if (roomFilterList.get(position).getStatus().equals("Completed")) {
            holder.spStatus.setSelection(2);
        } else if (roomFilterList.get(position).getStatus().equals("Cancelled")) {
            holder.spStatus.setSelection(3);
        }

        holder.spStatus.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (holder.spStatus.getSelectedItemPosition() == 0) {
                    updateJobStatus(("1"), roomFilterList.get(position).getId());
                } else if (holder.spStatus.getSelectedItemPosition() == 1) {
                    updateJobStatus("2", roomFilterList.get(position).getId());
                } else if (holder.spStatus.getSelectedItemPosition() == 2) {
                    updateJobStatus("3", roomFilterList.get(position).getId());
                } else if (holder.spStatus.getSelectedItemPosition() == 3) {
                    updateJobStatus("4", roomFilterList.get(position).getId());
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        JobWallsListAdapter adapter = new JobWallsListAdapter(editable, position, roomFilterList.get(position).getWalls(), context, wallItemImageClickListener);
        holder.wallList.setAdapter(adapter);

        if (editable) {
            holder.buRoomAdd.setVisibility(View.VISIBLE);
            holder.buRoomAdd.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    new CreateQuoteDialog(context, 2, product, position, inquire_id,0).show();
                }
            });
            holder.buRoomDelete.setVisibility(View.VISIBLE);
            holder.buRoomDelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    quoteListTextChange.deleteRoom(position);
                }
            });
        } else {
            holder.buRoomAdd.setVisibility(View.GONE);
            holder.buRoomDelete.setVisibility(View.GONE);
        }
    }

    @Override
    public int getItemCount() {
        return roomFilterList.size();
    }

    @Override
    public Filter getFilter() {
        if (roomListFilter == null)
            roomListFilter = new OrderListFilter(this, roomListOriginal);
        return roomListFilter;
    }

    private void updateJobStatus(final String status, final String id) {
        String url = "http://applns.lnsdigital.com/wallapi/public/roomupdate";

        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new com.android.volley.Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("Response Status", response);
                    }
                },
                new com.android.volley.Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e("Error Status", error.getMessage());
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> params = new HashMap<>();
                params.put("id", id);
                params.put("statues", status);

                Log.e("Params", params.toString());

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String,String> params = new HashMap<String,String>();
                params.put("Authorization", "Bearer " + sharedPreferences.getToken());
                return params;
            }
        };

        requestQueue.add(stringRequest);
    }

    private class OrderListFilter extends Filter {

        private final JobRoomsListAdapter adapter;
        private final List<Room> originalList;

        private OrderListFilter(JobRoomsListAdapter adapter, List<Room> originalList) {
            super();
            this.adapter = adapter;
            this.originalList = new LinkedList<>(originalList);
            roomListFiltered = new ArrayList<>();
        }

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            roomListFiltered.clear();
            final FilterResults results = new FilterResults();

            if (constraint.length() == 0) {
                roomListFiltered.addAll(originalList);
            } else {
                final String filterPattern = constraint.toString().toLowerCase().trim();

                for (final Room list : originalList) {
                    if (String.valueOf(list.getName()).toLowerCase().contains(filterPattern)) {
                        roomListFiltered.add(list);
                    }
                }
            }
            results.values = roomListFiltered;
            results.count = roomListFiltered.size();
            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            adapter.roomFilterList.clear();
            adapter.roomFilterList.addAll((ArrayList<Room>) results.values);
            adapter.notifyDataSetChanged();
        }
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.bu_room_add)
        ImageView buRoomAdd;
        @BindView(R.id.et_room_name)
        ETRobotoBold etRoomName;
        @BindView(R.id.tx_room_total)
        TVRobotoBold txRoomTotal;
        @BindView(R.id.et_room_des)
        ETRobotoRegular etRoomDes;
        @BindView(R.id.wall_list)
        RecyclerView wallList;
        @BindView(R.id.bu_room_delete)
        ImageView buRoomDelete;
        @BindView(R.id.sp_status)
        Spinner spStatus;

        ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);

            //set spinner items
            String[] arr = {"Pending", "InProgress", "Completed", "Cancelled"};
            ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(context, R.layout.spinner_item, arr); //selected item will look like a spinner set from XML
            spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            spStatus.setAdapter(spinnerArrayAdapter);

        }

    }
}
