/*
 * Copyright (c) 2017. A.G.THAMAYS <gthamays@gmail.com>
 * This file is part of Light & Shade.
 * Light & Shade can not be copied and/or distribute without the express
 * permission of A.G.THAMAYS
 */

package com.agtapp.lightandshade.adapter;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.agtapp.lightandshade.R;
import com.agtapp.lightandshade.model.Room;
import com.agtapp.lightandshade.model.Wall;
import com.agtapp.lightandshade.util.MyToast;
import com.agtapp.lightandshade.util.edit_text.ETRobotoBold;
import com.agtapp.lightandshade.util.edit_text.ETRobotoRegular;
import com.agtapp.lightandshade.util.text_view.TVRobotoBold;
import com.agtapp.lightandshade.util.text_view.TVRobotoRegular;
import com.squareup.picasso.Picasso;

import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by A.G.THAMAYS on 10/13/2017.
 */

public class QuoteExpandableListAdapter extends BaseExpandableListAdapter {

    private Context context;
    private List<Room> roomListOriginal;
//    private List<Wall> wallListOriginal;
//    private HashMap<String, List<Wall>> wallListOriginal;

    public QuoteExpandableListAdapter(Context context, List<Room> roomListOriginal) {
        this.context = context;
        this.roomListOriginal = roomListOriginal;
//        this.wallListOriginal = wallListOriginal;
    }

    @Override
    public int getGroupCount() {
        return roomListOriginal.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return roomListOriginal.get(groupPosition).getWalls().size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return roomListOriginal.get(groupPosition);
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return roomListOriginal.get(groupPosition).getWalls().get(childPosition);
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
//        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.item_quote_room_list, null);
//        }

        ViewHolderRoom viewHolderRoom = new ViewHolderRoom(convertView);

        viewHolderRoom.etRoomName.setText(roomListOriginal.get(groupPosition).getName());
        viewHolderRoom.txRoomTotal.setText(roomListOriginal.get(groupPosition).getTotalPriceCountry() + " " + roomListOriginal.get(groupPosition).getTotalPrice());
        viewHolderRoom.etRoomDes.setText(roomListOriginal.get(groupPosition).getDescription());

        viewHolderRoom.buRoomAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new MyToast(context, "Add Not Available Now.");
            }
        });

        return convertView;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
//        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.item_quote_wall_list, null);
//        }

        final ViewHolderWall viewHolderWall = new ViewHolderWall(convertView);

        Picasso.with(context).load(roomListOriginal.get(groupPosition).getWalls().get(childPosition).getProductImage())
                .into(viewHolderWall.imWallProductImage, new com.squareup.picasso.Callback() {
                    @Override
                    public void onSuccess() {
                        viewHolderWall.wallProductImageLoad.setVisibility(View.GONE);
                    }

                    @Override
                    public void onError() {
                        viewHolderWall.imWallProductImage.setImageResource(R.drawable.bg_splash);
                        viewHolderWall.wallProductImageLoad.setVisibility(View.GONE);
                    }
                });

        viewHolderWall.txWallProductId.setText(roomListOriginal.get(groupPosition).getWalls().get(childPosition).getProductCode());
        viewHolderWall.txWallProductTitle.setText(roomListOriginal.get(groupPosition).getWalls().get(childPosition).getProductName());
        viewHolderWall.txWallArea.setText(roomListOriginal.get(groupPosition).getWalls().get(childPosition).getWallArea());
        viewHolderWall.txWallPrice.setText(roomListOriginal.get(groupPosition).getWalls().get(childPosition).getWallCostCountry() + " " + roomListOriginal.get(groupPosition).getWalls().get(childPosition).getWallCost());

        viewHolderWall.cdAddProduct.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new MyToast(context, "Add Not Available Now.");
            }
        });

        viewHolderWall.cdCaptureImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new MyToast(context, "Add Not Available Now.");
            }
        });

        return convertView;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }

    static class ViewHolderRoom {
        @BindView(R.id.bu_room_add)
        ImageView buRoomAdd;
        @BindView(R.id.et_room_name)
        ETRobotoBold etRoomName;
        @BindView(R.id.tx_room_total)
        TVRobotoBold txRoomTotal;
        @BindView(R.id.et_room_des)
        ETRobotoRegular etRoomDes;
//        @BindView(R.id.wall_list)
//        RecyclerView wallList;

        ViewHolderRoom(View view) {
            ButterKnife.bind(this, view);
        }
    }

    static class ViewHolderWall {
        @BindView(R.id.et_wall_name)
        ETRobotoBold etWallName;
        @BindView(R.id.wall_product_image_load)
        ProgressBar wallProductImageLoad;
        @BindView(R.id.im_wall_product_image)
        ImageView imWallProductImage;
        @BindView(R.id.tx_wall_product_id)
        TVRobotoRegular txWallProductId;
        @BindView(R.id.tx_wall_product_title)
        TVRobotoBold txWallProductTitle;
        @BindView(R.id.tx_wall_area)
        TVRobotoBold txWallArea;
        @BindView(R.id.tx_wall_price)
        TVRobotoBold txWallPrice;
        @BindView(R.id.cd_add_product)
        CardView cdAddProduct;
        @BindView(R.id.cd_capture_image)
        CardView cdCaptureImage;

        ViewHolderWall(View view) {
            ButterKnife.bind(this, view);
        }
    }
}
